package org.mtahq.qmg.acep;

import java.io.Serializable;


public class AcepInfo implements Serializable {
    @SuppressWarnings("compatibility:1102412331362593200")
    private static final long serialVersionUID = 1L;
    private String agency;
    private String category;
    private String element;
    private String project;
    private String task;
    private String projectDescription;
    private String taskDescription;
    
    private boolean partialAcep = false;
    
    public AcepInfo() {
        super();
    }

    public AcepInfo(String agency, String category, String element, String project, String task,
                    String projectDescription, String taskDescription) {
        super();
        this.agency = agency;
        this.category = category;
        this.element = element;
        this.project = project;
        this.task = task;
        this.projectDescription = projectDescription;
        this.taskDescription = taskDescription;
    }

    public AcepInfo(String agency, String category, String element, String project, String projectDescription) {
        super();
        this.agency = agency;
        this.category = category;
        this.element = element;
        this.project = project;
        this.projectDescription = projectDescription;
        this.partialAcep = (this.category == null || this.element == null || this.project == null);
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getAgency() {
        return agency;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getElement() {
        return element;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getProject() {
        return project;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getTask() {
        return task;
    }

    public void setPartialAcep(boolean partialAcep) {
        this.partialAcep = partialAcep;
    }

    public boolean isPartialAcep() {
        return partialAcep;
    }

    @Override
    public String toString() {
        String res = String.format("%s%s%s/%s", this.agency,this.category,this.element,this.project);
        if(this.task != null) res = res + " - " + this.task;
        return res;
    }
}
