package org.mtahq.qmg.acep.session;

import java.util.List;

import javax.annotation.Resource;

import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.mtahq.qmg.acep.model.ImpPlanAgency;
import org.mtahq.qmg.acep.model.ImpPlanCategory;
import org.mtahq.qmg.acep.model.ImpPlanElement;
import org.mtahq.qmg.acep.model.ImpPlanProject;
import org.mtahq.qmg.acep.model.ImpPlanTask;

@Stateless(name = "AcepSessionEJB", mappedName = "AcepSessionEJB")
public class AcepSessionEJBBean implements AcepSessionEJB, AcepSessionEJBLocal {
    @Resource
    SessionContext sessionContext;
    @PersistenceContext(unitName = "AcepModel")
    private EntityManager em;

    public AcepSessionEJBBean() {
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Object queryByRange(String jpqlStmt, int firstResult, int maxResults) {
        Query query = em.createQuery(jpqlStmt);
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
    }

    public <T> T persistEntity(T entity) {
        em.persist(entity);
        return entity;
    }

    public <T> T mergeEntity(T entity) {
        return em.merge(entity);
    }

    public void removeImpPlanTask(ImpPlanTask impPlanTask) {
        impPlanTask = em.find(ImpPlanTask.class, impPlanTask.getTaskSeq());
        em.remove(impPlanTask);
    }

    /** <code>select o from ImpPlanTask o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ImpPlanTask> getImpPlanTaskFindAll() {
        return em.createNamedQuery("ImpPlanTask.findAll").getResultList();
    }

    /** <code>select o from ImpPlanTask o where o.agencyCode = :agCode and o.category = :category and o.element = :element and o.project = :project</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ImpPlanTask> getImpPlanTaskFindForACEP(String agCode, String category, String element, String project) {
        return em.createNamedQuery("ImpPlanTask.findForACEP").setParameter("agCode", agCode).setParameter("category",
                                                                                                          category).setParameter("element",
                                                                                                                                 element).setParameter("project",
                                                                                                                                                       project).getResultList();
    }

    public void removeImpPlanElement(ImpPlanElement impPlanElement) {
        impPlanElement = em.find(ImpPlanElement.class, impPlanElement.getElementSeq());
        em.remove(impPlanElement);
    }

    /** <code>select o from ImpPlanElement o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ImpPlanElement> getImpPlanElementFindAll() {
        return em.createNamedQuery("ImpPlanElement.findAll").getResultList();
    }

    /** <code>select o from ImpPlanElement o where o.agencyCode = :agCode and o.category = :category</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ImpPlanElement> getImpPlanElementFindForAgencyAndCategory(String agCode, String category) {
        return em.createNamedQuery("ImpPlanElement.findForAgencyAndCategory").setParameter("agCode",
                                                                                           agCode).setParameter("category",
                                                                                                                category).getResultList();
    }

    /** <code>select o from ImpPlanElement o where o.agencyCode = :agCode and o.category = :category and o.element = :element</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ImpPlanElement> getImpPlanElementFindForAgencyCategoryAndElement(String agCode, String category,
                                                                                 String element) {
        return em.createNamedQuery("ImpPlanElement.findForAgencyCategoryAndElement").setParameter("agCode",
                                                                                                  agCode).setParameter("category",
                                                                                                                       category).setParameter("element",
                                                                                                                                              element).getResultList();
    }

    public void removeImpPlanCategory(ImpPlanCategory impPlanCategory) {
        impPlanCategory = em.find(ImpPlanCategory.class, impPlanCategory.getCategorySeq());
        em.remove(impPlanCategory);
    }

    /** <code>select o from ImpPlanCategory o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ImpPlanCategory> getImpPlanCategoryFindAll() {
        return em.createNamedQuery("ImpPlanCategory.findAll").getResultList();
    }

    /** <code>select o from ImpPlanCategory o where o.agencyCode = :agCode</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ImpPlanCategory> getImpPlanCategoryFindForAgency(String agCode) {
        return em.createNamedQuery("ImpPlanCategory.findForAgency").setParameter("agCode", agCode).getResultList();
    }

    /** <code>select o from ImpPlanCategory o where o.agencyCode = :agCode and o.category = :category</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ImpPlanCategory> getImpPlanCategoryFindForAgencyAndCategory(String agCode, String category) {
        return em.createNamedQuery("ImpPlanCategory.findForAgencyAndCategory").setParameter("agCode",
                                                                                            agCode).setParameter("category",
                                                                                                                 category).getResultList();
    }

    public void removeImpPlanProject(ImpPlanProject impPlanProject) {
        impPlanProject = em.find(ImpPlanProject.class, impPlanProject.getProjectSeq());
        em.remove(impPlanProject);
    }

    /** <code>select o from ImpPlanProject o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ImpPlanProject> getImpPlanProjectFindAll() {
        return em.createNamedQuery("ImpPlanProject.findAll").getResultList();
    }

    /** <code>select o from ImpPlanProject o where o.agencyCode = :agCode and o.category = :category and o.element = :element</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ImpPlanProject> getImpPlanProjectFindForAgencyCategoryAndElement(String agCode, String category,
                                                                                 String element) {
        return em.createNamedQuery("ImpPlanProject.findForAgencyCategoryAndElement").setParameter("agCode",
                                                                                                  agCode).setParameter("category",
                                                                                                                       category).setParameter("element",
                                                                                                                                              element).getResultList();
    }

    /** <code>select o from ImpPlanProject o where o.agencyCode = :agCode and o.category = :category and o.element = :element and o.project = :project</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public ImpPlanProject getImpPlanProjectFindForAgencyCategoryElementAndProject(String agCode, String category,
                                                                                  String element, String project) {
        ImpPlanProject proj = null;
        try {
            proj =
                em.createNamedQuery("ImpPlanProject.findForAgencyCategoryElementAndProject",
                                    ImpPlanProject.class).setParameter("agCode", agCode).setParameter("category",
                                                                                                      category).setParameter("element",
                                                                                                                             element).setParameter("project",
                                                                                                                                                   project).getSingleResult();
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        return proj;
    }

    public void removeImpPlanAgency(ImpPlanAgency impPlanAgency) {
        impPlanAgency = em.find(ImpPlanAgency.class, impPlanAgency.getAgencySeq());
        em.remove(impPlanAgency);
    }

    /** <code>select o from ImpPlanAgency o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ImpPlanAgency> getImpPlanAgencyFindAll() {
        return em.createNamedQuery("ImpPlanAgency.findAll").getResultList();
    }

    /** <code>select o from ImpPlanAgency o where o.agencyCode = :agCode</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ImpPlanAgency> getImpPlanAgencyFindForCode(String agCode) {
        return em.createNamedQuery("ImpPlanAgency.findForCode").setParameter("agCode", agCode).getResultList();
    }

    /** <code>select o from ImpPlanProject o where o.project like :proj order by o.project</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ImpPlanProject> getImpPlanProjectFindByProject(String proj) {
        return em.createNamedQuery("ImpPlanProject.findByProject").setParameter("proj", proj).getResultList();
    }

    /** <code>select o from ImpPlanTask o where o.taskNumber = :task</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public ImpPlanTask getImpPlanTaskFindByTaskNum(String task) {
        ImpPlanTask ipt = null;
        try {
            ipt =
                em.createNamedQuery("ImpPlanTask.findByTaskNum", ImpPlanTask.class).setParameter("task",
                                                                                                 task).getSingleResult();
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        return ipt;
    }
}
