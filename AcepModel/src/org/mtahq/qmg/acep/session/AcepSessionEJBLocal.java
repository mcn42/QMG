package org.mtahq.qmg.acep.session;

import java.io.Serializable;

import java.util.List;

import javax.ejb.Local;

import org.mtahq.qmg.acep.model.ImpPlanAgency;
import org.mtahq.qmg.acep.model.ImpPlanCategory;
import org.mtahq.qmg.acep.model.ImpPlanElement;
import org.mtahq.qmg.acep.model.ImpPlanProject;
import org.mtahq.qmg.acep.model.ImpPlanTask;

@Local
public interface AcepSessionEJBLocal extends Serializable {
    Object queryByRange(String jpqlStmt, int firstResult, int maxResults);

    <T> T persistEntity(T entity);

    <T> T mergeEntity(T entity);

    void removeImpPlanTask(ImpPlanTask impPlanTask);

    List<ImpPlanTask> getImpPlanTaskFindAll();

    List<ImpPlanTask> getImpPlanTaskFindForACEP(String agCode, String category, String element, String project);

    void removeImpPlanElement(ImpPlanElement impPlanElement);

    List<ImpPlanElement> getImpPlanElementFindAll();

    List<ImpPlanElement> getImpPlanElementFindForAgencyAndCategory(String agCode, String category);

    List<ImpPlanElement> getImpPlanElementFindForAgencyCategoryAndElement(String agCode, String category,
                                                                          String element);

    void removeImpPlanCategory(ImpPlanCategory impPlanCategory);

    List<ImpPlanCategory> getImpPlanCategoryFindAll();

    List<ImpPlanCategory> getImpPlanCategoryFindForAgency(String agCode);

    List<ImpPlanCategory> getImpPlanCategoryFindForAgencyAndCategory(String agCode, String category);

    void removeImpPlanProject(ImpPlanProject impPlanProject);

    List<ImpPlanProject> getImpPlanProjectFindAll();

    List<ImpPlanProject> getImpPlanProjectFindForAgencyCategoryAndElement(String agCode, String category,
                                                                          String element);

    ImpPlanProject getImpPlanProjectFindForAgencyCategoryElementAndProject(String agCode, String category,
                                                                           String element, String project);

    void removeImpPlanAgency(ImpPlanAgency impPlanAgency);

    List<ImpPlanAgency> getImpPlanAgencyFindAll();

    List<ImpPlanAgency> getImpPlanAgencyFindForCode(String agCode);

    List<ImpPlanProject> getImpPlanProjectFindByProject(String proj);

    ImpPlanTask getImpPlanTaskFindByTaskNum(String task);
}
