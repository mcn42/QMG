package org.mtahq.qmg.acep.model;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@XmlRootElement
@NamedQueries({
              @NamedQuery(name = "ImpPlanProject.findAll", query = "select o from ImpPlanProject o"),
              @NamedQuery(name = "ImpPlanProject.findByProject",
                          query = "select o from ImpPlanProject o where o.project like :proj order by o.project"),
              @NamedQuery(name = "ImpPlanProject.findForAgencyCategoryAndElement",
                          query =
                          "select o from ImpPlanProject o where o.agencyCode = :agCode and o.category = :category and o.element = :element"),
              @NamedQuery(name = "ImpPlanProject.findForAgencyCategoryElementAndProject",
                          query =
                          "select o from ImpPlanProject o where o.agencyCode = :agCode and o.category = :category and o.element = :element and o.project = :project")
    })
@Table(name = "IMP_PLAN_PROJECTS")
public class ImpPlanProject implements Serializable {
    @Column(name = "AGENCY_CODE", nullable = false, unique = true, length = 1)
    private String agencyCode;
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "APPROVAL_DATE")
    private Date approvalDate;
    @Column(name = "APPROVAL_STATUS", nullable = false, length = 1)
    private String approvalStatus;
    @Column(name = "APPROVED_BUDGET", nullable = false)
    private Long approvedBudget;
    @Column(name = "BLUE_PAGES", length = 2)
    private String bluePages;
    @Column(name = "BRIDGE_CODE", length = 1)
    private String bridgeCode;

    @Column(nullable = false, unique = true, length = 3)
    private String category;
    @Column(length = 4000)
    private String comments;

    @Column(length = 50)
    private String description;
    @Column(nullable = false, unique = true, length = 2)
    private String element;
    @Column(name = "ELEMENT_SEQ", nullable = false, unique = true)
    private Long elementSeq;

    @Column(name = "FUND_VALIDATION", length = 1)
    private String fundValidation;
    @Column(name = "FUNDING_APPROVAL", nullable = false, length = 1)
    private String fundingApproval;

    @Column(name = "NEEDS_CODE", length = 3)
    private String needsCode;

    @Column(name = "PROGRAM_MANAGER", length = 3)
    private String programManager;
    @Column(nullable = false, unique = true, length = 2)
    private String project;
    @Id
    @Column(name = "PROJECT_SEQ", nullable = false)
    private Long projectSeq;

    @Column(name = "STATUS_CODE", length = 3)
    private String statusCode;
    @Column(name = "TIME_SEQ", nullable = false)
    private Long timeSeq;
    @Column(name = "UPDATED_BY", length = 32)
    private String updatedBy;
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "UPDATED_ON")
    private Date updatedOn;

    public ImpPlanProject() {
    }

    public ImpPlanProject(String agencyCode, Date approvalDate, String approvalStatus, Long approvedBudget,
                          String bluePages, String bridgeCode, String category, String comments, String description,
                          String element, Long elementSeq, String fundValidation, String fundingApproval,
                          String needsCode, String programManager, String project, Long projectSeq, String statusCode,
                          Long timeSeq, String updatedBy, Date updatedOn) {
        super();
        this.agencyCode = agencyCode;
        this.approvalDate = approvalDate;
        this.approvalStatus = approvalStatus;
        this.approvedBudget = approvedBudget;
        this.bluePages = bluePages;
        this.bridgeCode = bridgeCode;
        this.category = category;
        this.comments = comments;
        this.description = description;
        this.element = element;
        this.elementSeq = elementSeq;
        this.fundValidation = fundValidation;
        this.fundingApproval = fundingApproval;
        this.needsCode = needsCode;
        this.programManager = programManager;
        this.project = project;
        this.projectSeq = projectSeq;
        this.statusCode = statusCode;
        this.timeSeq = timeSeq;
        this.updatedBy = updatedBy;
        this.updatedOn = updatedOn;
    }


    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(this.project);
        buffer.append('[');
        buffer.append(']');
        return buffer.toString();
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public Long getApprovedBudget() {
        return approvedBudget;
    }

    public void setApprovedBudget(Long approvedBudget) {
        this.approvedBudget = approvedBudget;
    }

    public String getBluePages() {
        return bluePages;
    }

    public void setBluePages(String bluePages) {
        this.bluePages = bluePages;
    }

    public String getBridgeCode() {
        return bridgeCode;
    }

    public void setBridgeCode(String bridgeCode) {
        this.bridgeCode = bridgeCode;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public Long getElementSeq() {
        return elementSeq;
    }

    public void setElementSeq(Long elementSeq) {
        this.elementSeq = elementSeq;
    }

    public String getFundValidation() {
        return fundValidation;
    }

    public void setFundValidation(String fundValidation) {
        this.fundValidation = fundValidation;
    }

    public String getFundingApproval() {
        return fundingApproval;
    }

    public void setFundingApproval(String fundingApproval) {
        this.fundingApproval = fundingApproval;
    }

    public String getNeedsCode() {
        return needsCode;
    }

    public void setNeedsCode(String needsCode) {
        this.needsCode = needsCode;
    }

    public String getProgramManager() {
        return programManager;
    }

    public void setProgramManager(String programManager) {
        this.programManager = programManager;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public Long getProjectSeq() {
        return projectSeq;
    }

    public void setProjectSeq(Long projectSeq) {
        this.projectSeq = projectSeq;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Long getTimeSeq() {
        return timeSeq;
    }

    public void setTimeSeq(Long timeSeq) {
        this.timeSeq = timeSeq;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
}
