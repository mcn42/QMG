package org.mtahq.qmg.acep.model;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "ImpPlanTask.findAll", query = "select o from ImpPlanTask o"),
        @NamedQuery(name = "ImpPlanTask.findForACEP",
            query =
            "select o from ImpPlanTask o where o.agencyCode = :agCode and o.category = :category and o.element = :element and o.project = :project"),
        @NamedQuery(name = "ImpPlanTask.findByTaskNum", query = "select o from ImpPlanTask o where o.taskNumber = :task")
    })
@Table(name = "IMP_PLAN_TASKS")
public class ImpPlanTask implements Serializable {
    private static final long serialVersionUID = 1226935044203674481L;
    @Column(name = "AGENCY_CODE", nullable = false, unique = true, length = 1)
    private String agencyCode;
    @Column(name = "AGENCY_PROJECT", length = 9)
    private String agencyProject;
    @Column(name = "AGENCY_SUBTYPE", length = 3)
    private String agencySubtype;
    @Column(name = "AGENCY_TASK", length = 7)
    private String agencyTask;
    @Column(name = "ASSET_AGENCY", nullable = false, length = 1)
    private String assetAgency;
    @Temporal(TemporalType.DATE)
    @Column(name = "BID_DATE")
    private Date bidDate;
    @Column(name = "BIDDER_CODE", length = 1)
    private String bidderCode;
    @Column(nullable = false, unique = true, length = 3)
    private String category;
    @Column(length = 4000)
    private String comments;
    @Column(name = "CONTRACT_DESCR1", length = 30)
    private String contractDescr1;
    @Column(name = "CONTRACT_DESCR2", length = 30)
    private String contractDescr2;
    @Column(name = "CONTRACT_DESCR3", length = 30)
    private String contractDescr3;
    @Temporal(TemporalType.DATE)
    @Column(name = "CONTRACT_END")
    private Date contractEnd;
    @Column(name = "CONTRACT_ID", length = 15)
    private String contractId;
    @Temporal(TemporalType.DATE)
    @Column(name = "CONTRACT_START")
    private Date contractStart;
    @Column(name = "COST_CURVE", length = 2)
    private String costCurve;
    @Column(name = "COST_TYPE", nullable = false, length = 3)
    private String costType;
    @Column(name = "CURRENT_ALLOCATION", nullable = false)
    private Long currentAllocation;
    @Column(name = "CURRENT_BUDGET_TYPE", length = 2)
    private String currentBudgetType;
    @Column(length = 50)
    private String description;
    @Column(name = "DESIGN_TYPE", nullable = false, length = 3)
    private String designType;
    @Column(nullable = false, unique = true, length = 2)
    private String element;
    @Column(name = "ENCUMBERED_AMOUNT", nullable = false)
    private Long encumberedAmount;
    @Column(name = "ENV_REMED", nullable = false, length = 1)
    private String envRemed;
    @Column(name = "ESTIMATE_AT_COMPLETION", nullable = false)
    private Long estimateAtCompletion;
    @Column(name = "EXPENDED_AMOUNT", nullable = false)
    private Long expendedAmount;
    @Column(name = "FED_FUND_FLAG", length = 1)
    private String fedFundFlag;
    @Column(name = "FORCE_ACCOUNT", length = 3)
    private String forceAccount;
    @Column(name = "FUND_VALIDATION", length = 1)
    private String fundValidation;
    @Column(name = "LAST_FUND_SEQ")
    private Integer lastFundSeq;
    @Column(name = "LINE_ALLOCATION_SUM", nullable = false)
    private Long lineAllocationSum;
    @Column(name = "OBLIGATED_AMOUNT", nullable = false)
    private Long obligatedAmount;
    @Column(name = "OPERATING_AGENCY", nullable = false, length = 1)
    private String operatingAgency;
    @Column(name = "ORIGINAL_ALLOCATION", nullable = false)
    private Long originalAllocation;
    @Column(name = "ORIGINAL_BUDGET_TYPE", length = 2)
    private String originalBudgetType;
    @Column(name = "PERIOD_EXPENDED", nullable = false)
    private Long periodExpended;
    @Column(name = "PLANNED_BID_YEAR", nullable = false)
    private Integer plannedBidYear;
    @Column(nullable = false, unique = true, length = 2)
    private String project;
    @Column(name = "PROJECT_MANAGER", length = 3)
    private String projectManager;
    @Column(name = "PROJECT_SEQ", nullable = false, unique = true)
    private Long projectSeq;
    @Column(name = "PSE_ID", length = 6)
    private String pseId;
    @Column(name = "PSR_OWN_AGY", length = 1)
    private String psrOwnAgy;
    @Column(name = "RESOURCE_TYPE", nullable = false, length = 1)
    private String resourceType;
    @Column(name = "RESPONSIBILITY_AREA", nullable = false, length = 2)
    private String responsibilityArea;
    @Column(name = "SCHEDULE_STATUS", length = 1)
    private String scheduleStatus;
    @Column(name = "SUM_OF_INQ", nullable = false)
    private Long sumOfInq;
    @Column(name = "TASK_NUMBER", nullable = false, unique = true, length = 6)
    private String taskNumber;
    @Id
    @Column(name = "TASK_SEQ", nullable = false)
    private Long taskSeq;
    @Column(name = "TASK_STATUS", length = 1)
    private String taskStatus;
    @Column(name = "TIME_SEQ", nullable = false)
    private Long timeSeq;
    @Column(name = "UPDATED_BY", length = 32)
    private String updatedBy;
    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATED_ON")
    private Date updatedOn;
    @Column(name = "USER_TYPE", length = 1)
    private String userType;
    @Column(name = "WAR_LEVEL", nullable = false, length = 3)
    private String warLevel;
    @Column(name = "WAR_LVL_UPD_BY", length = 32)
    private String warLvlUpdBy;
    @Temporal(TemporalType.DATE)
    @Column(name = "WAR_LVL_UPD_ON")
    private Date warLvlUpdOn;
    @Column(name = "WORK_TYPE", nullable = false, length = 3)
    private String workType;
    @Column(name = "YTD_EXPENDED", nullable = false)
    private Long ytdExpended;

    public ImpPlanTask() {
    }

    public ImpPlanTask(String agencyCode, String agencyProject, String agencySubtype, String agencyTask,
        String assetAgency, Date bidDate, String bidderCode, String category, String comments, String contractDescr1,
        String contractDescr2, String contractDescr3, Date contractEnd, String contractId, Date contractStart,
        String costCurve, String costType, Long currentAllocation, String currentBudgetType, String description,
        String designType, String element, Long encumberedAmount, String envRemed, Long estimateAtCompletion,
        Long expendedAmount, String fedFundFlag, String forceAccount, String fundValidation, Integer lastFundSeq,
        Long lineAllocationSum, Long obligatedAmount, String operatingAgency, Long originalAllocation,
        String originalBudgetType, Long periodExpended, Integer plannedBidYear, String project, String projectManager,
        Long projectSeq, String pseId, String psrOwnAgy, String resourceType, String responsibilityArea,
        String scheduleStatus, Long sumOfInq, String taskNumber, Long taskSeq, String taskStatus, Long timeSeq,
        String updatedBy, Date updatedOn, String userType, String warLevel, String warLvlUpdBy, Date warLvlUpdOn,
        String workType, Long ytdExpended) {
        this.agencyCode = agencyCode;
        this.agencyProject = agencyProject;
        this.agencySubtype = agencySubtype;
        this.agencyTask = agencyTask;
        this.assetAgency = assetAgency;
        this.bidDate = bidDate;
        this.bidderCode = bidderCode;
        this.category = category;
        this.comments = comments;
        this.contractDescr1 = contractDescr1;
        this.contractDescr2 = contractDescr2;
        this.contractDescr3 = contractDescr3;
        this.contractEnd = contractEnd;
        this.contractId = contractId;
        this.contractStart = contractStart;
        this.costCurve = costCurve;
        this.costType = costType;
        this.currentAllocation = currentAllocation;
        this.currentBudgetType = currentBudgetType;
        this.description = description;
        this.designType = designType;
        this.element = element;
        this.encumberedAmount = encumberedAmount;
        this.envRemed = envRemed;
        this.estimateAtCompletion = estimateAtCompletion;
        this.expendedAmount = expendedAmount;
        this.fedFundFlag = fedFundFlag;
        this.forceAccount = forceAccount;
        this.fundValidation = fundValidation;
        this.lastFundSeq = lastFundSeq;
        this.lineAllocationSum = lineAllocationSum;
        this.obligatedAmount = obligatedAmount;
        this.operatingAgency = operatingAgency;
        this.originalAllocation = originalAllocation;
        this.originalBudgetType = originalBudgetType;
        this.periodExpended = periodExpended;
        this.plannedBidYear = plannedBidYear;
        this.project = project;
        this.projectManager = projectManager;
        this.projectSeq = projectSeq;
        this.pseId = pseId;
        this.psrOwnAgy = psrOwnAgy;
        this.resourceType = resourceType;
        this.responsibilityArea = responsibilityArea;
        this.scheduleStatus = scheduleStatus;
        this.sumOfInq = sumOfInq;
        this.taskNumber = taskNumber;
        this.taskSeq = taskSeq;
        this.taskStatus = taskStatus;
        this.timeSeq = timeSeq;
        this.updatedBy = updatedBy;
        this.updatedOn = updatedOn;
        this.userType = userType;
        this.warLevel = warLevel;
        this.warLvlUpdBy = warLvlUpdBy;
        this.warLvlUpdOn = warLvlUpdOn;
        this.workType = workType;
        this.ytdExpended = ytdExpended;
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getAgencyProject() {
        return agencyProject;
    }

    public void setAgencyProject(String agencyProject) {
        this.agencyProject = agencyProject;
    }

    public String getAgencySubtype() {
        return agencySubtype;
    }

    public void setAgencySubtype(String agencySubtype) {
        this.agencySubtype = agencySubtype;
    }

    public String getAgencyTask() {
        return agencyTask;
    }

    public void setAgencyTask(String agencyTask) {
        this.agencyTask = agencyTask;
    }

    public String getAssetAgency() {
        return assetAgency;
    }

    public void setAssetAgency(String assetAgency) {
        this.assetAgency = assetAgency;
    }

    public Date getBidDate() {
        return bidDate;
    }

    public void setBidDate(Date bidDate) {
        this.bidDate = bidDate;
    }

    public String getBidderCode() {
        return bidderCode;
    }

    public void setBidderCode(String bidderCode) {
        this.bidderCode = bidderCode;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getContractDescr1() {
        return contractDescr1;
    }

    public void setContractDescr1(String contractDescr1) {
        this.contractDescr1 = contractDescr1;
    }

    public String getContractDescr2() {
        return contractDescr2;
    }

    public void setContractDescr2(String contractDescr2) {
        this.contractDescr2 = contractDescr2;
    }

    public String getContractDescr3() {
        return contractDescr3;
    }

    public void setContractDescr3(String contractDescr3) {
        this.contractDescr3 = contractDescr3;
    }

    public Date getContractEnd() {
        return contractEnd;
    }

    public void setContractEnd(Date contractEnd) {
        this.contractEnd = contractEnd;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public Date getContractStart() {
        return contractStart;
    }

    public void setContractStart(Date contractStart) {
        this.contractStart = contractStart;
    }

    public String getCostCurve() {
        return costCurve;
    }

    public void setCostCurve(String costCurve) {
        this.costCurve = costCurve;
    }

    public String getCostType() {
        return costType;
    }

    public void setCostType(String costType) {
        this.costType = costType;
    }

    public Long getCurrentAllocation() {
        return currentAllocation;
    }

    public void setCurrentAllocation(Long currentAllocation) {
        this.currentAllocation = currentAllocation;
    }

    public String getCurrentBudgetType() {
        return currentBudgetType;
    }

    public void setCurrentBudgetType(String currentBudgetType) {
        this.currentBudgetType = currentBudgetType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDesignType() {
        return designType;
    }

    public void setDesignType(String designType) {
        this.designType = designType;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public Long getEncumberedAmount() {
        return encumberedAmount;
    }

    public void setEncumberedAmount(Long encumberedAmount) {
        this.encumberedAmount = encumberedAmount;
    }

    public String getEnvRemed() {
        return envRemed;
    }

    public void setEnvRemed(String envRemed) {
        this.envRemed = envRemed;
    }

    public Long getEstimateAtCompletion() {
        return estimateAtCompletion;
    }

    public void setEstimateAtCompletion(Long estimateAtCompletion) {
        this.estimateAtCompletion = estimateAtCompletion;
    }

    public Long getExpendedAmount() {
        return expendedAmount;
    }

    public void setExpendedAmount(Long expendedAmount) {
        this.expendedAmount = expendedAmount;
    }

    public String getFedFundFlag() {
        return fedFundFlag;
    }

    public void setFedFundFlag(String fedFundFlag) {
        this.fedFundFlag = fedFundFlag;
    }

    public String getForceAccount() {
        return forceAccount;
    }

    public void setForceAccount(String forceAccount) {
        this.forceAccount = forceAccount;
    }

    public String getFundValidation() {
        return fundValidation;
    }

    public void setFundValidation(String fundValidation) {
        this.fundValidation = fundValidation;
    }

    public Integer getLastFundSeq() {
        return lastFundSeq;
    }

    public void setLastFundSeq(Integer lastFundSeq) {
        this.lastFundSeq = lastFundSeq;
    }

    public Long getLineAllocationSum() {
        return lineAllocationSum;
    }

    public void setLineAllocationSum(Long lineAllocationSum) {
        this.lineAllocationSum = lineAllocationSum;
    }

    public Long getObligatedAmount() {
        return obligatedAmount;
    }

    public void setObligatedAmount(Long obligatedAmount) {
        this.obligatedAmount = obligatedAmount;
    }

    public String getOperatingAgency() {
        return operatingAgency;
    }

    public void setOperatingAgency(String operatingAgency) {
        this.operatingAgency = operatingAgency;
    }

    public Long getOriginalAllocation() {
        return originalAllocation;
    }

    public void setOriginalAllocation(Long originalAllocation) {
        this.originalAllocation = originalAllocation;
    }

    public String getOriginalBudgetType() {
        return originalBudgetType;
    }

    public void setOriginalBudgetType(String originalBudgetType) {
        this.originalBudgetType = originalBudgetType;
    }

    public Long getPeriodExpended() {
        return periodExpended;
    }

    public void setPeriodExpended(Long periodExpended) {
        this.periodExpended = periodExpended;
    }

    public Integer getPlannedBidYear() {
        return plannedBidYear;
    }

    public void setPlannedBidYear(Integer plannedBidYear) {
        this.plannedBidYear = plannedBidYear;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getProjectManager() {
        return projectManager;
    }

    public void setProjectManager(String projectManager) {
        this.projectManager = projectManager;
    }

    public Long getProjectSeq() {
        return projectSeq;
    }

    public void setProjectSeq(Long projectSeq) {
        this.projectSeq = projectSeq;
    }

    public String getPseId() {
        return pseId;
    }

    public void setPseId(String pseId) {
        this.pseId = pseId;
    }

    public String getPsrOwnAgy() {
        return psrOwnAgy;
    }

    public void setPsrOwnAgy(String psrOwnAgy) {
        this.psrOwnAgy = psrOwnAgy;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResponsibilityArea() {
        return responsibilityArea;
    }

    public void setResponsibilityArea(String responsibilityArea) {
        this.responsibilityArea = responsibilityArea;
    }

    public String getScheduleStatus() {
        return scheduleStatus;
    }

    public void setScheduleStatus(String scheduleStatus) {
        this.scheduleStatus = scheduleStatus;
    }

    public Long getSumOfInq() {
        return sumOfInq;
    }

    public void setSumOfInq(Long sumOfInq) {
        this.sumOfInq = sumOfInq;
    }

    public String getTaskNumber() {
        return taskNumber;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public Long getTaskSeq() {
        return taskSeq;
    }

    public void setTaskSeq(Long taskSeq) {
        this.taskSeq = taskSeq;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public Long getTimeSeq() {
        return timeSeq;
    }

    public void setTimeSeq(Long timeSeq) {
        this.timeSeq = timeSeq;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getWarLevel() {
        return warLevel;
    }

    public void setWarLevel(String warLevel) {
        this.warLevel = warLevel;
    }

    public String getWarLvlUpdBy() {
        return warLvlUpdBy;
    }

    public void setWarLvlUpdBy(String warLvlUpdBy) {
        this.warLvlUpdBy = warLvlUpdBy;
    }

    public Date getWarLvlUpdOn() {
        return warLvlUpdOn;
    }

    public void setWarLvlUpdOn(Date warLvlUpdOn) {
        this.warLvlUpdOn = warLvlUpdOn;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public Long getYtdExpended() {
        return ytdExpended;
    }

    public void setYtdExpended(Long ytdExpended) {
        this.ytdExpended = ytdExpended;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(getClass().getName() + "@" + Integer.toHexString(hashCode()));
        buffer.append('[');
        buffer.append("agencyCode=");
        buffer.append(getAgencyCode());
        buffer.append(',');
        buffer.append("agencyProject=");
        buffer.append(getAgencyProject());
        buffer.append(',');
        buffer.append("agencySubtype=");
        buffer.append(getAgencySubtype());
        buffer.append(',');
        buffer.append("agencyTask=");
        buffer.append(getAgencyTask());
        buffer.append(',');
        buffer.append("assetAgency=");
        buffer.append(getAssetAgency());
        buffer.append(',');
        buffer.append("bidDate=");
        buffer.append(getBidDate());
        buffer.append(',');
        buffer.append("bidderCode=");
        buffer.append(getBidderCode());
        buffer.append(',');
        buffer.append("category=");
        buffer.append(getCategory());
        buffer.append(',');
        buffer.append("comments=");
        buffer.append(getComments());
        buffer.append(',');
        buffer.append("contractDescr1=");
        buffer.append(getContractDescr1());
        buffer.append(',');
        buffer.append("contractDescr2=");
        buffer.append(getContractDescr2());
        buffer.append(',');
        buffer.append("contractDescr3=");
        buffer.append(getContractDescr3());
        buffer.append(',');
        buffer.append("contractEnd=");
        buffer.append(getContractEnd());
        buffer.append(',');
        buffer.append("contractId=");
        buffer.append(getContractId());
        buffer.append(',');
        buffer.append("contractStart=");
        buffer.append(getContractStart());
        buffer.append(',');
        buffer.append("costCurve=");
        buffer.append(getCostCurve());
        buffer.append(',');
        buffer.append("costType=");
        buffer.append(getCostType());
        buffer.append(',');
        buffer.append("currentAllocation=");
        buffer.append(getCurrentAllocation());
        buffer.append(',');
        buffer.append("currentBudgetType=");
        buffer.append(getCurrentBudgetType());
        buffer.append(',');
        buffer.append("description=");
        buffer.append(getDescription());
        buffer.append(',');
        buffer.append("designType=");
        buffer.append(getDesignType());
        buffer.append(',');
        buffer.append("element=");
        buffer.append(getElement());
        buffer.append(',');
        buffer.append("encumberedAmount=");
        buffer.append(getEncumberedAmount());
        buffer.append(',');
        buffer.append("envRemed=");
        buffer.append(getEnvRemed());
        buffer.append(',');
        buffer.append("estimateAtCompletion=");
        buffer.append(getEstimateAtCompletion());
        buffer.append(',');
        buffer.append("expendedAmount=");
        buffer.append(getExpendedAmount());
        buffer.append(',');
        buffer.append("fedFundFlag=");
        buffer.append(getFedFundFlag());
        buffer.append(',');
        buffer.append("forceAccount=");
        buffer.append(getForceAccount());
        buffer.append(',');
        buffer.append("fundValidation=");
        buffer.append(getFundValidation());
        buffer.append(',');
        buffer.append("lastFundSeq=");
        buffer.append(getLastFundSeq());
        buffer.append(',');
        buffer.append("lineAllocationSum=");
        buffer.append(getLineAllocationSum());
        buffer.append(',');
        buffer.append("obligatedAmount=");
        buffer.append(getObligatedAmount());
        buffer.append(',');
        buffer.append("operatingAgency=");
        buffer.append(getOperatingAgency());
        buffer.append(',');
        buffer.append("originalAllocation=");
        buffer.append(getOriginalAllocation());
        buffer.append(',');
        buffer.append("originalBudgetType=");
        buffer.append(getOriginalBudgetType());
        buffer.append(',');
        buffer.append("periodExpended=");
        buffer.append(getPeriodExpended());
        buffer.append(',');
        buffer.append("plannedBidYear=");
        buffer.append(getPlannedBidYear());
        buffer.append(',');
        buffer.append("project=");
        buffer.append(getProject());
        buffer.append(',');
        buffer.append("projectManager=");
        buffer.append(getProjectManager());
        buffer.append(',');
        buffer.append("projectSeq=");
        buffer.append(getProjectSeq());
        buffer.append(',');
        buffer.append("pseId=");
        buffer.append(getPseId());
        buffer.append(',');
        buffer.append("psrOwnAgy=");
        buffer.append(getPsrOwnAgy());
        buffer.append(',');
        buffer.append("resourceType=");
        buffer.append(getResourceType());
        buffer.append(',');
        buffer.append("responsibilityArea=");
        buffer.append(getResponsibilityArea());
        buffer.append(',');
        buffer.append("scheduleStatus=");
        buffer.append(getScheduleStatus());
        buffer.append(',');
        buffer.append("sumOfInq=");
        buffer.append(getSumOfInq());
        buffer.append(',');
        buffer.append("taskNumber=");
        buffer.append(getTaskNumber());
        buffer.append(',');
        buffer.append("taskSeq=");
        buffer.append(getTaskSeq());
        buffer.append(',');
        buffer.append("taskStatus=");
        buffer.append(getTaskStatus());
        buffer.append(',');
        buffer.append("timeSeq=");
        buffer.append(getTimeSeq());
        buffer.append(',');
        buffer.append("updatedBy=");
        buffer.append(getUpdatedBy());
        buffer.append(',');
        buffer.append("updatedOn=");
        buffer.append(getUpdatedOn());
        buffer.append(',');
        buffer.append("userType=");
        buffer.append(getUserType());
        buffer.append(',');
        buffer.append("warLevel=");
        buffer.append(getWarLevel());
        buffer.append(',');
        buffer.append("warLvlUpdBy=");
        buffer.append(getWarLvlUpdBy());
        buffer.append(',');
        buffer.append("warLvlUpdOn=");
        buffer.append(getWarLvlUpdOn());
        buffer.append(',');
        buffer.append("workType=");
        buffer.append(getWorkType());
        buffer.append(',');
        buffer.append("ytdExpended=");
        buffer.append(getYtdExpended());
        buffer.append(']');
        return buffer.toString();
    }
}
