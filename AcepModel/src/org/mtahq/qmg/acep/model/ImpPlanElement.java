package org.mtahq.qmg.acep.model;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "ImpPlanElement.findAll", query = "select o from ImpPlanElement o"),
        @NamedQuery(name = "ImpPlanElement.findForAgencyAndCategory",
            query = "select o from ImpPlanElement o where o.agencyCode = :agCode and o.category = :category"),
        @NamedQuery(name = "ImpPlanElement.findForAgencyCategoryAndElement",
            query =
            "select o from ImpPlanElement o where o.agencyCode = :agCode and o.category = :category and o.element = :element")
    })
@Table(name = "IMP_PLAN_ELEMENTS")
public class ImpPlanElement implements Serializable {
    @Column(name = "AGENCY_CODE", nullable = false, unique = true, length = 1)
    private String agencyCode;
    @Column(nullable = false, unique = true, length = 3)
    private String category;
    @Column(name = "CATEGORY_SEQ", nullable = false, unique = true)
    private Long categorySeq;
    @Column(length = 4000)
    private String comments;
    @Column(length = 50)
    private String description;
    @Column(nullable = false, unique = true, length = 2)
    private String element;
    @Id
    @Column(name = "ELEMENT_SEQ", nullable = false)
    private Long elementSeq;
    @Column(name = "FUND_LINK_SEQ")
    private Long fundLinkSeq;
    @Column(name = "TIME_SEQ", nullable = false)
    private Long timeSeq;
    @Column(name = "UPDATED_BY", length = 32)
    private String updatedBy;
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "UPDATED_ON")
    private Date updatedOn;

    public ImpPlanElement() {
    }

    public ImpPlanElement(String agencyCode, String category, Long categorySeq, String comments, String description,
                          String element, Long elementSeq, Long fundLinkSeq, Long timeSeq, String updatedBy,
                          Date updatedOn) {
        this.agencyCode = agencyCode;
        this.category = category;
        this.categorySeq = categorySeq;
        this.comments = comments;
        this.description = description;
        this.element = element;
        this.elementSeq = elementSeq;
        this.fundLinkSeq = fundLinkSeq;
        this.timeSeq = timeSeq;
        this.updatedBy = updatedBy;
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(getClass().getName() + "@" + Integer.toHexString(hashCode()));
        buffer.append('[');
        buffer.append(']');
        return buffer.toString();
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getCategorySeq() {
        return categorySeq;
    }

    public void setCategorySeq(Long categorySeq) {
        this.categorySeq = categorySeq;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public Long getElementSeq() {
        return elementSeq;
    }

    public void setElementSeq(Long elementSeq) {
        this.elementSeq = elementSeq;
    }

    public Long getFundLinkSeq() {
        return fundLinkSeq;
    }

    public void setFundLinkSeq(Long fundLinkSeq) {
        this.fundLinkSeq = fundLinkSeq;
    }

    public Long getTimeSeq() {
        return timeSeq;
    }

    public void setTimeSeq(Long timeSeq) {
        this.timeSeq = timeSeq;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
}
