package org.mtahq.qmg.acep.model;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "ImpPlanCategory.findAll", query = "select o from ImpPlanCategory o"),
        //  Added Filter for 'recent' Categories
        @NamedQuery(name = "ImpPlanCategory.findForAgency",
            query = "select o from ImpPlanCategory o where o.agencyCode = :agCode and o.planSeries in ('4','5','6','7','V')"),
        @NamedQuery(name = "ImpPlanCategory.findForAgencyAndCategory",
            query = "select o from ImpPlanCategory o where o.agencyCode = :agCode and o.category = :category")
    })
@Table(name = "IMP_PLAN_CATEGORIES")
public class ImpPlanCategory implements Serializable {
    @Column(name = "AGENCY_CODE", nullable = false, unique = true, length = 1)
    private String agencyCode;
    @Column(nullable = false, unique = true, length = 3)
    private String category;
    @Id
    @Column(name = "CATEGORY_SEQ", nullable = false)
    private Long categorySeq;
    @Column(nullable = false, length = 50)
    private String description;
    @Column(name = "PLAN_SERIES", nullable = false, length = 1)
    private String planSeries;
    @Column(name = "TIME_SEQ", nullable = false)
    private Long timeSeq;
    @Column(name = "UPDATED_BY", length = 32)
    private String updatedBy;
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "UPDATED_ON")
    private Date updatedOn;

    public ImpPlanCategory() {
    }

    public ImpPlanCategory(String agencyCode, String category, Long categorySeq, String description, String planSeries,
                           Long timeSeq, String updatedBy, Date updatedOn) {
        this.agencyCode = agencyCode;
        this.category = category;
        this.categorySeq = categorySeq;
        this.description = description;
        this.planSeries = planSeries;
        this.timeSeq = timeSeq;
        this.updatedBy = updatedBy;
        this.updatedOn = updatedOn;
        
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(getClass().getName() + "@" + Integer.toHexString(hashCode()));
        buffer.append('[');
        buffer.append(']');
        return buffer.toString();
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getCategorySeq() {
        return categorySeq;
    }

    public void setCategorySeq(Long categorySeq) {
        this.categorySeq = categorySeq;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlanSeries() {
        return planSeries;
    }

    public void setPlanSeries(String planSeries) {
        this.planSeries = planSeries;
    }

    public Long getTimeSeq() {
        return timeSeq;
    }

    public void setTimeSeq(Long timeSeq) {
        this.timeSeq = timeSeq;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
}
