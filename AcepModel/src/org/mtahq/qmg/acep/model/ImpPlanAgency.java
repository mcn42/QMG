package org.mtahq.qmg.acep.model;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "ImpPlanAgency.findAll", query = "select o from ImpPlanAgency o"),
        @NamedQuery(name = "ImpPlanAgency.findForCode",
            query = "select o from ImpPlanAgency o where o.agencyCode = :agCode")
    })
@Table(name = "IMP_PLAN_AGENCIES")
public class ImpPlanAgency implements Serializable {
    @Column(name = "AGENCY_CODE", nullable = false, unique = true, length = 1)
    private String agencyCode;
    @Id
    @Column(name = "AGENCY_SEQ", nullable = false)
    private Long agencySeq;
    @Column(nullable = false, length = 50)
    private String description;
    @Column(name = "LAST_TASK", nullable = false)
    private Integer lastTask;
    @Column(name = "LAST_WAR", nullable = false)
    private Integer lastWar;
    @Column(name = "TIME_SEQ", nullable = false)
    private Long timeSeq;
    @Column(name = "UPDATED_BY", length = 32)
    private String updatedBy;
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "UPDATED_ON")
    private Date updatedOn;

    public ImpPlanAgency() {
    }

    public ImpPlanAgency(String agencyCode, Long agencySeq, String description, Integer lastTask, Integer lastWar,
                         Long timeSeq, String updatedBy, Date updatedOn) {
        this.agencyCode = agencyCode;
        this.agencySeq = agencySeq;
        this.description = description;
        this.lastTask = lastTask;
        this.lastWar = lastWar;
        this.timeSeq = timeSeq;
        this.updatedBy = updatedBy;
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(getClass().getName() + "@" + Integer.toHexString(hashCode()));
        buffer.append('[');
        buffer.append(']');
        return buffer.toString();
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public Long getAgencySeq() {
        return agencySeq;
    }

    public void setAgencySeq(Long agencySeq) {
        this.agencySeq = agencySeq;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getLastTask() {
        return lastTask;
    }

    public void setLastTask(Integer lastTask) {
        this.lastTask = lastTask;
    }

    public Integer getLastWar() {
        return lastWar;
    }

    public void setLastWar(Integer lastWar) {
        this.lastWar = lastWar;
    }

    public Long getTimeSeq() {
        return timeSeq;
    }

    public void setTimeSeq(Long timeSeq) {
        this.timeSeq = timeSeq;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
}
