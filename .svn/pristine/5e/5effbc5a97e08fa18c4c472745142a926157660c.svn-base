package org.mtahq.qmg.view.managed;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import org.mtahq.qmg.entity.MasterQaDeficiencyCode;
import org.mtahq.qmg.entity.QaSubRptDeficiencyCode;
import org.mtahq.qmg.session.QmgSessionEJBLocal;
import org.mtahq.qmg.util.QmgLogger;
import org.mtahq.qmg.view.util.ServiceLocator;

public class DeficiencyCodeBean implements Serializable {
    @SuppressWarnings("compatibility:-290892803664479182")
    private static final long serialVersionUID = -6823069368874908862L;
    private QmgSessionEJBLocal qmgSessionBean = ServiceLocator.getQmgSessionBean();
    private Map<String,List<SelectItem>> codesByTableVersion; 
    private List<SelectItem> tableVersions;
    
    private String currentTableVersion;
    private List<SelectItem> currentCodesList;
    
    public DeficiencyCodeBean() {
        super();
        this.buildMaps();
    }
    
    private void buildMaps() {
        List<MasterQaDeficiencyCode> allCodes = this.qmgSessionBean.getMasterQaDeficiencyCodeFindAll();
        this.codesByTableVersion = new HashMap<String,List<SelectItem>>();
        this.tableVersions = new ArrayList<SelectItem>();
        for(MasterQaDeficiencyCode code:allCodes) {
            String tbl = code.getMasterDeficTableVersion();
            List<SelectItem> items = this.codesByTableVersion.get(tbl);
            if(items == null) {
                items = new ArrayList<SelectItem>();
                this.codesByTableVersion.put(tbl, items);
                SelectItem ver = new SelectItem(tbl,tbl);
                this.tableVersions.add(ver);
            }
            SelectItem item = new SelectItem(code,code.getMasterDeficCodeId() + " - " + code.getDescription(),code.getDescription());
            items.add(item);
        }                                          
    }

    public List<SelectItem> getTableVersions() {
        return tableVersions;
    }

    public void setCurrentTableVersion(String currentTableVersion) {
        this.currentTableVersion = currentTableVersion;
    }

    public String getCurrentTableVersion() {
        return currentTableVersion;
    }

    public List<SelectItem> getCurrentCodesList() {
        return currentCodesList;
    }
    
    public List<SelectItem> getCodesList(String tableName) {
        return this.codesByTableVersion.get(tableName);
    }
    
    public List<MasterQaDeficiencyCode> convertSubreportListToMasterList(List<QaSubRptDeficiencyCode> subList) {
        List<MasterQaDeficiencyCode> mList = new ArrayList<MasterQaDeficiencyCode>();
        if(subList.size() == 0) return mList;
        String tblVersion = subList.get(0).getMasterDeficTableVersion();
        List<SelectItem> version = this.codesByTableVersion.get(tblVersion);
        if(version == null) {
            QmgLogger.getLogger().severe("No Master Deficiency Code Table found for number '" + tblVersion + "'");
            return mList;
        }
        for(QaSubRptDeficiencyCode subCode:subList) {
            MasterQaDeficiencyCode mCode = null;
            for(SelectItem s: version) {
                MasterQaDeficiencyCode code = (MasterQaDeficiencyCode) s.getValue();
                if(code.getMasterDeficCodeId().equals(subCode.getMasterDeficCodeId())) {
                    mCode = code;
                }
            }
            if(mCode == null)
            {
                QmgLogger.getLogger().severe("No Master Deficiency Code found for ID '" + subCode.getMasterDeficCodeId() + "'");
            } else {
                mList.add(mCode);
            }
        }
        return mList;
    }
    
    public List<QaSubRptDeficiencyCode> convertMasterListToSubreportList(List<MasterQaDeficiencyCode> mCodes) {       
        List<QaSubRptDeficiencyCode> sCodes = new ArrayList<QaSubRptDeficiencyCode>();
        if(mCodes == null || mCodes.size() == 0) return sCodes;
        for(MasterQaDeficiencyCode mCode:mCodes) {
            QaSubRptDeficiencyCode code = new QaSubRptDeficiencyCode(mCode);
            sCodes.add(code);
        }
        return sCodes;
    }
}
