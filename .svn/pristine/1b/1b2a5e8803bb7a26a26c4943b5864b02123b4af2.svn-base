package org.mtahq.qmg.view.managed;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.PostActivate;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import javax.servlet.http.HttpSession;

import org.mtahq.qmg.entity.CmisStaff;
import org.mtahq.qmg.entity.ProjectStaff;
import org.mtahq.qmg.entity.QmgProject;
import org.mtahq.qmg.enumerations.RoleCodes;
import org.mtahq.qmg.session.QmgSessionEJBLocal;
import org.mtahq.qmg.util.QmgLogger;
import org.mtahq.qmg.view.util.ServiceLocator;


public class QmgSession implements Serializable {
    @SuppressWarnings("compatibility:1402539903268210575")
    private static final long serialVersionUID = 1L;
    private transient QmgSessionEJBLocal ejbRef = ServiceLocator.getQmgSessionBean();
    
    private String userId;
    private String role;
    private boolean admin;
    
    private CmisStaff staffEntity;
    private List<QmgProject> userProjects = new ArrayList<QmgProject>();
    private List<SelectItem> userProjectItems = new ArrayList<SelectItem>();
    
    private boolean reportAdditionOK;
        
    public QmgSession() {
        super();
    }
    
    @PostActivate
    public void postActivate() {
        if(this.ejbRef == null) this.ejbRef = ServiceLocator.getQmgSessionBean();
    }
       
    //@PostConstruct
    public void postConstruct() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        this.userId = ctx.getExternalContext().getRemoteUser();
        if(this.userId == null) {
            //  unauthorized
            QmgLogger.getLogger().warning("Remote User was NULL");
            //this.userId = "1001273";  //  for testing only
            //return;
        }
        this.userId = this.userId.toUpperCase();
        QmgLogger.getLogger().info("Staff Authentication ID: " + this.userId);
        HttpSession session = (HttpSession)ctx.getExternalContext().getSession(false);
        StringBuffer buff = new StringBuffer();
        buff.append("Session Login:\n");
        buff.append("Session ID:");
        buff.append(session.getId());
        buff.append("\n");
        buff.append("Auth ID:");
        buff.append(this.userId);
        buff.append("\n");
        QmgLogger.getLogger().info(buff.toString());
        //  Check existence in User table and retrieve role
        
        this.retrieveStaffRecord();
    }
    
    
    private void retrieveStaffRecord() {
        try {
            this.staffEntity = this.ejbRef.getCmisStaffFindByBSCID(this.userId);
            //this.staffEntity = this.ejbRef.getCmisStaffFindByUserID(this.userId);
            
            if(this.staffEntity == null) {
                QmgLogger.getLogger().severe(String.format("Staff entity was null for ID '%s'",this.userId));
                LogoutBean.redirectToUnauth();
                return;
            }
            
            this.role = this.staffEntity.getRoleCode();
            //  'None' Role cannot log in, nor can a Login Status other than 'L'
            if(this.role.equals(RoleCodes.NONE.getCode()) || !this.staffEntity.getLoginStatus().equals("L")) {
                QmgLogger.getLogger().severe(String.format("Staff entity with ID '%s' had an invalid Role of '%s'",this.userId,this.role));
                LogoutBean.redirectToUnauth();
                return;
            }
            this.admin = this.role.equals(RoleCodes.ADMINISTRATOR.getCode());
            this.reportAdditionOK = this.role.equals(RoleCodes.ADMINISTRATOR.getCode()) || this.role.equals(RoleCodes.QA_MANAGER.getCode());
            QmgLogger.getLogger().info(String.format("Staff Entity: '%s'",this.staffEntity.toString()));
            for(ProjectStaff ps:this.staffEntity.getProjectStaffList()) {
                this.userProjects.add(ps.getQmgProject());
                SelectItem si = new SelectItem(ps.getQmgProject());
                this.userProjectItems.add(si);
            }
            
            //  Switch BSC ID to user ID after BSC authentication
            this.userId = this.staffEntity.getUserId().toUpperCase();
        }
        catch(Exception e) {
            QmgLogger.getLogger().log(Level.SEVERE,"Cmis Staff record retrieval error",e);
            LogoutBean.redirectToUnauth();
        }
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isAdmin() {
        return admin;
    }

    public List<QmgProject> getUserProjects() {
        return userProjects;
    }

    public List<SelectItem> getUserProjectItems() {
        return userProjectItems;
    }

    public CmisStaff getStaffEntity() {
        //if(this.staffEntity == null) this.retrieveStaffRecord();
        return staffEntity;
    }

    public boolean isReportAdditionOK() {
        return reportAdditionOK;
    }
}
