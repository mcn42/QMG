package org.mtahq.qmg.view.util;

import java.text.DateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;

import javax.el.ValueExpression;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.servlet.http.Cookie;

import oracle.adf.view.rich.context.AdfFacesContext;

import org.mtahq.qmg.entity.CmisStaff;
import org.mtahq.qmg.entity.CorpQaMainReport;
import org.mtahq.qmg.util.QmgLogger;

public class Utils {
    private Utils() {
        super();
    }
    private static FacesContext ctx;
    private static DateFormat tf = DateFormat.getTimeInstance(DateFormat.MEDIUM);
    private static DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
    private static Logger logger = QmgLogger.getLogger();
    private static HashMap<String, CmisStaff> staffByName = null;

    public static void setDf(DateFormat df) {
        Utils.df = df;
    }

    public static DateFormat getDf() {
        return df;
    }

    public static void addErrorMessage(String message, Throwable t) {
        ctx = FacesContext.getCurrentInstance();
        ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message + ":\n" + t.getMessage()));
    }
    
    public static void addErrorMessage(String message, String detail) {
        ctx = FacesContext.getCurrentInstance();
        ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, detail));
    }

    public static void addNumberRelationErrorMessage(String message, String details) {
        ctx = FacesContext.getCurrentInstance();
        ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message + ":\n" + details));
    }

    public static void addWarningMessage(String message, String details) {
        ctx = FacesContext.getCurrentInstance();
        ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, message, message + "\n" + details));
    }

    public static void addInfoMessage(String message, String details) {
        ctx = FacesContext.getCurrentInstance();
        ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, message + "\n" + details));
    }

    public static String getCurrentTimeString() {
        return tf.format(new Date());
    }

    public static String formatMediumDate(Date d) {
        return df.format(d);
    }

    public Date getToday() {
        GregorianCalendar cal = new GregorianCalendar();
        cal.set(Calendar.HOUR, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    public static void logCookies(Cookie[] cookies) {
        StringBuffer buff = new StringBuffer();
        buff.append("Cookies:\n");
        for (Cookie c : cookies) {
            buff.append("Name: ");
            buff.append(c.getName());
            buff.append(", Value: ");
            buff.append(c.getValue());
            buff.append(", Path: ");
            buff.append(c.getPath());
            buff.append("\n");
        }
        logger.info(buff.toString());
    }

    public static String getDefaultPassword(String userId) {
        String pwd = userId.toUpperCase();
        while (pwd.length() < 8) {
            pwd = pwd + "*";
        }
        logger.fine("Default password for '" + userId + "' = '" + pwd + "'");
        return pwd;
    }

    public static int getMonthForJavaMonth(int jm) {
        int mo = -1;
        switch (jm) {
        case Calendar.JANUARY:
            mo = 1;
            break;
        case Calendar.FEBRUARY:
            mo = 2;
            break;
        case Calendar.MARCH:
            mo = 3;
            break;
        case Calendar.APRIL:
            mo = 4;
            break;
        case Calendar.MAY:
            mo = 5;
            break;
        case Calendar.JUNE:
            mo = 6;
            break;
        case Calendar.JULY:
            mo = 7;
            break;
        case Calendar.AUGUST:
            mo = 8;
            break;
        case Calendar.SEPTEMBER:
            mo = 9;
            break;
        case Calendar.OCTOBER:
            mo = 10;
            break;
        case Calendar.NOVEMBER:
            mo = 11;
            break;
        case Calendar.DECEMBER:
            mo = 12;
            break;
        }
        return mo;
    }

    public static String getContextParameter(String name) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        return ctx.getExternalContext().getInitParameter(name);
    }

    public static Integer getStaffIdForName(String fullName) {
        if (staffByName == null) {
            FacesContext ctx = FacesContext.getCurrentInstance();
            ValueExpression ve =
                ctx.getApplication().getExpressionFactory().createValueExpression(ctx.getELContext(),
                                                                                  "#{AppInfo.qamStaffByName}",
                                                                                  HashMap.class);
            staffByName = (HashMap<String, CmisStaff>) ve.getValue(ctx.getELContext());
        }
        CmisStaff staff = staffByName.get(fullName);
        if (staff == null) {
            QmgLogger.getLogger().severe(String.format("Staff record not found for name '%s'", fullName));
            return -1;
        }
        return staff.getCmisStaffId();
    }

    public static CmisStaff getCurrentStaffEntity() {

        FacesContext ctx = FacesContext.getCurrentInstance();
        CmisStaff staff = null;
        ValueExpression ve =
            ctx.getApplication().getExpressionFactory().createValueExpression(ctx.getELContext(),
                                                                              "#{QmgSession.staffEntity}",
                                                                              CmisStaff.class);
        staff = (CmisStaff) ve.getValue(ctx.getELContext());

        if (staff == null) {
            QmgLogger.getLogger().severe("Current Staff record was NULL");
        }
        return staff;
    }

    public static boolean isOwner(CorpQaMainReport report, CmisStaff staff) {
        return report.getOwner().equals(staff);
    }

    //  Refreshes a component when a change is made in code or within a region
    public static void refreshComponent(String componentId) {
        FacesContext facesCtx = FacesContext.getCurrentInstance();
        //UIComponent comp = facesCtx.getViewRoot().findComponent(componentId);
        UIComponent comp = findComponentInRoot(componentId);
        if (comp != null)
            AdfFacesContext.getCurrentInstance().addPartialTarget(comp);
    }

    /**
     * Locate an UIComponent in view root with its component id. Use a recursive way to achieve this.
     * Taken from http://www.jroller.com/page/mert?entry=how_to_find_a_uicomponent
     * @param id UIComponent id
     * @return UIComponent object
     */
    public static UIComponent findComponentInRoot(String id) {
        UIComponent component = null;

        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (facesContext != null) {
            UIComponent root = facesContext.getViewRoot();
            component = findComponent(root, id,0);
        }
        return component;
    }

    /**
     * Locate an UIComponent from its root component.
     * Taken from http://www.jroller.com/page/mert?entry=how_to_find_a_uicomponent
     * @param base root Component (parent)
     * @param id UIComponent id
     * @return UIComponent object
     */
    public static UIComponent findComponent(UIComponent base, String id, int tab) {
        if (id.equals(base.getId()))
            return base;

        UIComponent children = null;
        UIComponent result = null;
        Iterator childrens = base.getFacetsAndChildren();
        while (childrens.hasNext() && (result == null)) {
            children = (UIComponent) childrens.next();
            
//            for(int i = 0;i < tab;i++) {
//                System.out.print("-");
//            }
//            System.out.println(children.getId());
            
            if (id.equals(children.getId())) {
                result = children;
                break;
            }
            result = findComponent(children, id,++tab);
            if (result != null) {
                break;
            }
        }
        return result;
    }
}
