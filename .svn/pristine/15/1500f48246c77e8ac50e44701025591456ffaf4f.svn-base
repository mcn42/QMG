<?xml version='1.0' encoding='UTF-8'?>
<ui:composition xmlns:ui="http://java.sun.com/jsf/facelets" xmlns:af="http://xmlns.oracle.com/adf/faces/rich"
                xmlns:f="http://java.sun.com/jsf/core">
  <af:panelGroupLayout layout="vertical" id="pgl1" styleClass="AFStretchWidth">
    <af:panelTabbed id="ph1" styleClass="AFStretchWidth">
      <af:showDetailItem id="sdi1" text="Report Search">
        <af:panelGroupLayout id="pgl2" layout="vertical">
          <af:panelFormLayout id="pfl1">
            <f:facet name="footer">
              <af:toolbar id="t2" inlineStyle="float: right;">
                <af:button text="Clear" id="b5" action="#{ReportList.clearSearch_action}"/>
                <af:button text="Search" id="b4" action="#{ReportList.performSearchAction}"/>
              </af:toolbar>
            </f:facet>
            <af:selectOneChoice label="Report Type" id="soc2" value="#{ReportList.searchReportType}"
                                partialTriggers="b5">
              <f:selectItems value="#{AppInfo.reportTypeItems}" id="si2"/>
            </af:selectOneChoice>
            <af:inputText label="Report No." id="it3" value="#{ReportList.reportNo}"/>
            <af:panelLabelAndMessage label="ACEP/Task" id="plam1">
              <af:panelGroupLayout id="pgl4" layout="horizontal">
                <af:button text="Select ACEP" id="b7" action="acep"/>
                <af:spacer width="10" height="10" id="s2"/>
                <af:inputText label="Label 1" id="it5" simple="true" readOnly="true" partialTriggers="b5 b7"
                              value="#{ReportList.acepInfo == null?'':pageFlowScope.ReportList.acepInfo}"/>
              </af:panelGroupLayout>
            </af:panelLabelAndMessage>
            <af:inputDate label="Start Date" id="id2" value="#{ReportList.startDate}" usage="search">
              <af:convertDateTime type="date" hintDate="#{AppInfo.constants['DATE_HINT_TEXT']}"/>
            </af:inputDate>
            <af:inputDate label="End Date" id="id3" value="#{ReportList.endDate}" usage="search">
              <af:convertDateTime type="date" hintDate="#{AppInfo.constants['DATE_HINT_TEXT']}"/>
            </af:inputDate>
            <af:inputText label="QAM" id="it4" value="#{ReportList.qamName}">
              <af:autoSuggestBehavior suggestedItems="#{ReportList.staff_suggestItems}"/>
            </af:inputText>
          </af:panelFormLayout>
          <af:spacer width="10" height="10" id="s1"/>
          <af:panelCollection id="pc1" styleClass="AFStretchWidth">
            <f:facet name="menus"/>
            <f:facet name="toolbar">
              <af:toolbar id="t1">
                <af:button text="Deactivate Selected" id="b3" rendered="false"/>
                <af:group id="g1">
                  <af:button text="Add New Report" id="b1" action="detail" disabled="#{!QmgSession.reportAdditionOK}">
                    <af:showPopupBehavior popupId="::p1"/>
                  </af:button>
                  <af:button text="Download Excel" id="b2">
                    <af:exportCollectionActionListener type="excelHTML" exportedId="resId1"
                                                       filename="ReportSearchResults.xls" title="QMG Report List"/>
                  </af:button>
                </af:group>
              </af:toolbar>
            </f:facet>
            <f:facet name="statusbar"/>
            <af:table var="row" rows="25" emptyText="No matching Reports" rowBandingInterval="1" rowSelection="single"
                      fetchSize="25" queryListener="#{bindings.MainReportCriteriaSet1Query.processQuery}"
                      filterVisible="true" varStatus="vs" id="resId1" columnStretching="last"
                      value="#{ReportList.searchResults}" summary="Search Results"
                      partialTriggers=":::b4 :::b5">
              <af:column sortProperty="#{bindings.corpQaMainReportFindAll.hints.mainReportNum.name}" sortable="true"
                         headerText="Report Num." id="resId1c2" rowHeader="true">
                <af:link text="#{row.mainReportNum}" id="l1" action="detail">
                  <af:setPropertyListener from="#{row}" to="#{ReportList.selectedReport}" type="action"/>
                </af:link>
              </af:column>
              <af:column sortProperty="reportType" sortable="true" headerText="Type" id="resId1c1">
                <af:outputText value="#{row.mainReportType}" id="ot1"/>
              </af:column>
              <af:column sortProperty="reportTitle" sortable="true" headerText="Report Title" id="resId1c3" width="300">
                <af:outputText value="#{row.reportTitle}" id="ot3"/>
              </af:column>
              <af:column sortProperty="projectnum.}" sortable="true" headerText="ACEP" id="resId1c4">
                <af:outputText value="#{row.acepString}" id="ot4"/>
              </af:column>
              <af:column sortProperty="taskNum" sortable="true" headerText="Task Num." id="resId1c5">
                <af:outputText value="#{row.task}" id="ot5"/>
              </af:column>
              <af:column id="c1" headerText="QAM">
                <af:outputText value="#{row.owner.fullName}" id="ot2"/>
              </af:column>
              <af:column sortProperty="createDate" sortable="true" headerText="Date Created" id="resId1c6">
                <af:outputText value="#{row.createDate}"
                               shortDesc="#{bindings.corpQaMainReportFindAll.hints.createDate.tooltip}" id="ot6">
                  <af:convertDateTime pattern="MMM/dd/yyyy"/>
                </af:outputText>
              </af:column>
            </af:table>
          </af:panelCollection>
        </af:panelGroupLayout>
      </af:showDetailItem>
      <af:showDetailItem text="Quick Search" id="sdi2" disclosed="#{ReportList.quickSearchSelected}">
        <af:panelFormLayout id="pfl2">
          <f:facet name="footer">
            <af:toolbar id="t3" inlineStyle="float: right;">
              <af:button text="Edit Report" id="b6" action="#{ReportList.quickEdit_action}"/>
            </af:toolbar>
          </f:facet>
          <af:selectOneChoice label="Report Type" value="#{ReportList.searchReportType}"
                              valueChangeListener="#{ReportList.reportType_valueChangeListener}"
                              id="soc3" autoSubmit="true" unselectedLabel="Select a Report Type"
                              showRequired="true">
            <f:selectItems value="#{AppInfo.reportTypeItems}" id="si3"/>
          </af:selectOneChoice>
          <af:selectOneChoice label="Year" value="#{ReportList.year}"
                              valueChangeListener="#{ReportList.year_valueChangeListener}" id="soc4"
                              autoSubmit="true" partialTriggers="soc3"
                              unselectedLabel="Select a Report Year" showRequired="true">
            <f:selectItems value="#{ReportList.currentYearItems}" id="si4"/>
          </af:selectOneChoice>
          <af:selectOneChoice label="Sequence" value="#{ReportList.sequence}" id="soc5"
                              partialTriggers="soc4" unselectedLabel="Select a Sequence" showRequired="true">
            <f:selectItems value="#{ReportList.reportSeqItems}" id="si5"/>
          </af:selectOneChoice>
        </af:panelFormLayout>
      </af:showDetailItem>
    </af:panelTabbed>
    <af:popup childCreation="deferred" autoCancel="disabled" id="p1">
      <af:dialog id="d1" title="Select Main Report Type" affirmativeTextAndAccessKey="Continue"
                 dialogListener="#{ReportList.reportType_dialogListener}">
        <f:facet name="buttonBar"/>
        <af:panelGroupLayout id="pgl3">
          <af:selectOneRadio label="Main Report Type" value="#{ReportList.reportType}" id="sor1">
            <f:selectItems value="#{AppInfo.addableReportTypeItems}" id="si6"/>
          </af:selectOneRadio>
        </af:panelGroupLayout>
      </af:dialog>
    </af:popup>
  </af:panelGroupLayout>
  <!--oracle-jdev-comment:preferred-managed-bean-name:ReportList-->
</ui:composition>
