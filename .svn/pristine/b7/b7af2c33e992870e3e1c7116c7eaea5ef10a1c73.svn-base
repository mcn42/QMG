package org.mtahq.qmg.view.managed;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import javax.annotation.PostConstruct;

import javax.el.ValueExpression;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import org.mtahq.qmg.acep.AcepInfo;
import org.mtahq.qmg.acep.model.ImpPlanAgency;
import org.mtahq.qmg.acep.model.ImpPlanCategory;
import org.mtahq.qmg.acep.model.ImpPlanElement;
import org.mtahq.qmg.acep.model.ImpPlanProject;
import org.mtahq.qmg.acep.model.ImpPlanTask;
import org.mtahq.qmg.acep.session.AcepSessionEJBLocal;
import org.mtahq.qmg.entity.QmgProject;
import org.mtahq.qmg.util.QmgLogger;
import org.mtahq.qmg.view.util.ServiceLocator;
import org.mtahq.qmg.view.util.Utils;

public class AcepBean implements Serializable {
    @SuppressWarnings("compatibility:8403220070114115898")
    private static final long serialVersionUID = 1L;
    private static final String LIRR_AGENCY_CODE = "L";

    private AcepSessionEJBLocal acepEjb = ServiceLocator.getAcepSessionBean();
    private TreeSet<String> allChecklistProjects = null;
    private String agencyCode;
    private String category;
    private String element;
    private String project;
    private String task;

    private String selectionString = "";

    private ImpPlanTask ipTask;
    private ImpPlanAgency ipAgency;
    private ImpPlanCategory ipCategory;
    private ImpPlanElement ipElement;
    private ImpPlanProject ipProject;
    
    private ImpPlanAgency lirr = null;

    private List<SelectItem> planAgencyItems = new ArrayList<SelectItem>();
    private List<SelectItem> planCategoryItems = new ArrayList<SelectItem>();
    private List<SelectItem> planElementItems = new ArrayList<SelectItem>();
    private List<SelectItem> planProjectItems = new ArrayList<SelectItem>();
    private List<SelectItem> planTaskItems = new ArrayList<SelectItem>();

    private List<SelectItem> userTaskItems = new ArrayList<SelectItem>();

    private List<SelectItem> filteredPlanTaskItems = new ArrayList<SelectItem>();
    private List<SelectItem> projectSuggestItems = new ArrayList<SelectItem>();
    private String projectNumber = "PN";

    private boolean projectOnly = false;
    private boolean allowingPartial;
    private boolean cancelled = false;

    private boolean acepActive;

    private QmgProject selectedProject;
    private ImpPlanTask userTask;

    public AcepBean() {
        super();
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("Building Agency list");
        List<ImpPlanAgency> agencies = this.acepEjb.getImpPlanAgencyFindAll();
        for (ImpPlanAgency agy : agencies) {
            SelectItem si = new SelectItem(agy, agy.getDescription(), agy.getAgencyCode());
            this.planAgencyItems.add(si);
            //  Select LIRR as default Agency
            if (agy.getAgencyCode().equalsIgnoreCase(LIRR_AGENCY_CODE)) {
                this.ipAgency = agy;
                this.lirr = agy;
            }
                
        }
        QmgLogger.getLogger().fine(this.planAgencyItems.size() + " Agencies found");

        if (this.ipAgency == null) {
            this.ipAgency = agencies.get(0);
        }
        FacesContext ctx = FacesContext.getCurrentInstance();
        ValueExpression ve =
            ctx.getApplication().getExpressionFactory().createValueExpression(ctx.getELContext(),
                                                                              "#{AppInfo.allChecklistProjects}",
                                                                              TreeSet.class);
        this.allChecklistProjects = (TreeSet<String>) ve.getValue(ctx.getELContext());
        this.buildCategoryItemList();
        this.refreshSelectionString();
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getElement() {
        return element;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getProject() {
        return project;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getTask() {
        return task;
    }

    public void setIpTask(ImpPlanTask ipTask) {
        this.ipTask = ipTask;
    }

    public ImpPlanTask getIpTask() {
        return ipTask;
    }

    public void setIpAgency(ImpPlanAgency ipAgency) {
        this.ipAgency = ipAgency;
    }

    public ImpPlanAgency getIpAgency() {
        return ipAgency;
    }

    public void setIpCategory(ImpPlanCategory ipCategory) {
        this.ipCategory = ipCategory;
    }

    public ImpPlanCategory getIpCategory() {
        return ipCategory;
    }

    public void setIpElement(ImpPlanElement ipElement) {
        this.ipElement = ipElement;
    }

    public ImpPlanElement getIpElement() {
        return ipElement;
    }

    public void setIpProject(ImpPlanProject ipProject) {
        this.ipProject = ipProject;
    }

    public ImpPlanProject getIpProject() {
        return ipProject;
    }

    public List<SelectItem> getPlanAgencyItems() {
        return planAgencyItems;
    }

    public List<SelectItem> getPlanCategoryItems() {
        return planCategoryItems;
    }

    public List<SelectItem> getPlanElementItems() {
        return planElementItems;
    }

    public List<SelectItem> getPlanProjectItems() {
        return planProjectItems;
    }

    public List<SelectItem> getPlanTaskItems() {
        return planTaskItems;
    }

    public String getSelectionString() {
        return selectionString;
    }

    public List<SelectItem> getFilteredPlanTaskItems() {
        return filteredPlanTaskItems;
    }

    public List<SelectItem> getProjectSuggestItems() {
        return projectSuggestItems;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectOnly(boolean projectOnly) {
        this.projectOnly = projectOnly;
    }

    public boolean isProjectOnly() {
        return projectOnly;
    }

    public void setAcepActive(boolean acepActive) {
        this.acepActive = acepActive;
    }

    public boolean isAcepActive() {
        return acepActive;
    }

    public List<SelectItem> getUserTaskItems() {
        return userTaskItems;
    }

    public void setSelectedProject(QmgProject selectedProject) {
        this.selectedProject = selectedProject;
    }

    public QmgProject getSelectedProject() {
        return selectedProject;
    }

    public void setUserTask(ImpPlanTask userTask) {
        this.userTask = userTask;
    }

    public ImpPlanTask getUserTask() {
        return userTask;
    }

    public void setAllowingPartial(boolean allowingPartial) {
        this.allowingPartial = allowingPartial;
    }

    public boolean isAllowingPartial() {
        return allowingPartial;
    }

    private void buildCategoryItemList() {
        this.planCategoryItems.clear();
        this.planElementItems.clear();
        this.planProjectItems.clear();
        this.planTaskItems.clear();
        List<ImpPlanCategory> cats = this.acepEjb.getImpPlanCategoryFindForAgency(this.ipAgency.getAgencyCode());
        for (ImpPlanCategory cat : cats) {
            SelectItem si =
                new SelectItem(cat, String.format("%s - %s", cat.getCategory(), cat.getDescription()),
                               cat.getCategory());
            this.planCategoryItems.add(si);
        }

    }

    private void buildElementItemList() {
        this.planElementItems.clear();
        this.planProjectItems.clear();
        this.planTaskItems.clear();
        List<ImpPlanElement> elems =
            this.acepEjb.getImpPlanElementFindForAgencyAndCategory(this.ipAgency.getAgencyCode(),
                                                                   this.ipCategory.getCategory());
        for (ImpPlanElement elem : elems) {
            SelectItem si =
                new SelectItem(elem, String.format("%s - %s", elem.getElement(), elem.getDescription()),
                               elem.getElement());
            this.planElementItems.add(si);
        }
    }

    private void buildProjectItemList() {
        this.planProjectItems.clear();
        this.planTaskItems.clear();
        List<ImpPlanProject> projs =
            this.acepEjb.getImpPlanProjectFindForAgencyCategoryAndElement(this.ipAgency.getAgencyCode(),
                                                                          this.ipCategory.getCategory(),
                                                                          this.ipElement.getElement());
        for (ImpPlanProject proj : projs) {
            SelectItem si =
                new SelectItem(proj, String.format("%s - %s", proj.getProject(), proj.getDescription()),
                               proj.getProject());
            this.planProjectItems.add(si);
        }
        this.filterProjects(projs);
    }

    private void buildTaskItemList() {
        this.planTaskItems.clear();
        List<ImpPlanTask> tasks =
            this.acepEjb.getImpPlanTaskFindForACEP(this.ipAgency.getAgencyCode(), this.ipCategory.getCategory(),
                                                   this.ipElement.getElement(), this.ipProject.getProject());
        for (ImpPlanTask task : tasks) {
            SelectItem si =
                new SelectItem(task, String.format("%s - %s", task.getTaskNumber(), task.getDescription()),
                               task.getDescription());
            this.planTaskItems.add(si);
        }
    }

    private void buildUserTaskItemList() {
        this.userTaskItems.clear();
        List<ImpPlanTask> tasks =
            this.acepEjb.getImpPlanTaskFindForACEP(this.selectedProject.getAgency(), this.selectedProject.getCategory(),
                                                   this.selectedProject.getElement(),
                                                   this.selectedProject.getProject());
        for (ImpPlanTask task : tasks) {
            SelectItem si =
                new SelectItem(task, String.format("%s - %s", task.getTaskNumber(), task.getDescription()),
                               task.getDescription());
            this.userTaskItems.add(si);
        }
    }

    public void agency_valueChangeListener(ValueChangeEvent vce) {
        this.ipAgency = (ImpPlanAgency) vce.getNewValue();
        if (this.ipAgency != null)
            this.buildCategoryItemList();
        this.refreshSelectionString();
    }

    public void category_valueChangeListener(ValueChangeEvent vce) {
        this.ipCategory = (ImpPlanCategory) vce.getNewValue();
        if (this.ipCategory != null)
            this.buildElementItemList();
        this.refreshSelectionString();
    }

    public void element_valueChangeListener(ValueChangeEvent vce) {
        this.ipElement = (ImpPlanElement) vce.getNewValue();
        if (this.ipElement != null)
            this.buildProjectItemList();
        this.refreshSelectionString();
    }

    public void project_valueChangeListener(ValueChangeEvent vce) {
        this.ipProject = (ImpPlanProject) vce.getNewValue();
        if (this.ipProject != null)
            this.buildTaskItemList();
        this.refreshSelectionString();
    }

    public String selectProject_action() {
        this.ipTask = null;
        return "return";
    }

    public String select_actionProjAndTask() {
        return "return";
    }

    public AcepInfo getAcepInfo() {
        if(this.cancelled == true) {
            this.cancelled = false;
            return null;
        }
        if(!this.allowingPartial)
        {
            if(this.ipAgency == null || this.ipCategory == null || this.ipElement == null || this.ipProject == null) {
                //Utils.addWarningMessage("A full ACEP must be provided.", "Please select a full ACEP.");
                return null;
            }           
        }
        if(this.ipAgency == null && this.allowingPartial)
        {
            Utils.addWarningMessage("The Agency must be provided at a minimum.", "Please select an Agency.");
            return null;
        }
        AcepInfo ai =
            new AcepInfo(this.ipAgency.getAgencyCode(), this.ipCategory.getCategory(), this.ipElement.getElement(),
                         this.ipProject.getProject(), this.ipProject.getDescription());
        if (this.ipTask != null) {
            ai.setTask(this.ipTask.getTaskNumber());
            ai.setTaskDescription(this.ipTask.getDescription());
        }
        return ai;
    }

    private void refreshSelectionString() {
        StringBuffer buff = new StringBuffer();
        if (this.ipAgency != null) {
            buff.append(this.ipAgency.getAgencyCode());
            buff.append("/");
        }
        if (this.ipCategory != null) {
            buff.append(this.ipCategory.getCategory());
            buff.append("/");
        }
        if (this.ipElement != null) {
            buff.append(this.ipElement.getElement());
            buff.append("/");
        }
        if (this.ipProject != null) {
            buff.append(this.ipProject.getProject());
        }
        if (this.ipTask != null) {
            buff.append(" - ");
            buff.append(this.ipTask.getTaskNumber());
        }
        this.selectionString = buff.toString();
    }

    public String projectChecklist_action() {
        if (this.ipProject == null) {
            FacesContext.getCurrentInstance().addMessage(null,
                                                         new FacesMessage("No Project is selected",
                                                                          "Please select a Project"));
            return null;
        }
        this.project = this.ipProject.getProject();
        QmgLogger.getLogger().fine(String.format("Attempting to retrieve Project Checklist for Project '%s'",
                                                 this.project));
        return "checklist";
    }

    private void filterProjects(List<ImpPlanProject> projects) {
        this.filteredPlanTaskItems.clear();
        for (ImpPlanProject proj : projects) {
            if (this.allChecklistProjects.contains("PN" + proj.getProject())) {
                SelectItem item =
                    new SelectItem(proj, String.format("%s - %s", proj.getProject(), proj.getDescription()),
                                   proj.getProject());
                this.filteredPlanTaskItems.add(item);
            }
        }
        QmgLogger.getLogger().fine("Matching Checklist Projects: " + this.filteredPlanTaskItems.size());
    }

    public void setAllChecklistProjects(TreeSet<String> allChecklistProjects) {
        this.allChecklistProjects = allChecklistProjects;
    }

    public TreeSet<String> getAllChecklistProjects() {
        return allChecklistProjects;
    }


    public List<SelectItem> project_suggestItems(String string) {
        this.projectSuggestItems.clear();
        for (String s : this.allChecklistProjects) {
            if (s.startsWith(string)) {
                SelectItem si = new SelectItem(s);
                this.projectSuggestItems.add(si);
            }
        }
        return this.projectSuggestItems;
    }

    public String projNumSearch_action() {
        return "checklist";
    }

    public void acep_dialogListener(DialogEvent dialogEvent) {
        // Add event code here...
    }

    public void acepPopup_fetchListener(PopupFetchEvent popupFetchEvent) {
        //  no op
    }

    public AcepInfo getUserAcepInfo() {
        if(this.selectedProject == null) return null;
        ImpPlanProject prj = this.acepEjb.getImpPlanProjectFindForAgencyCategoryElementAndProject(this.selectedProject.getAgency(),
                                                                                                  this.selectedProject.getCategory(),
                                                                                                  this.selectedProject.getElement(),
                                                                                                  this.selectedProject.getProject());
        if(prj == null) {
            QmgLogger.getLogger().severe(String.format("No Impact Project record found for QmgProject'%s'",
                                                     this.selectedProject));
            return null;
        }
        String desc = prj == null?"":prj.getDescription();
        AcepInfo ai =
            new AcepInfo(this.selectedProject.getAgency(), this.selectedProject.getCategory(), this.selectedProject.getElement(),
                         this.selectedProject.getProject(),desc);
        if (this.userTask != null) {
            ai.setTask(this.userTask.getTaskNumber());
            ai.setTaskDescription(this.userTask.getDescription());
        }
        return ai;
    }

    public void userProject_valueChangeListener(ValueChangeEvent vce) {
        QmgProject prj = (QmgProject) vce.getNewValue();
        this.selectedProject = prj;
        if (prj != null) {
            this.buildUserTaskItemList();
        }
    }

    public String cancelUserProject_action() {
        this.selectedProject = null;
        this.userTask = null;

        return "return";
    }

    public String selectUserProject_action() {
        this.userTask = null;
        return "return";
    }

    public String selectUserProjTask_action() {
        // Add event code here...
        return "return";
    }

    public String cancelSelect_action() {
        this.ipAgency = this.lirr;
        this.ipCategory = null;
        this.ipElement = null;
        this.ipProject = null;
        this.ipTask = null;
        this.selectionString = "";
        this.cancelled = true;
        return "return";
    }
}
