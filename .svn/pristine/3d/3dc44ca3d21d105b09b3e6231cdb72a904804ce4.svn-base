package org.mtahq.qmg.view.managed;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import oracle.adf.view.rich.event.DialogEvent;

import org.mtahq.qmg.beans.SequenceBeanInterface;
import org.mtahq.qmg.entity.CorpQaMainReport;
import org.mtahq.qmg.entity.CorpQaSubReport;
import org.mtahq.qmg.entity.MasterQaDeficiencyCode;
import org.mtahq.qmg.entity.QaSubRptDeficiencyCode;
import org.mtahq.qmg.session.QmgSessionEJBLocal;
import org.mtahq.qmg.util.QmgLogger;
import org.mtahq.qmg.view.util.ServiceLocator;
import org.mtahq.qmg.view.util.Utils;

public class SubreportBean implements Serializable {
    private String subreportType;
    private CorpQaSubReport subreport;
    private CorpQaMainReport report;
    private String tableVersion;
    private String deficiencyCode;
    private List<SelectItem> deficiencyCodeItems;
    private String userId;

    private QmgSessionEJBLocal ejbRef = ServiceLocator.getQmgSessionBean();
    private SequenceBeanInterface sequenceBean = ServiceLocator.getSequenceBean();

    private DeficiencyCodeBean defcodeBean;

    private List<MasterQaDeficiencyCode> masterDefCodes;
    private String defCodeString = "";
    private Set<QaSubRptDeficiencyCode> oldCodes = new HashSet<QaSubRptDeficiencyCode>();
    
    private boolean useDispo = true;
    private boolean useMatl = false;

    private boolean disabled = true;

    public SubreportBean() {
        super();
    }

    public void setDefcodeBean(DeficiencyCodeBean defcodeBean) {
        this.defcodeBean = defcodeBean;
    }

    public DeficiencyCodeBean getDefcodeBean() {
        return defcodeBean;
    }

    public void setSubreportType(String subreportType) {
        this.subreportType = subreportType;
    }

    public String getSubreportType() {
        return subreportType;
    }

    public void setSubreport(CorpQaSubReport subreport) {
        this.subreport = subreport;
        if (this.subreport != null) {
            this.subreportType = this.subreport.getSubReportType();
            this.tableVersion = this.report.getMasterDeficTableVersion();
            this.masterDefCodes = this.defcodeBean.convertSubreportListToMasterList(subreport.getDeficiencyCodes());
            if (this.tableVersion != null) {
                this.deficiencyCodeItems = this.defcodeBean.getCodesList(this.tableVersion);
                buildDefCodeString();
            } else {
                this.deficiencyCodeItems = new ArrayList<SelectItem>();
            }

        }
    }
    
    private void clearOldCodes() {
        for(QaSubRptDeficiencyCode code:this.oldCodes) {
            if(code.getId() != null) {
                this.ejbRef.removeQaSubRptDeficiencyCode(code);
            }
        }
        this.oldCodes.clear();
    }

    private void buildDefCodeString() {
        StringBuffer buff = new StringBuffer();
        for (QaSubRptDeficiencyCode sr : this.subreport.getDeficiencyCodes()) {
            buff.append(sr.getMasterDeficCodeId());
            buff.append(", ");
        }
        this.defCodeString = buff.toString();
        if (this.defCodeString.length() > 2)
            this.defCodeString = this.defCodeString.substring(0, this.defCodeString.length() - 2);
    }

    public CorpQaSubReport getSubreport() {
        return subreport;
    }

    public void setTableVersion(String tableVersion) {
        this.tableVersion = tableVersion;
    }

    public String getTableVersion() {
        return tableVersion;
    }

    public void setDeficiencyCode(String deficiencyCode) {
        this.deficiencyCode = deficiencyCode;
    }

    public String getDeficiencyCode() {
        return deficiencyCode;
    }

    public List<SelectItem> getDeficiencyCodeItems() {
        return deficiencyCodeItems;
    }

    public void setMasterDefCodes(List<MasterQaDeficiencyCode> masterDefCodes) {
        this.masterDefCodes = masterDefCodes;
    }

    public List<MasterQaDeficiencyCode> getMasterDefCodes() {
        return masterDefCodes;
    }

    public String getDefCodeString() {
        return defCodeString;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setReport(CorpQaMainReport report) {
        this.report = report;
    }

    public CorpQaMainReport getReport() {
        return report;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setUseDispo(boolean useDispo) {
        this.useDispo = useDispo;
    }

    public boolean isUseDispo() {
        return useDispo;
    }

    public void setUseMatl(boolean useMatl) {
        this.useMatl = useMatl;
    }

    public boolean isUseMatl() {
        return useMatl;
    }

    public void tableVersion_valueChangeListener(ValueChangeEvent vce) {
        this.tableVersion = (String) vce.getNewValue();
        if (this.tableVersion != null) {
            this.deficiencyCodeItems = this.defcodeBean.getCodesList(this.tableVersion);
        } else {
            this.deficiencyCodeItems = new ArrayList<SelectItem>();
        }
    }

    public void deficiencyCode_dialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.no)
            return;
        List<QaSubRptDeficiencyCode> codes = new ArrayList<QaSubRptDeficiencyCode>();
        codes.addAll(this.subreport.getDeficiencyCodes());
        for(QaSubRptDeficiencyCode code:codes) {
            code.setMainReportNum(null);
            code.setSubReportNum(null);
        }
        this.oldCodes.addAll(this.subreport.getDeficiencyCodes());
        this.subreport.getDeficiencyCodes().clear();
        codes = this.defcodeBean.convertMasterListToSubreportList(this.masterDefCodes);
        Date d = new Date();
        //  Prep code entities for Insert
        for(QaSubRptDeficiencyCode code:codes) {
            code.setCreateDate(d);
            code.setCreateUserId(this.userId);
            code.setLastModDate(d);
            code.setLastModUserId(this.userId);
            code.setMainReportNum(this.report.getMainReportNum());
            
            code.setSubReportNum(this.subreport.getSubReportNum());
        }
        this.subreport.setDeficiencyCodes(codes);
        this.buildDefCodeString();
        Utils.refreshComponent("it12");
    }

    public String saveSubreport_action() {
        boolean ok = this.validateDeficiencyCodes();
        if(ok) this.saveSubreport();
        return null;
    }
    
    private boolean validateDeficiencyCodes() {
        boolean res = true;
        List<QaSubRptDeficiencyCode> codes = this.subreport.getDeficiencyCodes();
        if(codes == null || codes.size() < 2) return res;
        Set<String> prefixes = new HashSet<String>();
        List<String> list = new ArrayList<String>();
        for(QaSubRptDeficiencyCode code: codes) {
            String[] split = code.getMasterDeficCodeId().split("\\.");
            String pf = split[0];
            prefixes.add(pf);
            list.add(pf);
        }
        if(prefixes.size() > 1) {
            String msg = String.format("Deficiency Codes from different groups [%s] are selected.",Utils.joinStrings(list));
            Utils.addErrorMessage(msg,"Please select Codes from a single group.");
            res = false;
        }
        return res;
    }

    public String reloadSubreport_action() {
        if (this.subreport.getId() != null) {
            this.ejbRef.refreshEntity(this.subreport.getId(), CorpQaSubReport.class);
        }
        return null;
    }

    private void saveSubreport() {
        Date d = new Date();
        this.subreport.setLastModDate(d);
        this.subreport.setLastModUserId(this.userId);
        this.clearOldCodes();
        try {
            if (this.subreport.getId() == null) {
                this.subreport.setCreateDate(d);
                this.subreport.setCreateUserId(this.userId);

                String srId = this.sequenceBean.getNextReportNumber(this.subreport.getSubReportType());
                this.subreport.setSubReportNum(srId);
                
                for(QaSubRptDeficiencyCode code:this.subreport.getDeficiencyCodes()) {
                    code.setSubReportNum(srId);
                }

                this.subreport = this.report.addCorpQaSubReport(this.subreport);
                this.subreport = this.ejbRef.persistEntity(this.subreport);
            } else {
                this.subreport = this.ejbRef.mergeEntity(this.subreport);
            }
        } catch (Exception e) {
            QmgLogger.getLogger().log(Level.SEVERE, "An error occurred while saving the Subreport", e);
            Utils.addErrorMessage("An error occurred while saving the Subreport", e);
            return;
        }
        Utils.addInfoMessage("The Subreport was saved successfully.", "");
    }
}
