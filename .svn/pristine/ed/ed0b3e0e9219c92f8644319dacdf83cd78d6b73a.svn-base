package org.mtahq.qmg.view.managed;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import javax.el.ValueExpression;

import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import oracle.adf.view.rich.event.DialogEvent;

import org.mtahq.qmg.acep.AcepInfo;
import org.mtahq.qmg.entity.CmisStaff;
import org.mtahq.qmg.entity.CorpQaMainReport;
import org.mtahq.qmg.session.QmgSessionEJBLocal;
import org.mtahq.qmg.util.QmgLogger;
import org.mtahq.qmg.view.util.ServiceLocator;
import org.mtahq.qmg.view.util.Utils;

public class ReportListBean implements Serializable {
    private static final String QUERY_PREFIX = "select o from CorpQaMainReport o where ";
    private static final String TYPE_PARAM_NAME = "type";
    
    private static final String REPORT_PARAM_NAME = "report";
    private static final String AGENCY_PARAM_NAME = "agency";
    private static final String CATEGORY_PARAM_NAME = "category";
    private static final String ELEMENT_PARAM_NAME = "element";
    private static final String PROJECT_PARAM_NAME = "project";
    private static final String TASK_PARAM_NAME = "task";   
    private static final String START_PARAM_NAME = "startDate";
    private static final String END_PARAM_NAME = "endDate";
    private static final String MANAGER_PARAM_NAME = "manager";

    private QmgSessionEJBLocal ejbRef = ServiceLocator.getQmgSessionBean();

    private String reportType;
    private CorpQaMainReport selectedReport;
    private CmisStaff staffEntity;
    private String userId;

    private String searchReportType = null;
    private String reportNo;
    private String projectNo;
    private String taskNo;
    private Date startDate;
    private Date endDate;
    private Integer managerId;
    private String qamName;
    private AcepInfo acepInfo;

    private Map<String, Serializable> parameterMap;
    private List<CorpQaMainReport> searchResults;
    
    private List<SelectItem> currentYearItems = new ArrayList<SelectItem>();
    private List<SelectItem> matchingQamStaff = new ArrayList<SelectItem>();
    private Set<String> allQamStaff;
    private Map<String,List<SelectItem>> yearsByReportType = new HashMap<String,List<SelectItem>>();
    private String year = null;
    private String sequence;
    private List<SelectItem> reportSeqItems = new ArrayList<SelectItem>();
    private boolean quickSearchSelected = false;

    public ReportListBean() {
        super();
    }

    public String performSearchAction() {
        String query = this.buildQuery();
        if (this.parameterMap.size() == 0) {
            FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage("Please provide at least one filter crtiterion", ""));
            return null;
        }
        try {
            this.searchResults = this.ejbRef.performReportSearch(query, this.parameterMap);
            QmgLogger.getLogger().fine("Main Report Query results: " + this.searchResults.size());
        } catch (Exception e) {
            QmgLogger.getLogger().log(Level.SEVERE, "A Main Report search error occurred: ", e);
            FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage("A Report Search error occurred:\n" + e.getMessage(), ""));
        }
        return null;
    }

    public String clearSearch_action() {
        this.searchReportType = "";
        this.acepInfo = null;
        this.reportNo = "";
        this.taskNo = "";
        this.projectNo = "";
        this.managerId = null;
        this.startDate = null;
        this.endDate = null;
        this.qamName = null;
        this.searchResults = new ArrayList<CorpQaMainReport>();
        return null;
    }

    private String buildQuery() {
        this.parameterMap = new HashMap<String, Serializable>();
        StringBuffer query = new StringBuffer();
        query.append(QUERY_PREFIX);
        boolean hasWhere = false;
        if (this.searchReportType != null && this.searchReportType.length() > 0) {
            query.append("o.mainReportType = :");
            query.append(TYPE_PARAM_NAME);
            query.append(" ");
            this.parameterMap.put(TYPE_PARAM_NAME, this.searchReportType);
            hasWhere = true;
        }
//        if (this.reportNo != null && this.reportNo.length() > 0) {
//            if (hasWhere)
//                query.append(" and ");
//            query.append("o.mainReportNum like :");
//            query.append(REPORT_PARAM_NAME);
//            query.append(" ");
//            this.parameterMap.put(REPORT_PARAM_NAME, this.reportNo + "%");
//            hasWhere = true;
//        }
//        if (this.projectNo != null && this.projectNo.length() > 0) {
//            if (hasWhere)
//                query.append(" and ");
//            query.append("o.projectNum like :");
//            query.append(PROJECT_PARAM_NAME);
//            query.append(" ");
//            this.parameterMap.put(PROJECT_PARAM_NAME, this.projectNo + "%");
//            hasWhere = true;
//        }
//        if (this.taskNo != null && this.taskNo.length() > 0) {
//            if (hasWhere)
//                query.append(" and ");
//            query.append("o.taskNum like :");
//            query.append(TASK_PARAM_NAME);
//            query.append(" ");
//            this.parameterMap.put(TASK_PARAM_NAME, this.taskNo + "%");
//            hasWhere = true;
//        }
        if(this.acepInfo != null) {
            if (hasWhere)
                query.append(" and ");
            hasWhere = true;
            query.append("o.agency = :");
            query.append(AGENCY_PARAM_NAME);
            query.append(" ");
            this.parameterMap.put(AGENCY_PARAM_NAME, this.acepInfo.getAgency());
            
            query.append(" and ");
            query.append("o.category = :");
            query.append(CATEGORY_PARAM_NAME);
            query.append(" ");
            this.parameterMap.put(CATEGORY_PARAM_NAME, this.acepInfo.getCategory());
            
            query.append(" and ");
            query.append("o.element = :");
            query.append(ELEMENT_PARAM_NAME);
            query.append(" ");
            this.parameterMap.put(ELEMENT_PARAM_NAME, this.acepInfo.getElement());
            
            query.append(" and ");
            query.append("o.project = :");
            query.append(PROJECT_PARAM_NAME);
            query.append(" ");
            this.parameterMap.put(PROJECT_PARAM_NAME, this.acepInfo.getProject());
            
            if(this.acepInfo.getTask() != null) {
                query.append(" and ");
                query.append("o.task = :");
                query.append(TASK_PARAM_NAME);
                query.append(" ");
                this.parameterMap.put(TASK_PARAM_NAME, this.acepInfo.getTask());
            }
        }

        if (this.qamName != null) {
            this.staffEntity = this.findStaffEntityForName();
            if(this.staffEntity != null) {
                if (hasWhere)
                    query.append(" and ");
                query.append("o.owner = :");
                query.append(MANAGER_PARAM_NAME);
                query.append(" ");
                this.managerId = Utils.getStaffIdForName(this.qamName);
                this.parameterMap.put(MANAGER_PARAM_NAME, this.staffEntity);
                hasWhere = true;
            } else {
                QmgLogger.getLogger().severe(String.format("No Cmis Staff record was found for name '%s'",this.qamName));
            }           
        }

        //  Flip start and end dates if necessary
        if (this.startDate != null && this.endDate != null) {
            if (endDate.before(startDate)) {
                Date temp = endDate;
                endDate = startDate;
                startDate = temp;
            }
        }

        if (this.startDate != null) {
            if (hasWhere)
                query.append(" and ");
            query.append("o.createDate >= :");
            query.append(START_PARAM_NAME);
            query.append(" ");
            this.parameterMap.put(START_PARAM_NAME, this.startDate);
            hasWhere = true;
        }
        if (this.endDate != null) {
            if (hasWhere)
                query.append(" and ");
            query.append("o.createDate <= :");
            query.append(END_PARAM_NAME);
            query.append(" ");
            this.parameterMap.put(END_PARAM_NAME, this.endDate);
            hasWhere = true;
        }
        query.append("order by o.mainReportNum");

        QmgLogger.getLogger().fine("Main Report Query string: " + query.toString());
        QmgLogger.getLogger().fine("Main Report parameters: " + this.parameterMap.toString());

        return query.toString();
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getReportType() {
        return reportType;
    }

    public void setSelectedReport(CorpQaMainReport selectedReport) {
        this.selectedReport = selectedReport;
    }

    public CorpQaMainReport getSelectedReport() {
        return selectedReport;
    }

    public void setSearchReportType(String searchReportType) {
        this.searchReportType = searchReportType;
    }

    public String getSearchReportType() {
        return searchReportType;
    }

    public void setReportNo(String reportNo) {
        this.reportNo = reportNo;
    }

    public String getReportNo() {
        return reportNo;
    }

    public void setProjectNo(String projectNo) {
        this.projectNo = projectNo;
    }

    public String getProjectNo() {
        return projectNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    public Integer getManagerId() {
        return managerId;
    }

    public List<CorpQaMainReport> getSearchResults() {
        return searchResults;
    }

    public List<SelectItem> getCurrentYearItems() {
        return currentYearItems;
    }

    public void setYearsByReportType(Map<String, List<SelectItem>> yearsByReportType) {
        this.yearsByReportType = yearsByReportType;
    }

    public Map<String, List<SelectItem>> getYearsByReportType() {
        return yearsByReportType;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getYear() {
        return year;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getSequence() {
        return sequence;
    }

    public List<SelectItem> getReportSeqItems() {
        return reportSeqItems;
    }

    public void setQamName(String qamName) {
        this.qamName = qamName;
    }

    public String getQamName() {
        return qamName;
    }

    public void setAllQamStaff(Set<String> allQamStaff) {
        this.allQamStaff = allQamStaff;
    }

    public Set<String> getAllQamStaff() {
        return allQamStaff;
    }

    public void setStaffEntity(CmisStaff staffEntity) {
        this.staffEntity = staffEntity;
        if(this.staffEntity != null) this.userId = this.staffEntity.getUserId();
    }

    public CmisStaff getStaffEntity() {
        return staffEntity;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setAcepInfo(AcepInfo acepInfo) {
        this.acepInfo = acepInfo;
    }

    public AcepInfo getAcepInfo() {
        return acepInfo;
    } 

    public void setQuickSearchSelected(boolean quickSearchSelected) {
        this.quickSearchSelected = quickSearchSelected;
    }

    public boolean isQuickSearchSelected() {
        return quickSearchSelected;
    }

    public void reportType_valueChangeListener(ValueChangeEvent vce) {
        this.sequence = null;
        this.year = null;
        if(this.reportSeqItems != null) this.reportSeqItems.clear();
        String rptType = (String) vce.getNewValue();
        
        this.reportType = rptType;
        if(rptType != null  && rptType.length() > 0)
        {
            this.currentYearItems = this.yearsByReportType.get(rptType);
        }
        else {
            this.currentYearItems = new ArrayList<SelectItem>();
        }
    }
    
    private void buildSequencesForYear(String reportType, String yr) {
        this.reportSeqItems.clear();
        this.sequence = "";
        String reportNum = reportType + yr + "-";
        HashMap<String,Serializable> map = new HashMap<String,Serializable>();
        map.put("report", reportNum + "%");
        StringBuffer query = new StringBuffer();
        query.append(QUERY_PREFIX);
        query.append("o.mainReportNum like :");
        query.append(REPORT_PARAM_NAME);
        query.append(" order by o.mainReportNum");
        List<CorpQaMainReport> reports = null;
        try {
            QmgLogger.getLogger().fine("Build sequences for year - Query: " + query.toString() + ", Parms: " + map.toString());
            reports = this.ejbRef.performReportSearch(query.toString(), map);
            QmgLogger.getLogger().fine("Build sequences for year results: " + reports.size());
        } catch (Exception e) {
            QmgLogger.getLogger().log(Level.SEVERE, "A Build sequences for year error occurred: ", e);
            FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage("A 'Build sequences for year' error occurred:\n" + e.getMessage(), ""));
        }
        String last = null;
        for(CorpQaMainReport rpt:reports) {
            String suf = rpt.getMainReportNum().substring(7);
            last = suf;
            SelectItem si = new SelectItem(suf);
            this.reportSeqItems.add(si);
        }
        this.sequence = last;
    }

    public void year_valueChangeListener(ValueChangeEvent vce) {  
        this.sequence = null;
        if(this.reportSeqItems != null) this.reportSeqItems.clear();
        String yr = (String) vce.getNewValue();
        this.year = yr;
        if(yr != null) {
            this.buildSequencesForYear(this.reportType, yr);
        }
    }

    public String quickEdit_action() {
        String num = String.format("%s%s-%s", this.searchReportType,this.year,this.sequence);
        QmgLogger.getLogger().fine("Quick report search number: " + num);
        this.reportType = this.searchReportType;
        this.selectedReport = this.ejbRef.getCorpQaMainReportFindOneByNum(num);
        if(this.selectedReport == null) {
            FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage("No Main Report was found for number '" + num + "'", ""));
        }
        return "detail";
    }

    public String newMainReport_action() {
        this.selectedReport = new CorpQaMainReport();
        return null;
    }

    public void reportType_dialogListener(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome() == DialogEvent.Outcome.no) return;
        this.selectedReport = new CorpQaMainReport();
        this.selectedReport.setMainReportType(this.reportType);
        this.selectedReport.setOwner(this.staffEntity);
        
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationHandler nh = context.getApplication().getNavigationHandler();
        nh.handleNavigation(context, null, "detail");
    }
    
    public List<SelectItem> staff_suggestItems(String string) {
        this.matchingQamStaff.clear();
        for(String s:this.allQamStaff) {
            if(s.toUpperCase().startsWith(string.toUpperCase())) {
                SelectItem si = new SelectItem(s);
                this.matchingQamStaff.add(si);
            }
        }
        return this.matchingQamStaff;
    }
    
    private CmisStaff findStaffEntityForName() {
        CmisStaff staff = null;
        FacesContext ctx = FacesContext.getCurrentInstance();
        ValueExpression ve = ctx.getApplication().getExpressionFactory().createValueExpression(ctx.getELContext(), "#{AppInfo.qamStaffByName}", HashMap.class);
        HashMap<String,CmisStaff> map = (HashMap<String,CmisStaff>) ve.getValue(ctx.getELContext());
        staff = map.get(this.qamName);
        return staff;
    }
}
