package org.mtahq.qmg.view.managed;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.PostConstruct;

import javax.faces.model.SelectItem;

import org.mtahq.qmg.entity.CmisStaff;
import org.mtahq.qmg.entity.CqaReportCategory;
import org.mtahq.qmg.enumerations.MainReportTypes;
import org.mtahq.qmg.enumerations.ProjectChecklistStatusCodes;
import org.mtahq.qmg.enumerations.SubReportTypes;
import org.mtahq.qmg.session.QmgSessionEJBLocal;
import org.mtahq.qmg.util.QmgLogger;
import org.mtahq.qmg.view.util.ServiceLocator;

public class AppInfo  implements Serializable  {
    private List<SelectItem> reportTypeItems;
    private List<SelectItem> addableReportTypeItems;
    private List<SelectItem> subreportTypeItems;
    private Map<String, List<SelectItem>> yearsByReportType = new HashMap<String, List<SelectItem>>();
    private QmgSessionEJBLocal ejbRef = ServiceLocator.getQmgSessionBean();
    private Set<String> allChecklistProjects = new TreeSet<String>();
    private List<SelectItem> projectChecklistStatusItems;
    private List<SelectItem> allReportProjectItems = new ArrayList<SelectItem>();
    private List<SelectItem> masterChecklistVersionItems;
    private List<SelectItem> reportCategoryItems;
    private Integer currentMasterChecklistVersion;
    private Set<String> allQAMStaff = new TreeSet<String>();
    private Map<String,CmisStaff> qamStaffByName = new HashMap<String,CmisStaff>();
    private List<SelectItem> qamStaffItems = new ArrayList<SelectItem>();
    
    private Map<String,List<SelectItem>> subtypeMap = new HashMap<String,List<SelectItem>>();
    
    private String qmgVersion = "0.9";
    
    private Map<String,String> constants = new HashMap<String,String>();
    
    private String warningText =
        "WARNING: This Computer System is " + 
        "designed to prevent unauthorized access.  Unauthorized access is" + 
        " a crime.  If you are not authorized, DO NOT Attempt to gain access." +
        "If you are not sure, STOP and contact your Security Administrator to find out.";

    public AppInfo() {
        super();
        this.constants.put("DATE_HINT_TEXT", "Example: 05/15/2016");
    }
    
    @PostConstruct
    public void postConstruct() {
        QmgLogger.getLogger().info("Initializing application info...");
        this.buildReportTypes();
        this.buildReportYears();
        this.buildAllChecklistProjects();
        this.buildChecklistStatusItems();
        this.buildMasterChecklistItems();
        this.buildReportCategoryItems();
        this.buildAllReportProjects();
        this.buildStaffInfo();
    }

    private void buildReportTypes() {
        this.reportTypeItems = new ArrayList<SelectItem>();
        this.addableReportTypeItems = new ArrayList<SelectItem>();
        for (MainReportTypes t : MainReportTypes.values()) {
            SelectItem si = new SelectItem(t.toString());
            this.reportTypeItems.add(si);
            if(t.isActive())
              this.addableReportTypeItems.add(si);
        }
        this.subreportTypeItems = new ArrayList<SelectItem>();
        for (SubReportTypes t : SubReportTypes.values()) {
            SelectItem si = new SelectItem(t.toString());
            this.subreportTypeItems.add(si);
        } 
    }

    private void buildReportYears() {
        for (MainReportTypes t : MainReportTypes.values()) {
            this.buildYearMap(t.toString());
        }
    }
    
    private void buildAllChecklistProjects() {
        this.allChecklistProjects.clear();
        List<String> nums = this.ejbRef.getProjMonitorChecklistDistinctProjectNumbers("%");
        for(String prj:nums){
            this.allChecklistProjects.add(prj);
        }
        //QmgLogger.getLogger().fine("All Checklist Projects:\n" + this.allChecklistProjects.toString());
    }
    
    private void buildAllReportProjects() {
        this.allReportProjectItems.clear();
        List<String> nums = this.ejbRef.getCorpQaMainReportGetDistinctProjects();
        for(String prj:nums){
            SelectItem si = new SelectItem(prj);
            this.allReportProjectItems.add(si);
        }
        //QmgLogger.getLogger().fine("All Master Report Projects:\n" + this.allReportProjectItems.toString());
    }
    
    private void buildChecklistStatusItems() {
        this.projectChecklistStatusItems = new ArrayList<SelectItem>();
        for(ProjectChecklistStatusCodes code:ProjectChecklistStatusCodes.values()) {
            SelectItem si = new SelectItem(code.toString(),code.toString(),code.getLabel());
            this.projectChecklistStatusItems.add(si);
        }
    }
    
    private void buildMasterChecklistItems() {
        this.masterChecklistVersionItems = new ArrayList<SelectItem>();
        List<Integer> versions = this.ejbRef.getMasterProjMonitorChecklistDistinctVersions();
        for(Integer v:versions) {
            SelectItem si = new SelectItem(v);
            this.masterChecklistVersionItems.add(si);
            this.currentMasterChecklistVersion = v;
        }
    }
    
    private void buildReportCategoryItems() {
        //this.reportCategoryItems = new ArrayList<SelectItem>();
        List<CqaReportCategory> cats = this.ejbRef.getCqaReportCategoryFindAll();
        Collections.sort(cats);
        QmgLogger.getLogger().info(String.format("Report Subtype count: %s", cats.size()));
        for(CqaReportCategory cat:cats) {
            //QmgLogger.getLogger().info(String.format("Report Subtype found: %s", cat));
            String repType = cat.getReportType();
            List<SelectItem> items = this.subtypeMap.get(repType);
            if(items == null) {
                items = new ArrayList<SelectItem>();
                this.subtypeMap.put(repType, items);
            }
            SelectItem si = new SelectItem(cat.getCqaReportCategoryId(),cat.getDescription());
            items.add(si);
        }
        
    }

    private void buildYearMap(String rptType) {
        List<Date> dates = this.ejbRef.getCorpQaMainReportGetDistinctCreateDates(rptType);
        Set<String> yrs = new TreeSet<String>();
        GregorianCalendar cal = new GregorianCalendar();
        for (Date d : dates) {
            cal.setTime(d);
            int yr = cal.get(Calendar.YEAR);
            String y = Integer.toString(yr);
            yrs.add(y);
        }
        List<SelectItem> items = new ArrayList<SelectItem>();
        for (String s : yrs) {
            SelectItem si = new SelectItem(s.substring(2),s);
            items.add(si);
        }
        this.yearsByReportType.put(rptType, items);
    }
    
    private void buildStaffInfo() {
        List<CmisStaff> staff = this.ejbRef.getCmisStaffFindAll();
        for(CmisStaff s: staff) {
            this.allQAMStaff.add(s.getFullName());
            this.qamStaffByName.put(s.getFullName(), s);
            SelectItem si = new SelectItem(s,s.getFullName());
            this.qamStaffItems.add(si);
        }
    }

    public List<SelectItem> getReportTypeItems() {
        return reportTypeItems;
    }

    public Map<String, List<SelectItem>> getYearsByReportType() {
        return yearsByReportType;
    }

    public List<SelectItem> getYearItemsForReportType(String reportType) {
        return this.yearsByReportType.get(reportType);
    }

    public Set<String> getAllChecklistProjects() {
        return allChecklistProjects;
    }

    public List<SelectItem> getProjectChecklistStatusItems() {
        return projectChecklistStatusItems;
    }

    public List<SelectItem> getMasterChecklistVersionItems() {
        return masterChecklistVersionItems;
    }

    public Integer getCurrentMasterChecklistVersion() {
        return currentMasterChecklistVersion;
    }

    public List<SelectItem> getReportCategoryItems() {
        return reportCategoryItems;
    }

    public List<SelectItem> getAllReportProjectItems() {
        return allReportProjectItems;
    }

    public Set<String> getAllQAMStaff() {
        return allQAMStaff;
    }

    public Map<String, CmisStaff> getQamStaffByName() {
        return qamStaffByName;
    }

    public List<SelectItem> getQamStaffItems() {
        return qamStaffItems;
    }

    public List<SelectItem> getSubreportTypeItems() {
        return subreportTypeItems;
    }

    public String getWarningText() {
        return warningText;
    }

    public List<SelectItem> getAddableReportTypeItems() {
        return addableReportTypeItems;
    }

    public void setQmgVersion(String qmgVersion) {
        this.qmgVersion = qmgVersion;
    }

    public String getQmgVersion() {
        return qmgVersion;
    }

    public Map<String, List<SelectItem>> getSubtypeMap() {
        return subtypeMap;
    }

    public Map<String, String> getConstants() {
        return constants;
    }
}
