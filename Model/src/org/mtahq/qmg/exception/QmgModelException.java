package org.mtahq.qmg.exception;

public class QmgModelException extends Exception {
    public QmgModelException(String string, Throwable throwable, boolean b, boolean b1) {
        super(string, throwable, b, b1);
    }

    public QmgModelException(Throwable throwable) {
        super(throwable);
    }

    public QmgModelException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public QmgModelException(String string) {
        super(string);
    }

    public QmgModelException() {
        super();
    }
}
