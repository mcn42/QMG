package org.mtahq.qmg.exception;

public class QmgException extends Exception {
    public QmgException(String string, Throwable throwable, boolean b, boolean b1) {
        super(string, throwable, b, b1);
    }

    public QmgException(Throwable throwable) {
        super(throwable);
    }

    public QmgException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public QmgException(String string) {
        super(string);
    }

    public QmgException() {
        super();
    }
}
