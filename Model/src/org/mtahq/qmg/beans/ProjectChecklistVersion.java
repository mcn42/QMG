package org.mtahq.qmg.beans;

import java.text.DateFormat;

import java.util.Date;

public class ProjectChecklistVersion {
    private Integer version;
    private Date versionDate;

    private static DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);

    public ProjectChecklistVersion(Integer version, Date versionDate) {
        super();
        this.version = version;
        this.versionDate = versionDate;
    }

    public Integer getVersion() {
        return version;
    }

    public Date getVersionDate() {
        return versionDate;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof ProjectChecklistVersion)) {
            return false;
        }
        final ProjectChecklistVersion other = (ProjectChecklistVersion) object;
        if (!(version == null ? other.version == null : version.equals(other.version))) {
            return false;
        }
        if (!(versionDate == null ? other.versionDate == null : versionDate.equals(other.versionDate))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 37;
        int result = 1;
        result = PRIME * result + ((version == null) ? 0 : version.hashCode());
        result = PRIME * result + ((versionDate == null) ? 0 : versionDate.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return String.format("#%s - %s", this.version,this.versionDate);
    }
}
