package org.mtahq.qmg.beans;

import java.io.Serializable;

import javax.ejb.Local;

import org.mtahq.qmg.exception.QmgException;

@Local
public interface SequenceBeanInterface extends Serializable {
    void refreshSequenceData();

    String getCurrentYearString();

    boolean isValidReportType(String reportType);

    String getNextReportNumber(String reportType) throws QmgException;
}
