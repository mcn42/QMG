package org.mtahq.qmg.beans;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import javax.ejb.EJB;
import javax.ejb.Singleton;

import org.mtahq.qmg.enumerations.MainReportTypes;
import org.mtahq.qmg.enumerations.SubReportTypes;
import org.mtahq.qmg.exception.QmgException;
import org.mtahq.qmg.session.QmgSessionEJBLocal;
import org.mtahq.qmg.util.QmgLogger;

/**  This class managess AtomicInteger instances for each Report Type.  It initializes them on startup by selecting the
 *   maximum Report Numbers for each Type and extracting the sequence number from them.  The main inteface method is
 *   'String getNextReportNumber(String reportType)'.  A static instance is provided so the class can be used as a
 *   Singleton.  This instance could be refreshed daily by a timer in Production. */
@Singleton(name="QmgSequenceBean",mappedName="QmgSequenceBean")
public class SequenceBean implements SequenceBeanInterface {
    @SuppressWarnings("compatibility:9083682722276049304")
    private static final long serialVersionUID = 1945342557618546456L;

    @EJB
    private QmgSessionEJBLocal ejbRef;
    
    private String currentYearString = "-1";
    private Map<String,AtomicInteger> sequenceMap = new HashMap<String,AtomicInteger>();
    private boolean clearSequences = false;
    
    public SequenceBean(QmgSessionEJBLocal ejbRef) {
        super();
        this.ejbRef = ejbRef;
        this.refreshSequenceData();
    }
    
    public SequenceBean() {
        super();       
    }
    
    @PostConstruct
    public void postConstruct() {
        this.refreshSequenceData();
    }
    
    //@Schedule(dayOfWeek="*",hour="0",minute="1")
    public void refreshSequenceData() {
        this.calculateYearString();
        this.initializeSequences();
    }
    
    private void calculateYearString() {
        String oldYear = this.currentYearString;
        GregorianCalendar cal = new GregorianCalendar();
        int yr = cal.get(Calendar.YEAR);
        this.currentYearString = Integer.toString(yr).substring(2);
        QmgLogger.getLogger().info("Current Year string initialized to " + this.currentYearString);  
        if(!oldYear.equals("-1") && !oldYear.equals(this.currentYearString)) {
            QmgLogger.getLogger().info("Current Year Rollover occurred");
            this.clearSequences = true;
        }
    }
    
    private AtomicInteger getSequence(String reportType) {
        AtomicInteger ai = this.sequenceMap.get(reportType);
        if(ai == null) {
            ai = new AtomicInteger();
            this.sequenceMap.put(reportType, ai);
        }
        return ai;
    }
    
    private void initializeSequences() {
        boolean main = true;
        for(MainReportTypes typ:MainReportTypes.values()) {
            String ts = typ.toString();
            int val = this.getInitialSeqValueForType(ts, main);
            if(val != -1)
            {
                AtomicInteger ai = new AtomicInteger(val);
                this.sequenceMap.put(ts, ai);
            }
        }
        main = false;
        for (SubReportTypes typ : SubReportTypes.values()) {
            String ts = typ.toString();
            int val = this.getInitialSeqValueForType(ts, main);
            if (val != -1) {
                AtomicInteger ai = new AtomicInteger(val);
                this.sequenceMap.put(ts, ai);
            }
        }
    }
    
    private int getInitialSeqValueForType(String type, boolean main) {
        String num = null;
        String numPrefix = type + this.currentYearString + "%";
        if(main) {
            num = this.ejbRef.getCorpQaMainReportFindMaxNumberByType(type,numPrefix);
        } else {
            num = this.ejbRef.getCorpQaSubReportFindMaxNumberByType(type,numPrefix);
        }
        
        if(num == null) {
            QmgLogger.getLogger().severe(String.format("Find Maximum number for %s Report Type '%s' returned NULL!",main?"Main":"Sub",type));
            return 0;
        }
        
        int pos = num.lastIndexOf("-");
        if(pos == -1) {
            QmgLogger.getLogger().severe(String.format("No hyphen: Could not parse suffix for %s Report Type '%s': '%s'",
                main ? "Main" : "Sub", type, num));
            return -1;
        }
        int res = -1;
        String tail = num.substring(pos + 1);
        try {
            res = Integer.parseInt(tail);
        } catch (NumberFormatException nfe) {
            QmgLogger.getLogger().severe(String.format("NFE: Could not parse suffix for %s Report Type '%s': '%s'",
                main ? "Main" : "Sub", type, tail));
            return -1;
        }
        QmgLogger.getLogger().info(String.format("Initial value for %s Report Type '%s': '%s'",
            main ? "Main" : "Sub", type, res));
        return res;
    }

    public String getCurrentYearString() {
        return currentYearString;
    }
    
    public boolean isValidReportType(String reportType) {
//        boolean isMain = MainReportTypes.valueOf(reportType) != null;
//        boolean isSub = SubReportTypes.valueOf(reportType) != null;
//        return isMain || isSub;
        return true;
    }
    
    public String getNextReportNumber(String reportType) throws QmgException {
        if(!this.isValidReportType(reportType)) {
            throw new QmgException(String.format("'%s' is not a valid Main or Sub Report Type", reportType));
        }
        AtomicInteger ai = this.getSequence(reportType);
        int next = ai.incrementAndGet();
        String nxtStr = Integer.toString(next);
        while(nxtStr.length() < 3) nxtStr = "0" + nxtStr;
        String gen = reportType + this.currentYearString + "-" + nxtStr;
        QmgLogger.getLogger().fine(String.format("Generated new Report Number '%s'", gen));
        return gen;
    }

}
