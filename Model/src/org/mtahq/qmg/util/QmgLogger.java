package org.mtahq.qmg.util;

import java.io.IOException;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import weblogic.i18n.logging.NonCatalogLogger;

public class QmgLogger {
    private static NonCatalogLogger wlLogger = new NonCatalogLogger("QMG");
    public QmgLogger() {
        super();
    }
    private static Logger logger = Logger.getLogger("org.mtahq.qmg");
    
    static {
        setUpLoggers();
    }
    
    private static void setUpLoggers() {  
        logger.info("Logging setup initiated");
        logger.setLevel(Level.ALL);
        
        try {
            String path = "D:/logs/QMG.log";
            String sys = System.getProperty("os.name");
//            String path = "/wl8/glassfish3/glassfish/domains/domain1/logs/OrgChart.log";
            if(sys.contains("Windows")) {
                path = "C:/logs/QMG.log";
            } else if (sys.contains("Mac")) {
                path = "/Users/michaeln/logs/QMG.log";
            }
            logger.info("Log file path = " + path);
            
            //logger.addHandler(new ConsoleHandler());
            //logger.addHandler(new ServerLoggingHandler());
            FileHandler fh = new FileHandler(path);
            
            fh.setFormatter(new SimpleFormatter());
            logger.addHandler(fh);
            
        } catch (IOException e) {
            logger.log(Level.SEVERE,"Could not add log FileHandler",e);
        }
        logger.setUseParentHandlers(true);
    }    
    public static Logger getLogger() {
        return logger;
    }
    
    public static void infoToWebLogic(String msg) {
        wlLogger.info(msg);
    }
}
