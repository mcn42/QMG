package org.mtahq.qmg.enumerations;


public enum RoleCodes {
    QA_MANAGER("Q"),ADMINISTRATOR("A"),VIEWER("V"),NONE("X");
    
    String code;

    private RoleCodes(String code) {
        this.code = code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
