package org.mtahq.qmg.enumerations;

public enum SubReportTypes {
    PAR("PAR"), CAR("CAR"), OBS("OBS");
    private String value; 
    private SubReportTypes(String value) {
        this.value = value; } 
}

