package org.mtahq.qmg.enumerations;


public enum MainReportTypes {
    QASR(true),IQAR(true),FAIR(true),VQAR(false),SSSR(false);
    
    private boolean active;
    
    private MainReportTypes(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }
}
