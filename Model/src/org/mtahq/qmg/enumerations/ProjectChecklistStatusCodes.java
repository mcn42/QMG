package org.mtahq.qmg.enumerations;

public enum ProjectChecklistStatusCodes {
    NY("Not Yet"),    
    S("Satisfactory"),
    MS("Mostly Satisfactory"),
    MU("Mostly Unsatisfactory"),
    U("Unsatisfactory"),
    NA("Not Applicable");

    private String label;
    private ProjectChecklistStatusCodes(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
