package org.mtahq.qmg.session;

import java.io.Serializable;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import javax.persistence.NoResultException;

import org.mtahq.qmg.entity.CmisStaff;
import org.mtahq.qmg.entity.CorpQaMainReport;
import org.mtahq.qmg.entity.CorpQaSubReport;
import org.mtahq.qmg.entity.CqaDatePerformed;
import org.mtahq.qmg.entity.CqaReportCategory;
import org.mtahq.qmg.entity.MasterProjMonitorChecklist;
import org.mtahq.qmg.entity.MasterQaDeficiencyCode;
import org.mtahq.qmg.entity.ProjMonitorChecklist;
import org.mtahq.qmg.entity.ProjectStaff;
import org.mtahq.qmg.entity.QaSubRptDeficiencyCode;
import org.mtahq.qmg.entity.QmgProject;

@Local
public interface QmgSessionEJBLocal extends Serializable {
    Object queryByRange(String jpqlStmt, int firstResult, int maxResults);

    <T> T persistEntity(T entity);

    <T> T mergeEntity(T entity);

    void removeCqaReportCategory(CqaReportCategory cqaReportCategory);

    List<CqaReportCategory> getCqaReportCategoryFindAll();

    void removeMasterProjMonitorChecklist(MasterProjMonitorChecklist masterProjMonitorChecklist);

    List<MasterProjMonitorChecklist> getMasterProjMonitorChecklistFindAll();

    void removeMasterQaDeficiencyCode(MasterQaDeficiencyCode masterQaDeficiencyCode);

    List<MasterQaDeficiencyCode> getMasterQaDeficiencyCodeFindAll();

    void removeCorpQaSubReport(CorpQaSubReport corpQaSubReport);

    List<CorpQaSubReport> getCorpQaSubReportFindAll();

    void removeQaSubRptDeficiencyCode(QaSubRptDeficiencyCode qaSubRptDeficiencyCode);

    List<QaSubRptDeficiencyCode> getQaSubRptDeficiencyCodeFindAll();

    void removeCorpQaMainReport(CorpQaMainReport corpQaMainReport);

    List<CorpQaMainReport> getCorpQaMainReportFindAll();

    void removeProjMonitorChecklist(ProjMonitorChecklist projMonitorChecklist);

    List<ProjMonitorChecklist> getProjMonitorChecklistFindAll();

    void removeCqaDatePerformed(CqaDatePerformed cqaDatePerformed);

    List<CqaDatePerformed> getCqaDatePerformedFindAll();

    String getCorpQaSubReportFindMaxNumberByType(String type, String numPrefix);

    String getCorpQaMainReportFindMaxNumberByType(String type, String numPrefix);

    List<CorpQaMainReport> performReportSearch(String query, Map<String, Serializable> parameters);

    List<MasterProjMonitorChecklist> getMasterProjMonitorChecklistFindForVersionNumber(Integer version);

    List<Integer> getMasterProjMonitorChecklistDistinctVersions();

    List<ProjMonitorChecklist> getProjMonitorChecklistFindForProjectVersion(String proj, Integer version);

    List<Integer> getProjMonitorChecklistDistinctProjectVersions(String proj);

    List<Date> getCorpQaMainReportGetDistinctCreateDates(String type);

    CorpQaMainReport getCorpQaMainReportFindOneByNum(String num);

    List<String> getProjMonitorChecklistDistinctProjectNumbers(String proj);

    MasterProjMonitorChecklist getMasterProjMonitorChecklistFindForVersionAndItem(Integer version, Integer item);

    List<String> getCorpQaMainReportGetDistinctProjects();

    void removeCmisStaff(CmisStaff cmisStaff);

    List<CmisStaff> getCmisStaffFindAll();

    CmisStaff getCmisStaffFindByID(Integer id);

    CmisStaff getCmisStaffFindByUserID(String id);

    <T> T refreshEntity(Object id, Class<T> cls);

    MasterQaDeficiencyCode getMasterQaDeficiencyCodeFindForTableAndCode(String table, String code);

    List<ProjMonitorChecklist> getProjMonitorChecklistFindForProjectVersionForACEP(String agency, String category,
                                                                                   String element, String project,
                                                                                   Integer version);

    List<Integer> getProjMonitorChecklistDistinctProjectVersionsForACEP(String agency, String category, String element,
                                                                        String project);

    void removeProjectStaff(ProjectStaff projectStaff);

    void removeQmgProject(QmgProject qmgProject);

    List<QmgProject> getQmgProjectFindAll();

    List<QmgProject> getQmgProjectFindByA(String agency);

    List<QmgProject> getQmgProjectFindByAC(String agency, String category);

    List<QmgProject> getQmgProjectFindByACE(String agency, String category, String element);

    QmgProject getQmgProjectFindByACEP(String agency, String category, String element, String project);

    Boolean getProjectStaffFindProjectAndStaffExists(CmisStaff staff, QmgProject proj);

    ProjectStaff getProjectStaffFindForProjectAndStaff(CmisStaff staff, QmgProject proj);

    CmisStaff getCmisStaffFindByBSCID(String id);

    List<CorpQaSubReport> getCorpQaSubReportFindForMainReport(CorpQaMainReport mainRpt);

    List<String> getCorpQaMainReportGetAllNumbersForType(String type);

    CmisStaff getCmisStaffFindByEmail(String id);

    QmgProject refreshProject(QmgProject proj);

    List<CmisStaff> getCmisStaffFindActive();

    Integer getQaSubRptDeficiencyCodeDeleteForSubReport(String main, String sub);

    List<ProjectStaff> getProjectStaffFindForStaff(CmisStaff staff);
}
