package org.mtahq.qmg.session;

import java.io.Serializable;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.annotation.Resource;

import javax.ejb.Schedule;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import javax.persistence.TypedQuery;

import org.eclipse.persistence.config.CacheUsage;

import org.eclipse.persistence.config.QueryHints;

import org.mtahq.qmg.entity.CmisStaff;
import org.mtahq.qmg.entity.CorpQaMainReport;
import org.mtahq.qmg.entity.CorpQaSubReport;
import org.mtahq.qmg.entity.CqaDatePerformed;
import org.mtahq.qmg.entity.CqaReportCategory;
import org.mtahq.qmg.entity.MasterProjMonitorChecklist;
import org.mtahq.qmg.entity.MasterQaDeficiencyCode;
import org.mtahq.qmg.entity.ProjMonitorChecklist;
import org.mtahq.qmg.entity.ProjectStaff;
import org.mtahq.qmg.entity.QaSubRptDeficiencyCode;
import org.mtahq.qmg.entity.QmgProject;
import org.mtahq.qmg.util.QmgLogger;

@Stateless(name = "QmgSessionEJB", mappedName = "QmgSessionEJB")
public class QmgSessionEJBBean implements QmgSessionEJB, QmgSessionEJBLocal {
    @Resource
    private SessionContext sessionContext;
    @PersistenceContext(unitName = "Model")
    private EntityManager em;

    public QmgSessionEJBBean() {
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Object queryByRange(String jpqlStmt, int firstResult, int maxResults) {
        Query query = em.createQuery(jpqlStmt);
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
    }

    public <T> T persistEntity(T entity) {
        em.persist(entity);
        return entity;
    }

    public <T> T mergeEntity(T entity) {
        return em.merge(entity);
    }

    public void removeCqaReportCategory(CqaReportCategory cqaReportCategory) {
        cqaReportCategory = em.find(CqaReportCategory.class, cqaReportCategory.getId());
        em.remove(cqaReportCategory);
    }

    /** <code>select o from CqaReportCategory o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<CqaReportCategory> getCqaReportCategoryFindAll() {
        return em.createNamedQuery("CqaReportCategory.findAll", CqaReportCategory.class).getResultList();
    }

    public void removeMasterProjMonitorChecklist(MasterProjMonitorChecklist masterProjMonitorChecklist) {
        masterProjMonitorChecklist = em.find(MasterProjMonitorChecklist.class, masterProjMonitorChecklist.getId());
        em.remove(masterProjMonitorChecklist);
    }

    /** <code>select o from MasterProjMonitorChecklist o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<MasterProjMonitorChecklist> getMasterProjMonitorChecklistFindAll() {
        return em.createNamedQuery("MasterProjMonitorChecklist.findAll",
                                   MasterProjMonitorChecklist.class).getResultList();
    }

    public void removeMasterQaDeficiencyCode(MasterQaDeficiencyCode masterQaDeficiencyCode) {
        masterQaDeficiencyCode = em.find(MasterQaDeficiencyCode.class, masterQaDeficiencyCode.getId());
        em.remove(masterQaDeficiencyCode);
    }

    /** <code>select o from MasterQaDeficiencyCode o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<MasterQaDeficiencyCode> getMasterQaDeficiencyCodeFindAll() {
        return em.createNamedQuery("MasterQaDeficiencyCode.findAll", MasterQaDeficiencyCode.class).getResultList();
    }

    public void removeCorpQaSubReport(CorpQaSubReport corpQaSubReport) {
        corpQaSubReport = em.find(CorpQaSubReport.class, corpQaSubReport.getId());
        em.remove(corpQaSubReport);
    }

    /** <code>select o from CorpQaSubReport o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<CorpQaSubReport> getCorpQaSubReportFindAll() {
        return em.createNamedQuery("CorpQaSubReport.findAll", CorpQaSubReport.class).getResultList();
    }

    public void removeQaSubRptDeficiencyCode(QaSubRptDeficiencyCode qaSubRptDeficiencyCode) {
        qaSubRptDeficiencyCode = em.find(QaSubRptDeficiencyCode.class, qaSubRptDeficiencyCode.getId());
        em.remove(qaSubRptDeficiencyCode);
    }

    /** <code>select o from QaSubRptDeficiencyCode o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<QaSubRptDeficiencyCode> getQaSubRptDeficiencyCodeFindAll() {
        return em.createNamedQuery("QaSubRptDeficiencyCode.findAll", QaSubRptDeficiencyCode.class).getResultList();
    }

    public void removeCorpQaMainReport(CorpQaMainReport corpQaMainReport) {
        corpQaMainReport = em.find(CorpQaMainReport.class, corpQaMainReport.getId());
        em.remove(corpQaMainReport);
    }

    /** <code>select o from CorpQaMainReport o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<CorpQaMainReport> getCorpQaMainReportFindAll() {
        return em.createNamedQuery("CorpQaMainReport.findAll", CorpQaMainReport.class).getResultList();
    }

    public void removeProjMonitorChecklist(ProjMonitorChecklist projMonitorChecklist) {
        projMonitorChecklist = em.find(ProjMonitorChecklist.class, projMonitorChecklist.getId());
        em.remove(projMonitorChecklist);
    }

    /** <code>select o from ProjMonitorChecklist o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ProjMonitorChecklist> getProjMonitorChecklistFindAll() {
        return em.createNamedQuery("ProjMonitorChecklist.findAll", ProjMonitorChecklist.class).getResultList();
    }

    public void removeCqaDatePerformed(CqaDatePerformed cqaDatePerformed) {
        cqaDatePerformed = em.find(CqaDatePerformed.class, cqaDatePerformed.getId());
        em.remove(cqaDatePerformed);
    }

    /** <code>select o from CqaDatePerformed o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<CqaDatePerformed> getCqaDatePerformedFindAll() {
        return em.createNamedQuery("CqaDatePerformed.findAll", CqaDatePerformed.class).getResultList();
    }

    /** <code>select max(o.subReportNum) from CorpQaSubReport o where o.subReportType = :type</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String getCorpQaSubReportFindMaxNumberByType(String type, String numPrefix) {
        return em.createNamedQuery("CorpQaSubReport.findMaxNumberByType", String.class).setParameter("type",
                                                                                                     type).setParameter("numPrefix",
                                                                                                                        numPrefix).getSingleResult();
    }

    /** <code>select max(o.mainReportNum) from CorpQaMainReport o where o.mainReportType = :type</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String getCorpQaMainReportFindMaxNumberByType(String type, String numPrefix) {
        return em.createNamedQuery("CorpQaMainReport.findMaxNumberByType", String.class).setParameter("type",
                                                                                                      type).setParameter("numPrefix",
                                                                                                                         numPrefix).getSingleResult();
    }

    public List<CorpQaMainReport> performReportSearch(String query, Map<String, Serializable> parameters) {
        List<CorpQaMainReport> list = null;

        Query q = em.createQuery(query, CorpQaMainReport.class);
        for (String key : parameters.keySet()) {
            Object obj = parameters.get(key);
            q.setParameter(key, obj);
        }
        list = q.getResultList();
        return list;
    }

    /** <code>select o from MasterProjMonitorChecklist o where o.masterVersionNum = :version</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<MasterProjMonitorChecklist> getMasterProjMonitorChecklistFindForVersionNumber(Integer version) {
        return em.createNamedQuery("MasterProjMonitorChecklist.findForVersionNumber",
                                   MasterProjMonitorChecklist.class).setParameter("version", version).getResultList();
    }

    /** <code>select DISTINCT o.masterVersionNum from MasterProjMonitorChecklist o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Integer> getMasterProjMonitorChecklistDistinctVersions() {
        return em.createNamedQuery("MasterProjMonitorChecklist.distinctVersions", Integer.class).getResultList();
    }

    /** <code>select o from ProjMonitorChecklist o where o.projectNum = :proj and o.projectVersionNum = :version</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ProjMonitorChecklist> getProjMonitorChecklistFindForProjectVersion(String proj, Integer version) {
        return em.createNamedQuery("ProjMonitorChecklist.findForProjectVersion",
                                   ProjMonitorChecklist.class).setParameter("proj", proj).setParameter("version",
                                                                                                       version).getResultList();
    }

    /** <code>select DISTINCT o.projectVersionNum from ProjMonitorChecklist o where o.projectNum = :proj order by o.projectVersionDate</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Integer> getProjMonitorChecklistDistinctProjectVersions(String proj) {
        return em.createNamedQuery("ProjMonitorChecklist.distinctProjectVersions", Integer.class).setParameter("proj",
                                                                                                               proj).getResultList();
    }

    /** <code>select DISTINCT o.createDate from CorpQaMainReport o where o.mainReportType = :type</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Date> getCorpQaMainReportGetDistinctCreateDates(String type) {
        return em.createNamedQuery("CorpQaMainReport.getDistinctYears", Date.class).setParameter("type",
                                                                                                 type).getResultList();
    }

    /** <code>select o from CorpQaMainReport o where o.mainReportNum = :num</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public CorpQaMainReport getCorpQaMainReportFindOneByNum(String num) {
        CorpQaMainReport rpt = null;
        try {
            rpt =
                em.createNamedQuery("CorpQaMainReport.findOneByNum", CorpQaMainReport.class).setParameter("num",
                                                                                                          num).getSingleResult();
        } catch (NoResultException e) {
            QmgLogger.getLogger().log(Level.SEVERE, "No result for Main Report Number '" + num + "'", e);
        }
        return rpt;
    }

    /** <code>select DISTINCT o.projectNum from ProjMonitorChecklist o where o.projectNum like :proj order by o.projectNum</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<String> getProjMonitorChecklistDistinctProjectNumbers(String proj) {
        return em.createNamedQuery("ProjMonitorChecklist.distinctProjectNumbers", String.class).setParameter("proj",
                                                                                                             proj).getResultList();
    }

    /** <code>select o from MasterProjMonitorChecklist o where o.masterVersionNum = :version and o.itemNum = :item</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public MasterProjMonitorChecklist getMasterProjMonitorChecklistFindForVersionAndItem(Integer version,
                                                                                         Integer item) {
        MasterProjMonitorChecklist mi = null;
        try {
            mi =
                em.createNamedQuery("MasterProjMonitorChecklist.findForVersionAndItem",
                                    MasterProjMonitorChecklist.class).setParameter("version",
                                                                                   version).setParameter("item",
                                                                                                         item).getSingleResult();
        } catch (NoResultException e) {
            QmgLogger.getLogger().log(Level.SEVERE,
                                      String.format("Master Checklist Item not found for Version %s, Item %s", version,
                                                    item), e);
        }
        return mi;
    }

    /** <code>select DISTINCT o.projectNum from CorpQaMainReport o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<String> getCorpQaMainReportGetDistinctProjects() {
        return em.createNamedQuery("CorpQaMainReport.getDistinctProjects", String.class).getResultList();
    }

    public void removeCmisStaff(CmisStaff cmisStaff) {
        cmisStaff = em.find(CmisStaff.class, cmisStaff.getCmisStaffId());
        em.remove(cmisStaff);
    }

    /** <code>select o from CmisStaff o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<CmisStaff> getCmisStaffFindAll() {
        return em.createNamedQuery("CmisStaff.findAll", CmisStaff.class).getResultList();
    }

    /** <code>select o from CmisStaff o where o.cmisStaffId = :id</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public CmisStaff getCmisStaffFindByID(Integer id) {
        return em.createNamedQuery("CmisStaff.findByID", CmisStaff.class).setParameter("id", id).getSingleResult();
    }

    /** <code>select o from CmisStaff o where o.userId = :id</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public CmisStaff getCmisStaffFindByUserID(String id) {
        return em.createNamedQuery("CmisStaff.findByUserID", CmisStaff.class).setParameter("id", id).getSingleResult();
    }

    /** <code>select o from CmisStaff o where o.userId = :id</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public CmisStaff getCmisStaffFindByBSCID(String id) {
        return em.createNamedQuery("CmisStaff.findByBSCID", CmisStaff.class).setParameter("id", id).getSingleResult();
    }

    public <T> T refreshEntity(Object id, Class<T> cls) {
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("javax.persistence.cache.retrieveMode", "BYPASS");
        return this.em.find(cls, id, properties);
    }

    /** <code>select o from MasterQaDeficiencyCode o where o.masterDeficTableVersion = :table and o.masterDeficCodeId = :code</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public MasterQaDeficiencyCode getMasterQaDeficiencyCodeFindForTableAndCode(String table, String code) {
        return em.createNamedQuery("MasterQaDeficiencyCode.findForTableAndCode",
                                   MasterQaDeficiencyCode.class).setParameter("table", table).setParameter("code",
                                                                                                           code).getSingleResult();
    }

    /** <code>select o from ProjMonitorChecklist o where o.agency = :agency and o.category = :category and o.element = :element and o.project = :project and o.projectVersionNum = :version</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ProjMonitorChecklist> getProjMonitorChecklistFindForProjectVersionForACEP(String agency,
                                                                                          String category,
                                                                                          String element,
                                                                                          String project,
                                                                                          Integer version) {
        return em.createNamedQuery("ProjMonitorChecklist.findForProjectVersionForACEP",
                                   ProjMonitorChecklist.class).setParameter("agency", agency).setParameter("category",
                                                                                                           category).setParameter("element",
                                                                                                                                  element).setParameter("project",
                                                                                                                                                        project).setParameter("version",
                                                                                                                                                                              version).getResultList();
    }

    /** <code>select DISTINCT o.projectVersionNum from ProjMonitorChecklist o where o.agency = :agency and o.category = :category and o.element = :element and o.project = :project order by o.projectVersionDate</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Integer> getProjMonitorChecklistDistinctProjectVersionsForACEP(String agency, String category,
                                                                               String element, String project) {
        return em.createNamedQuery("ProjMonitorChecklist.distinctProjectVersionsForACEP",
                                   Integer.class).setParameter("agency", agency).setParameter("category",
                                                                                              category).setParameter("element",
                                                                                                                     element).setParameter("project",
                                                                                                                                           project).getResultList();
    }

    public void removeProjectStaff(ProjectStaff projectStaff) {
        projectStaff = em.find(ProjectStaff.class, projectStaff.getId());
        em.remove(projectStaff);
    }

    public void removeQmgProject(QmgProject qmgProject) {
        qmgProject = em.find(QmgProject.class, qmgProject.getId());
        em.remove(qmgProject);
    }

    /** <code>select o from QmgProject o</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<QmgProject> getQmgProjectFindAll() {
        return em.createNamedQuery("QmgProject.findAll", QmgProject.class).getResultList();
    }

    /** <code>select o from QmgProject o where o.agency = :agency</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<QmgProject> getQmgProjectFindByA(String agency) {
        return em.createNamedQuery("QmgProject.findByA", QmgProject.class).setParameter("agency",
                                                                                        agency).getResultList();
    }

    /** <code>select o from QmgProject o where o.agency = :agency and o.category = :category</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<QmgProject> getQmgProjectFindByAC(String agency, String category) {
        return em.createNamedQuery("QmgProject.findByAC", QmgProject.class).setParameter("agency",
                                                                                         agency).setParameter("category",
                                                                                                              category).getResultList();
    }

    /** <code>select o from QmgProject o where o.agency = :agency and o.category = :category and o.element = :element</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<QmgProject> getQmgProjectFindByACE(String agency, String category, String element) {
        return em.createNamedQuery("QmgProject.findByACE", QmgProject.class).setParameter("agency",
                                                                                          agency).setParameter("category",
                                                                                                               category).setParameter("element",
                                                                                                                                      element).getResultList();
    }

    /** <code>select o from QmgProject o where o.agency = :agency and o.category = :category and o.element = :element and o.project = :project</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public QmgProject getQmgProjectFindByACEP(String agency, String category, String element, String project) {
        QmgProject proj = null;
        try {
            proj =
                em.createNamedQuery("QmgProject.findByACEP", QmgProject.class).setParameter("agency",
                                                                                            agency).setParameter("category",
                                                                                                                 category).setParameter("element",
                                                                                                                                        element).setParameter("project",
                                                                                                                                                              project).setHint("eclipselink.cache-usage",
                                                                                                                                                                               "CheckCacheThenDatabase").getSingleResult();
        } catch (NoResultException e) {
            QmgLogger.getLogger().warning(String.format("No QmgProject found for ACEP %s%s%s/%s", agency, category,
                                                        element, project));
        }
        return proj;
    }

    private static final Integer DAYS_UNTIL_EXPIRATION = new Integer(90);

    @Schedule(dayOfWeek = "*", hour = "7", minute = "0")
    public void deactivateOlderReports() {
        GregorianCalendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, -DAYS_UNTIL_EXPIRATION);
        Date cutoff = cal.getTime();
        List<CorpQaMainReport> active = null;
        QmgLogger.getLogger().info("Main Report inactivation started....");
        try {
            active = em.createNamedQuery("CorpQaMainReport.findAllActive", CorpQaMainReport.class).getResultList();
            QmgLogger.getLogger().info(String.format("%s Main Reports were found for potential inactivation.",
                                                     active.size()));
        } catch (Exception e) {
            QmgLogger.getLogger().log(Level.SEVERE, "A Main Report inactivation retrieval error occurred", e);
            return;
        }
        int count = 0;
        Date d = new Date();
        for (CorpQaMainReport report : active) {
            if (report.getCreateDate().before(cutoff)) {
                report.setStatus("I");
                report.setLastModDate(d);
                report.setLastModUserId("QMG");
                try {
                    em.merge(report);
                    count++;
                } catch (Exception e) {
                    QmgLogger.getLogger().log(Level.SEVERE,
                                              String.format("A Main Report inactivation error occurred for report\n %s",
                                                            report), e);
                }
            }
        }
        QmgLogger.getLogger().info(String.format("%s Main Reports were successfully inactivated.", count));
    }

    /** <code>select case when (count(o) > 0) then true else false end from ProjectStaff o where o.CMIS_STAFF = :staff and o.qmgProject = :proj</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Boolean getProjectStaffFindProjectAndStaffExists(CmisStaff staff, QmgProject proj) {
        return em.createNamedQuery("ProjectStaff.findProjectAndStaffExists", Boolean.class).setParameter("staff",
                                                                                                         staff).setParameter("proj",
                                                                                                                             proj).getSingleResult();
    }

    /** <code>select o from ProjectStaff o where o.CMIS_STAFF = :staff and o.qmgProject = :proj</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public ProjectStaff getProjectStaffFindForProjectAndStaff(CmisStaff staff, QmgProject proj) {
        List<ProjectStaff> ps = null;
        try {
            ps =
                em.createNamedQuery("ProjectStaff.findForProjectAndStaff", ProjectStaff.class).setParameter("staff",
                                                                                                            staff).setParameter("proj",
                                                                                                                                proj).getResultList();
        } catch (Exception e) {
            QmgLogger.getLogger().log(Level.SEVERE, "Failure while checking ProjectStaff existence", e);
        }
        return ps.size() > 0 ? ps.get(0) : null;
    }

    /** <code>select o from CorpQaSubReport o where o.mainReport = :mainRpt</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<CorpQaSubReport> getCorpQaSubReportFindForMainReport(CorpQaMainReport mainRpt) {
        return em.createNamedQuery("CorpQaSubReport.findForMainReport", CorpQaSubReport.class).setParameter("mainRpt",
                                                                                                            mainRpt).getResultList();
    }

    /** <code>select o.mainReportNum from CorpQaMainReport o where o.mainReportType = :type</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<String> getCorpQaMainReportGetAllNumbersForType(String type) {
        return em.createNamedQuery("CorpQaMainReport.getAllNumbersForType", String.class).setParameter("type",
                                                                                                       type).getResultList();
    }

    /** <code>select o from CmisStaff o where o.email = :id</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public CmisStaff getCmisStaffFindByEmail(String id) {

        CmisStaff cs = null;
        try {
            cs = em.createNamedQuery("CmisStaff.findByEmail", CmisStaff.class).setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            QmgLogger.getLogger().log(Level.SEVERE, "Failed to retrieve Staff by Email", e);
        }
        return cs;
    }

    public QmgProject refreshProject(QmgProject proj) {
        try {
            proj = em.find(QmgProject.class, proj.getId());
        } catch (Exception e) {
            QmgLogger.getLogger().log(Level.SEVERE, "Failed to refresh QmgProject", e);
        }
        return proj;
    }

    /** <code>select o from CmisStaff o where o.activeFlag = 'Y' order by o.lastName, o.firstName </code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<CmisStaff> getCmisStaffFindActive() {
        return em.createNamedQuery("CmisStaff.findActive", CmisStaff.class).getResultList();
    }

    /** <code>delete from QaSubRptDeficiencyCode o where o.mainReportNum = :main and o.subReportNum = :sub</code> */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Integer getQaSubRptDeficiencyCodeDeleteForSubReport(String main, String sub) {
        TypedQuery nq =
            em.createNamedQuery("QaSubRptDeficiencyCode.deleteForSubReport",
                                QaSubRptDeficiencyCode.class).setParameter("main", main).setParameter("sub", sub);
        int i = -1;
        try {
            i = nq.executeUpdate();
        } catch (Exception e) {
            QmgLogger.getLogger().log(Level.SEVERE,
                                      String.format("Error while deleting DefCode(s) for SubReport %s", sub), e);
        }
        QmgLogger.infoToWebLogic(String.format("Deleted %s existing DefCode(s) for SubReport %s", i, sub));
        return i;
    }

    /** <code>select o from ProjectStaff o where o.CMIS_STAFF = :staff</code> */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ProjectStaff> getProjectStaffFindForStaff(CmisStaff staff) {
        return em.createNamedQuery("ProjectStaff.findForStaff", ProjectStaff.class)
            .setHint(QueryHints.CACHE_USAGE, CacheUsage.CheckCacheOnly).setParameter("staff",staff).getResultList();
    }
}
