package org.mtahq.qmg.entity;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import javax.persistence.criteria.Fetch;

import javax.xml.bind.annotation.XmlTransient;

@Entity
@NamedQueries({ @NamedQuery(name = "CorpQaMainReport.findAll", query = "select o from CorpQaMainReport o"),
                @NamedQuery(name = "CorpQaMainReport.findOneByNum",
                            query = "select o from CorpQaMainReport o where o.mainReportNum = :num"),
                @NamedQuery(name = "CorpQaMainReport.findMaxNumberByType",
                            query =
                            "select max(o.mainReportNum) from CorpQaMainReport o where o.mainReportType = :type and o.mainReportNum like :numPrefix"),
                @NamedQuery(name = "CorpQaMainReport.getDistinctYears",
                            query =
                            "select DISTINCT o.createDate from CorpQaMainReport o where o.mainReportType = :type"),
                @NamedQuery(name = "CorpQaMainReport.getDistinctProjects",
                            query = "select DISTINCT o.projectNum from CorpQaMainReport o"),
                @NamedQuery(name = "CorpQaMainReport.getAllNumbersForType",
                            query = "select o.mainReportNum from CorpQaMainReport o where o.mainReportType = :type"),
                @NamedQuery(name = "CorpQaMainReport.findAllActive",
                            query = "select o from CorpQaMainReport o where o.status = 'A'"),
                @NamedQuery(name = "CorpQaMainReport.findReportIds",
                            query = "select o.mainReportNum from CorpQaMainReport o where o.mainReportType = :rptType")
    })
@Table(name = "CORP_QA_MAIN_REPORT")
public class CorpQaMainReport implements Serializable {
    private static final long serialVersionUID = -3932708671609945624L;
    @Column(name = "AREA_OPS", length = 50)
    private String areaOps;
    @Column(name = "CORP_QA_VENDOR", length = 50)
    private String corpQaVendor;
    @Temporal(TemporalType.DATE)
    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;
    @Column(name = "CREATE_USER_ID", nullable = false, length = 8)
    private String createUserId;
    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_PERFORMED")
    private Date datePerformed;
    @Column(name = "EXECUTIVE_SUMMARY", length = 4000)
    private String executiveSummary;
    @Id
    @GeneratedValue(generator = "QMG_GENERAL_SQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "QMG_GENERAL_SQ", sequenceName = "QMG.QMG_GENERAL_SQ", allocationSize = 1)
    @Column(nullable = false)
    private Long id;
    @Temporal(TemporalType.DATE)
    @Column(name = "LAST_MOD_DATE", nullable = false)
    private Date lastModDate;
    @Column(name = "LAST_MOD_USER_ID", nullable = false, length = 8)
    private String lastModUserId;
    @Column(name = "MAIN_REPORT_NUM", nullable = false, unique = true, length = 10)
    private String mainReportNum;
    @Column(name = "MAIN_REPORT_TYPE", length = 4)
    private String mainReportType = "QASR";
    @Column(name = "MASTER_DEFIC_TABLE_VERSION", length = 20)
    private String masterDeficTableVersion = "QA-2008";
    @Column(name = "NUMBER_OF_TESTS")
    private Integer numberOfTests = new Integer(1);
    @Column(name = "PROJ_QUALITY_PLAN_APPROVAL", length = 1)
    private String projQualityPlanApproval;
    @Column(name = "PROJECT_NUM", length = 4)
    private String projectNum;
    @Column(length = 4000)
    private String purpose;
    //    @Column(name = "QAM_CMIS_STAFF_ID")
    //    private Integer qamCmisStaffId;
    @Column(name = "REFERENCE_DOCUMENTS", length = 4000)
    private String referenceDocuments;
    @Column(name = "REPORT_CATEGORY_ID", length = 6)
    private String reportCategoryId;
    @Column(name = "REPORT_TITLE", length = 100)
    private String reportTitle;
    @Column(name = "TASK_NUM", length = 6)
    private String taskNum;
    @Column(name = "USAGE_CODE")
    private Integer usageCode;
    @Column(name = "STATUS", length = 2)
    private String status = "A";

    @Column(length = 2)
    private String agency;
    @Column(length = 6)
    private String category;
    @Column(length = 4)
    private String element;
    @Column(length = 4)
    private String project;
    @Column(length = 10)
    private String task;

    @Transient
    private String taskDescription;
    @Transient
    private String projectDescription;

    @ManyToOne
    @JoinColumn(name = "QAM_CMIS_STAFF_ID", referencedColumnName = "CMIS_STAFF_ID")
    private CmisStaff owner;

    @OneToMany(mappedBy = "mainReport", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    private List<CorpQaSubReport> subReportList;

    @OneToMany(cascade = { CascadeType.ALL })
    @JoinColumn(name = "MAIN_REPORT_NUM", referencedColumnName = "MAIN_REPORT_NUM")
    private List<CqaDatePerformed> performedDates;

    public CorpQaMainReport() {
    }

    public CorpQaMainReport(String areaOps, String corpQaVendor, Date createDate, String createUserId,
                            Date datePerformed, String executiveSummary, Long id, Date lastModDate,
                            String lastModUserId, String mainReportType, String masterDeficTableVersion,
                            Integer numberOfTests, String projQualityPlanApproval, String projectNum, String purpose,
                            String referenceDocuments, String reportCategoryId, String reportTitle, String taskNum,
                            Integer usageCode) {
        this.areaOps = areaOps;
        this.corpQaVendor = corpQaVendor;
        this.createDate = createDate;
        this.createUserId = createUserId;
        this.datePerformed = datePerformed;
        this.executiveSummary = executiveSummary;
        this.id = id;
        this.lastModDate = lastModDate;
        this.lastModUserId = lastModUserId;
        this.mainReportType = mainReportType;
        this.masterDeficTableVersion = masterDeficTableVersion;
        this.numberOfTests = numberOfTests;
        this.projQualityPlanApproval = projQualityPlanApproval;
        this.projectNum = projectNum;
        this.purpose = purpose;
        this.referenceDocuments = referenceDocuments;
        this.reportCategoryId = reportCategoryId;
        this.reportTitle = reportTitle;
        this.taskNum = taskNum;
        this.usageCode = usageCode;
    }

    public String getAreaOps() {
        return areaOps;
    }

    public void setAreaOps(String areaOps) {
        this.areaOps = areaOps;
    }

    public String getCorpQaVendor() {
        return corpQaVendor;
    }

    public void setCorpQaVendor(String corpQaVendor) {
        this.corpQaVendor = corpQaVendor;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public Date getDatePerformed() {
        return datePerformed;
    }

    public void setDatePerformed(Date datePerformed) {
        this.datePerformed = datePerformed;
    }

    public String getExecutiveSummary() {
        return executiveSummary;
    }

    public void setExecutiveSummary(String executiveSummary) {
        this.executiveSummary = executiveSummary;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLastModDate() {
        return lastModDate;
    }

    public void setLastModDate(Date lastModDate) {
        this.lastModDate = lastModDate;
    }

    public String getLastModUserId() {
        return lastModUserId;
    }

    public void setLastModUserId(String lastModUserId) {
        this.lastModUserId = lastModUserId;
    }

    public String getMainReportNum() {
        return mainReportNum;
    }

    public void setMainReportNum(String mainReportNum) {
        this.mainReportNum = mainReportNum;
    }

    public String getMainReportType() {
        return mainReportType;
    }

    public void setMainReportType(String mainReportType) {
        this.mainReportType = mainReportType;
    }

    public String getMasterDeficTableVersion() {
        return masterDeficTableVersion;
    }

    public void setMasterDeficTableVersion(String masterDeficTableVersion) {
        this.masterDeficTableVersion = masterDeficTableVersion;
    }

    public Integer getNumberOfTests() {
        return numberOfTests;
    }

    public void setNumberOfTests(Integer numberOfTests) {
        this.numberOfTests = numberOfTests;
    }

    public String getProjQualityPlanApproval() {
        return projQualityPlanApproval;
    }

    public void setProjQualityPlanApproval(String projQualityPlanApproval) {
        this.projQualityPlanApproval = projQualityPlanApproval;
    }

    public String getProjectNum() {
        return projectNum;
    }

    public void setProjectNum(String projectNum) {
        this.projectNum = projectNum;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public void setOwner(CmisStaff owner) {
        this.owner = owner;
    }

    public CmisStaff getOwner() {
        return owner;
    }

    public String getReferenceDocuments() {
        return referenceDocuments;
    }

    public void setReferenceDocuments(String referenceDocuments) {
        this.referenceDocuments = referenceDocuments;
    }

    public String getReportCategoryId() {
        return reportCategoryId;
    }

    public void setReportCategoryId(String reportCategoryId) {
        this.reportCategoryId = reportCategoryId;
    }

    public String getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }

    public String getTaskNum() {
        return taskNum;
    }

    public void setTaskNum(String taskNum) {
        this.taskNum = taskNum;
    }

    public Integer getUsageCode() {
        return usageCode;
    }

    public void setUsageCode(Integer usageCode) {
        this.usageCode = usageCode;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getAgency() {
        return agency;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getElement() {
        return element;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getProject() {
        return project;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getTask() {
        return task;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @XmlTransient
    public List<CorpQaSubReport> getSubReportList() {
        if (this.subReportList == null)
            this.subReportList = new ArrayList<>();
        return subReportList;
    }

    public void setSubReportList(List<CorpQaSubReport> subReportList) {
        this.subReportList = subReportList;
    }

    public CorpQaSubReport addCorpQaSubReport(CorpQaSubReport corpQaSubReport) {
        getSubReportList().add(corpQaSubReport);
        corpQaSubReport.setMainReport(this);
        return corpQaSubReport;
    }

    public CorpQaSubReport removeCorpQaSubReport(CorpQaSubReport corpQaSubReport) {
        getSubReportList().remove(corpQaSubReport);
        corpQaSubReport.setMainReport(null);
        return corpQaSubReport;
    }

    public void setPerformedDates(List<CqaDatePerformed> performedDates) {
        this.performedDates = performedDates;
    }

    public List<CqaDatePerformed> getPerformedDates() {
        if (this.performedDates == null)
            this.performedDates = new ArrayList<CqaDatePerformed>();
        return performedDates;
    }

    public String getAcepString() {
        if (this.agency == null || this.agency.length() == 0)
            return "";
        return String.format("%s/%s/%s/%s", this.agency, this.category, this.element, this.project);
    }


    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(getClass().getName() + "@" + Integer.toHexString(hashCode()));
        buffer.append('[');
        buffer.append("areaOps=");
        buffer.append(getAreaOps());
        buffer.append(',');
        buffer.append("corpQaVendor=");
        buffer.append(getCorpQaVendor());
        buffer.append(',');
        buffer.append("createDate=");
        buffer.append(getCreateDate());
        buffer.append(',');
        buffer.append("createUserId=");
        buffer.append(getCreateUserId());
        buffer.append(',');
        buffer.append("datePerformed=");
        buffer.append(getDatePerformed());
        buffer.append(',');
        buffer.append("executiveSummary=");
        buffer.append(getExecutiveSummary());
        buffer.append(',');
        buffer.append("id=");
        buffer.append(getId());
        buffer.append(',');
        buffer.append("lastModDate=");
        buffer.append(getLastModDate());
        buffer.append(',');
        buffer.append("lastModUserId=");
        buffer.append(getLastModUserId());
        buffer.append(',');
        buffer.append("mainReportNum=");
        buffer.append(getMainReportNum());
        buffer.append(',');
        buffer.append("mainReportType=");
        buffer.append(getMainReportType());
        buffer.append(',');
        buffer.append("masterDeficTableVersion=");
        buffer.append(getMasterDeficTableVersion());
        buffer.append(',');
        buffer.append("numberOfTests=");
        buffer.append(getNumberOfTests());
        buffer.append(',');
        buffer.append("projQualityPlanApproval=");
        buffer.append(getProjQualityPlanApproval());
        buffer.append(',');
        buffer.append("projectNum=");
        buffer.append(getProjectNum());
        buffer.append(',');
        buffer.append("purpose=");
        buffer.append(getPurpose());
        buffer.append(',');
        buffer.append("referenceDocuments=");
        buffer.append(getReferenceDocuments());
        buffer.append(',');
        buffer.append("reportCategoryId=");
        buffer.append(getReportCategoryId());
        buffer.append(',');
        buffer.append("reportTitle=");
        buffer.append(getReportTitle());
        buffer.append(',');
        buffer.append("taskNum=");
        buffer.append(getTaskNum());
        buffer.append(',');
        buffer.append("usageCode=");
        buffer.append(getUsageCode());
        buffer.append(']');
        return buffer.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof CorpQaMainReport)) {
            return false;
        }
        final CorpQaMainReport other = (CorpQaMainReport) object;
        if (!(id == null ? other.id == null : id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 37;
        int result = 1;
        result = PRIME * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
}
