package org.mtahq.qmg.entity;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({ @NamedQuery(name = "CqaReportCategory.findAll", query = "select o from CqaReportCategory o order by o.displaySequence") })
@Table(name = "CQA_REPORT_CATEGORY")
public class CqaReportCategory implements Serializable,Comparable<CqaReportCategory> {
    private static final long serialVersionUID = -4084196117349927752L;
    @Column(name = "CQA_REPORT_CATEGORY_ID", nullable = false, unique = true, length = 6)
    private String cqaReportCategoryId;
    @Temporal(TemporalType.DATE)
    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;
    @Column(name = "CREATE_USER_ID", nullable = false, length = 8)
    private String createUserId;
    @Column(length = 30)
    private String description;
    @Column(name = "DISPLAY_SEQUENCE")
    private Integer displaySequence;
    @Id
    @GeneratedValue(generator = "QMG_GENERAL_SQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Long id;
    @Temporal(TemporalType.DATE)
    @Column(name = "LAST_MOD_DATE", nullable = false)
    private Date lastModDate;
    @Column(name = "LAST_MOD_USER_ID", nullable = false, length = 8)
    private String lastModUserId;

    public CqaReportCategory() {
    }

    public CqaReportCategory(String cqaReportCategoryId, Date createDate, String createUserId, String description,
                             Integer displaySequence, Long id, Date lastModDate, String lastModUserId) {
        this.cqaReportCategoryId = cqaReportCategoryId;
        this.createDate = createDate;
        this.createUserId = createUserId;
        this.description = description;
        this.displaySequence = displaySequence;
        this.id = id;
        this.lastModDate = lastModDate;
        this.lastModUserId = lastModUserId;
    }

    public String getCqaReportCategoryId() {
        return cqaReportCategoryId;
    }

    public void setCqaReportCategoryId(String cqaReportCategoryId) {
        this.cqaReportCategoryId = cqaReportCategoryId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDisplaySequence() {
        return displaySequence;
    }

    public void setDisplaySequence(Integer displaySequence) {
        this.displaySequence = displaySequence;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLastModDate() {
        return lastModDate;
    }

    public void setLastModDate(Date lastModDate) {
        this.lastModDate = lastModDate;
    }

    public String getLastModUserId() {
        return lastModUserId;
    }

    public void setLastModUserId(String lastModUserId) {
        this.lastModUserId = lastModUserId;
    }
    
    public String getReportType() {
        return this.cqaReportCategoryId == null?"":this.cqaReportCategoryId.substring(0, 4);
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(getClass().getName() + "@" + Integer.toHexString(hashCode()));
        buffer.append('[');
        buffer.append("cqaReportCategoryId=");
        buffer.append(getCqaReportCategoryId());
        buffer.append(',');
        buffer.append("createDate=");
        buffer.append(getCreateDate());
        buffer.append(',');
        buffer.append("createUserId=");
        buffer.append(getCreateUserId());
        buffer.append(',');
        buffer.append("description=");
        buffer.append(getDescription());
        buffer.append(',');
        buffer.append("displaySequence=");
        buffer.append(getDisplaySequence());
        buffer.append(',');
        buffer.append("id=");
        buffer.append(getId());
        buffer.append(',');
        buffer.append("lastModDate=");
        buffer.append(getLastModDate());
        buffer.append(',');
        buffer.append("lastModUserId=");
        buffer.append(getLastModUserId());
        buffer.append(']');
        return buffer.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof CqaReportCategory)) {
            return false;
        }
        final CqaReportCategory other = (CqaReportCategory) object;
        if (!(id == null ? other.id == null : id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 37;
        int result = 1;
        result = PRIME * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }


    @Override
    public int compareTo(CqaReportCategory other) {
        return this.displaySequence.compareTo(other.displaySequence);
    }
}
