package org.mtahq.qmg.entity;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@NamedQueries({
              @NamedQuery(name = "ProjMonitorChecklist.findAll", query = "select o from ProjMonitorChecklist o"),
              @NamedQuery(name = "ProjMonitorChecklist.findForProjectVersion",
                          query =
                          "select o from ProjMonitorChecklist o where o.projectNum = :proj and o.projectVersionNum = :version"),
              @NamedQuery(name = "ProjMonitorChecklist.findForProjectVersionForACEP",
                          query =
                          "select o from ProjMonitorChecklist o where o.agency = :agency and o.category = :category and o.element = :element and o.project = :project and o.projectVersionNum = :version"),
              @NamedQuery(name = "ProjMonitorChecklist.distinctProjectVersions",
                          query =
                          "select DISTINCT o.projectVersionNum from ProjMonitorChecklist o where o.projectNum = :proj order by o.projectVersionDate"),
              @NamedQuery(name = "ProjMonitorChecklist.distinctProjectNumbers",
                          query =
                          "select DISTINCT o.projectNum from ProjMonitorChecklist o where o.projectNum like :proj order by o.projectNum"),
              @NamedQuery(name = "ProjMonitorChecklist.distinctProjectVersionsForACEP",
                          query =
                          "select DISTINCT o.projectVersionNum from ProjMonitorChecklist o where o.agency = :agency and o.category = :category and o.element = :element and o.project = :project order by o.projectVersionDate")
    })
@Table(name = "PROJ_MONITOR_CHECKLIST")
public class ProjMonitorChecklist implements Serializable {
    private static final long serialVersionUID = 3399765767356435184L;
    @Temporal(TemporalType.DATE)
    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;
    @Column(name = "CREATE_USER_ID", nullable = false, length = 8)
    private String createUserId;
    @Id
    @GeneratedValue(generator = "QMG_GENERAL_SQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Long id;
    @Column(name = "ITEM_NUM", nullable = false, unique = true)
    private Integer itemNum;
    @Temporal(TemporalType.DATE)
    @Column(name = "LAST_MOD_DATE", nullable = false)
    private Date lastModDate;
    @Column(name = "LAST_MOD_USER_ID", nullable = false, length = 8)
    private String lastModUserId;
    @Column(name = "MASTER_VERSION_NUM", nullable = false, unique = true)
    private Integer masterVersionNum;
    @Column(name = "PMR_STATUS_VERSION_NUM", nullable = false)
    private Integer pmrStatusVersionNum;
    @Column(name = "PROJECT_NUM", nullable = false, unique = true, length = 4)
    private String projectNum;
    @Temporal(TemporalType.DATE)
    @Column(name = "PROJECT_VERSION_DATE", nullable = false)
    private Date projectVersionDate;
    @Column(name = "PROJECT_VERSION_NUM", nullable = false, unique = true)
    private Integer projectVersionNum;
    @Column(name = "QAM_CMIS_STAFF_ID")
    private Integer qamCmisStaffId;
    @Column(length = 2)
    private String status = "NY";
    @Column(name = "USAGE_CODE", nullable = false, unique = true)
    private Integer usageCode;

    @Column(length = 2)
    private String agency;
    @Column(length = 6)
    private String category;
    @Column(length = 4)
    private String element;
    @Column(length = 4)
    private String project;

    @Transient
    private MasterProjMonitorChecklist masterItem;

    public ProjMonitorChecklist() {
    }

    public ProjMonitorChecklist(MasterProjMonitorChecklist source) {
        this.masterItem = source;
        this.itemNum = source.getItemNum();
        this.createDate = new Date();
        this.lastModDate = this.createDate;
        this.masterVersionNum = source.getMasterVersionNum();

    }

    public ProjMonitorChecklist(Date createDate, String createUserId, Long id, Integer itemNum, Date lastModDate,
                                String lastModUserId, Integer masterVersionNum, Integer pmrStatusVersionNum,
                                String projectNum, Date projectVersionDate, Integer projectVersionNum,
                                Integer qamCmisStaffId, String status, Integer usageCode) {
        this.createDate = createDate;
        this.createUserId = createUserId;
        this.id = id;
        this.itemNum = itemNum;
        this.lastModDate = lastModDate;
        this.lastModUserId = lastModUserId;
        this.masterVersionNum = masterVersionNum;
        this.pmrStatusVersionNum = pmrStatusVersionNum;
        this.projectNum = projectNum;
        this.projectVersionDate = projectVersionDate;
        this.projectVersionNum = projectVersionNum;
        this.qamCmisStaffId = qamCmisStaffId;
        this.status = status;
        this.usageCode = usageCode;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getItemNum() {
        return itemNum;
    }

    public void setItemNum(Integer itemNum) {
        this.itemNum = itemNum;
    }

    public Date getLastModDate() {
        return lastModDate;
    }

    public void setLastModDate(Date lastModDate) {
        this.lastModDate = lastModDate;
    }

    public String getLastModUserId() {
        return lastModUserId;
    }

    public void setLastModUserId(String lastModUserId) {
        this.lastModUserId = lastModUserId;
    }

    public Integer getMasterVersionNum() {
        return masterVersionNum;
    }

    public void setMasterVersionNum(Integer masterVersionNum) {
        this.masterVersionNum = masterVersionNum;
    }

    public Integer getPmrStatusVersionNum() {
        return pmrStatusVersionNum;
    }

    public void setPmrStatusVersionNum(Integer pmrStatusVersionNum) {
        this.pmrStatusVersionNum = pmrStatusVersionNum;
    }

    public String getProjectNum() {
        return projectNum;
    }

    public void setProjectNum(String projectNum) {
        this.projectNum = projectNum;
    }

    public Date getProjectVersionDate() {
        return projectVersionDate;
    }

    public void setProjectVersionDate(Date projectVersionDate) {
        this.projectVersionDate = projectVersionDate;
    }

    public Integer getProjectVersionNum() {
        return projectVersionNum;
    }

    public void setProjectVersionNum(Integer projectVersionNum) {
        this.projectVersionNum = projectVersionNum;
    }

    public Integer getQamCmisStaffId() {
        return qamCmisStaffId;
    }

    public void setQamCmisStaffId(Integer qamCmisStaffId) {
        this.qamCmisStaffId = qamCmisStaffId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getUsageCode() {
        return usageCode;
    }

    public void setUsageCode(Integer usageCode) {
        this.usageCode = usageCode;
    }

    public MasterProjMonitorChecklist getMasterItem() {
        return masterItem;
    }

    public void setMasterItem(MasterProjMonitorChecklist masterItem) {
        this.masterItem = masterItem;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getAgency() {
        return agency;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getElement() {
        return element;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getProject() {
        return project;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(getClass().getName() + "@" + Integer.toHexString(hashCode()));
        buffer.append('[');
        buffer.append("createDate=");
        buffer.append(getCreateDate());
        buffer.append(',');
        buffer.append("createUserId=");
        buffer.append(getCreateUserId());
        buffer.append(',');
        buffer.append("id=");
        buffer.append(getId());
        buffer.append(',');
        buffer.append("itemNum=");
        buffer.append(getItemNum());
        buffer.append(',');
        buffer.append("lastModDate=");
        buffer.append(getLastModDate());
        buffer.append(',');
        buffer.append("lastModUserId=");
        buffer.append(getLastModUserId());
        buffer.append(',');
        buffer.append("masterVersionNum=");
        buffer.append(getMasterVersionNum());
        buffer.append(',');
        buffer.append("pmrStatusVersionNum=");
        buffer.append(getPmrStatusVersionNum());
        buffer.append(',');
        buffer.append("projectNum=");
        buffer.append(getProjectNum());
        buffer.append(',');
        buffer.append("projectVersionDate=");
        buffer.append(getProjectVersionDate());
        buffer.append(',');
        buffer.append("projectVersionNum=");
        buffer.append(getProjectVersionNum());
        buffer.append(',');
        buffer.append("qamCmisStaffId=");
        buffer.append(getQamCmisStaffId());
        buffer.append(',');
        buffer.append("status=");
        buffer.append(getStatus());
        buffer.append(',');
        buffer.append("usageCode=");
        buffer.append(getUsageCode());
        buffer.append(']');
        return buffer.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof ProjMonitorChecklist)) {
            return false;
        }
        final ProjMonitorChecklist other = (ProjMonitorChecklist) object;
        if (!(id == null ? other.id == null : id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 37;
        int result = 1;
        result = PRIME * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
}
