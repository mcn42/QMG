package org.mtahq.qmg.entity;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@NamedQueries({
              @NamedQuery(name = "CmisStaff.findAll",
                          query = "select o from CmisStaff o order by o.lastName, o.firstName"),
              @NamedQuery(name = "CmisStaff.findActive",
                          query = "select o from CmisStaff o where o.activeFlag = 'Y' order by o.lastName, o.firstName "),
              @NamedQuery(name = "CmisStaff.findByID", query = "select o from CmisStaff o where o.cmisStaffId = :id"),
              @NamedQuery(name = "CmisStaff.findByUserID", query = "select o from CmisStaff o where o.userId = :id"),
              @NamedQuery(name = "CmisStaff.findByBSCID", query = "select o from CmisStaff o where o.bscId = :id"),
              @NamedQuery(name = "CmisStaff.findByEmail", query = "select o from CmisStaff o where o.email = :id")
    })
@Table(name = "CMIS_STAFF")
public class CmisStaff implements Serializable {
    private static final long serialVersionUID = -8789577511768144167L;
    @Column(name = "ACTIVE_FLAG", length = 1)
    private String activeFlag;
    @Id
    @GeneratedValue(generator = "QMG_GENERAL_SQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "CMIS_STAFF_ID", nullable = false)
    private Integer cmisStaffId;
    @Temporal(TemporalType.DATE)
    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;
    @Column(name = "CREATE_USER_ID", nullable = false, length = 8)
    private String createUserId;
    @Column(name = "DISPLAY_SEQUENCE")
    private Integer displaySequence;
    @Column(name = "EMAIL", length = 64)
    private String email;
    @Column(name = "FIRST_NAME", length = 20)
    private String firstName;
    @Temporal(TemporalType.DATE)
    @Column(name = "LAST_MOD_DATE", nullable = false)
    private Date lastModDate;
    @Column(name = "LAST_MOD_USER_ID", nullable = false, length = 8)
    private String lastModUserId;
    @Column(name = "LAST_NAME", length = 20)
    private String lastName;
    @Column(name = "MIDDLE_NAME", length = 20)
    private String middleName;
    @Column(name = "USER_ID", length = 32)
    private String userId;
    @Column(name = "ROLE_CODE", length = 2)
    private String roleCode = "Q";
    @Column(name = "LOGIN_STATUS", length = 2)
    private String loginStatus = "N";
    @Column(name="BSC_ID")
    private String bscId;

    @Transient
    private String fullName;
    
    @OneToMany(mappedBy = "CMIS_STAFF", cascade = { CascadeType.ALL})
    private List<ProjectStaff> projectStaffList;

    public CmisStaff() {
    }

    public CmisStaff(String activeFlag, Integer cmisStaffId, Date createDate, String createUserId,
                     Integer displaySequence,String email, String firstName, Date lastModDate, String lastModUserId, String lastName,
                     String middleName) {
        this.activeFlag = activeFlag;
        this.cmisStaffId = cmisStaffId;
        this.createDate = createDate;
        this.createUserId = createUserId;
        this.displaySequence = displaySequence;
        this.email = email;
        this.firstName = firstName;
        this.lastModDate = lastModDate;
        this.lastModUserId = lastModUserId;
        this.lastName = lastName;
        this.middleName = middleName;
        this.buildFullName();
    }


    public String getActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(String activeFlag) {
        this.activeFlag = activeFlag;
    }

    public Integer getCmisStaffId() {
        return cmisStaffId;
    }

    public void setCmisStaffId(Integer cmisStaffId) {
        this.cmisStaffId = cmisStaffId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public Integer getDisplaySequence() {
        return displaySequence;
    }

    public void setDisplaySequence(Integer displaySequence) {
        this.displaySequence = displaySequence;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        this.buildFullName();
    }

    public Date getLastModDate() {
        return lastModDate;
    }

    public void setLastModDate(Date lastModDate) {
        this.lastModDate = lastModDate;
    }

    public String getLastModUserId() {
        return lastModUserId;
    }

    public void setLastModUserId(String lastModUserId) {
        this.lastModUserId = lastModUserId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        this.buildFullName();
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
        this.buildFullName();
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setBscId(String bscId) {
        this.bscId = bscId;
    }

    public String getBscId() {
        return bscId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    private void buildFullName() {
        StringBuffer buff = new StringBuffer();
        buff.append(this.lastName);
        buff.append(", ");
        buff.append(this.firstName);
        if (this.middleName != null) {
            buff.append(" ");
            buff.append(this.middleName.charAt(0));
            buff.append(".");
        }
        this.fullName = buff.toString();
    }

    public String getFullName() {
        if (this.fullName == null)
            this.buildFullName();
        return fullName;
    }


    public List<ProjectStaff> getProjectStaffList() {
        if(this.projectStaffList == null) this.projectStaffList = new ArrayList<ProjectStaff>();
        return projectStaffList;
    }

    public void setProjectStaffList(List<ProjectStaff> projectStaffList) {
        this.projectStaffList = projectStaffList;
    }

    public ProjectStaff addProjectStaff(ProjectStaff projectStaff) {
        getProjectStaffList().add(projectStaff);
        projectStaff.setCMIS_STAFF(this);
        return projectStaff;
    }

    public ProjectStaff removeProjectStaff(ProjectStaff projectStaff) {
        getProjectStaffList().remove(projectStaff);
        projectStaff.setCMIS_STAFF(null);
        return projectStaff;
    }
    
    public boolean associatedWithProject(QmgProject proj) {
        boolean res = false;
        for(ProjectStaff ps:this.getProjectStaffList()) {
            if(proj.equals(ps.getQmgProject()) && ps.getId() != null) {
                res = true;
                break;
            }
        }
        return res;
    }
    
    public void refreshProjectStaff(ProjectStaff ps, String newRole) {
        for(ProjectStaff prjStaff:this.getProjectStaffList()) {
            if(prjStaff.equals(ps)) {
                prjStaff.setProjectRole(newRole);
            }
        }
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(getClass().getName() + "@" + Integer.toHexString(hashCode()));
        buffer.append('[');
        buffer.append("activeFlag=");
        buffer.append(getActiveFlag());
        buffer.append(',');
        buffer.append("cmisStaffId=");
        buffer.append(getCmisStaffId());
        buffer.append(',');
        buffer.append("createDate=");
        buffer.append(getCreateDate());
        buffer.append(',');
        buffer.append("createUserId=");
        buffer.append(getCreateUserId());
        buffer.append(',');
        buffer.append("displaySequence=");
        buffer.append(getDisplaySequence());
        buffer.append(',');
        buffer.append("email=");
        buffer.append(getEmail());
        buffer.append(',');
        buffer.append("firstName=");
        buffer.append(getFirstName());
        buffer.append(',');
        buffer.append("lastModDate=");
        buffer.append(getLastModDate());
        buffer.append(',');
        buffer.append("lastModUserId=");
        buffer.append(getLastModUserId());
        buffer.append(',');
        buffer.append("lastName=");
        buffer.append(getLastName());
        buffer.append(',');
        buffer.append("middleName=");
        buffer.append(getMiddleName());
        buffer.append(',');
        buffer.append("BSC ID=");
        buffer.append(getBscId());
        buffer.append(']');
        return buffer.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof CmisStaff)) {
            return false;
        }
        final CmisStaff other = (CmisStaff) object;
        if (!(cmisStaffId == null ? other.cmisStaffId == null : cmisStaffId.equals(other.cmisStaffId))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 37;
        int result = 1;
        result = PRIME * result + ((cmisStaffId == null) ? 0 : cmisStaffId.hashCode());
        return result;
    }
}
