package org.mtahq.qmg.entity;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({
              @NamedQuery(name = "MasterProjMonitorChecklist.findAll",
                          query = "select o from MasterProjMonitorChecklist o"),
              @NamedQuery(name = "MasterProjMonitorChecklist.findForVersionNumber",
                          query = "select o from MasterProjMonitorChecklist o where o.masterVersionNum = :version"),
              @NamedQuery(name = "MasterProjMonitorChecklist.findForVersionAndItem",
                          query =
                          "select o from MasterProjMonitorChecklist o where o.masterVersionNum = :version and o.itemNum = :item"),
              @NamedQuery(name = "MasterProjMonitorChecklist.distinctVersions",
                          query = "select DISTINCT o.masterVersionNum from MasterProjMonitorChecklist o")
    })
@Table(name = "MASTER_PROJ_MONITOR_CHECKLIST")
public class MasterProjMonitorChecklist implements Serializable {
    private static final long serialVersionUID = -4292347334316225261L;
    @Column(length = 70)
    private String attribute;
    @Column(name = "CATEGORY_NAME", length = 30)
    private String categoryName;
    @Temporal(TemporalType.DATE)
    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;
    @Column(name = "CREATE_USER_ID", nullable = false, length = 8)
    private String createUserId;
    @Id
    @GeneratedValue(generator = "QMG_GENERAL_SQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Long id;
    @Column(name = "ITEM_NUM", nullable = false, unique = true)
    private Integer itemNum;
    @Temporal(TemporalType.DATE)
    @Column(name = "LAST_MOD_DATE", nullable = false)
    private Date lastModDate;
    @Column(name = "LAST_MOD_USER_ID", nullable = false, length = 8)
    private String lastModUserId;
    @Temporal(TemporalType.DATE)
    @Column(name = "MASTER_VERSION_DATE")
    private Date masterVersionDate;
    @Column(name = "MASTER_VERSION_NUM", nullable = false, unique = true)
    private Integer masterVersionNum;
    @Column(name = "REFERENCE_CPM", length = 20)
    private String referenceCpm;
    @Column(name = "REFERENCE_ENG", length = 20)
    private String referenceEng;
    @Column(nullable = false)
    private Integer weight;

    public MasterProjMonitorChecklist() {
    }

    public MasterProjMonitorChecklist(String attribute, String categoryName, Date createDate, String createUserId,
                                      Long id, Integer itemNum, Date lastModDate, String lastModUserId,
                                      Date masterVersionDate, Integer masterVersionNum, String referenceCpm,
                                      String referenceEng, Integer weight) {
        this.attribute = attribute;
        this.categoryName = categoryName;
        this.createDate = createDate;
        this.createUserId = createUserId;
        this.id = id;
        this.itemNum = itemNum;
        this.lastModDate = lastModDate;
        this.lastModUserId = lastModUserId;
        this.masterVersionDate = masterVersionDate;
        this.masterVersionNum = masterVersionNum;
        this.referenceCpm = referenceCpm;
        this.referenceEng = referenceEng;
        this.weight = weight;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getItemNum() {
        return itemNum;
    }

    public void setItemNum(Integer itemNum) {
        this.itemNum = itemNum;
    }

    public Date getLastModDate() {
        return lastModDate;
    }

    public void setLastModDate(Date lastModDate) {
        this.lastModDate = lastModDate;
    }

    public String getLastModUserId() {
        return lastModUserId;
    }

    public void setLastModUserId(String lastModUserId) {
        this.lastModUserId = lastModUserId;
    }

    public Date getMasterVersionDate() {
        return masterVersionDate;
    }

    public void setMasterVersionDate(Date masterVersionDate) {
        this.masterVersionDate = masterVersionDate;
    }

    public Integer getMasterVersionNum() {
        return masterVersionNum;
    }

    public void setMasterVersionNum(Integer masterVersionNum) {
        this.masterVersionNum = masterVersionNum;
    }

    public String getReferenceCpm() {
        return referenceCpm;
    }

    public void setReferenceCpm(String referenceCpm) {
        this.referenceCpm = referenceCpm;
    }

    public String getReferenceEng() {
        return referenceEng;
    }

    public void setReferenceEng(String referenceEng) {
        this.referenceEng = referenceEng;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(getClass().getName() + "@" + Integer.toHexString(hashCode()));
        buffer.append('[');
        buffer.append("attribute=");
        buffer.append(getAttribute());
        buffer.append(',');
        buffer.append("categoryName=");
        buffer.append(getCategoryName());
        buffer.append(',');
        buffer.append("createDate=");
        buffer.append(getCreateDate());
        buffer.append(',');
        buffer.append("createUserId=");
        buffer.append(getCreateUserId());
        buffer.append(',');
        buffer.append("id=");
        buffer.append(getId());
        buffer.append(',');
        buffer.append("itemNum=");
        buffer.append(getItemNum());
        buffer.append(',');
        buffer.append("lastModDate=");
        buffer.append(getLastModDate());
        buffer.append(',');
        buffer.append("lastModUserId=");
        buffer.append(getLastModUserId());
        buffer.append(',');
        buffer.append("masterVersionDate=");
        buffer.append(getMasterVersionDate());
        buffer.append(',');
        buffer.append("masterVersionNum=");
        buffer.append(getMasterVersionNum());
        buffer.append(',');
        buffer.append("referenceCpm=");
        buffer.append(getReferenceCpm());
        buffer.append(',');
        buffer.append("referenceEng=");
        buffer.append(getReferenceEng());
        buffer.append(',');
        buffer.append("weight=");
        buffer.append(getWeight());
        buffer.append(']');
        return buffer.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof MasterProjMonitorChecklist)) {
            return false;
        }
        final MasterProjMonitorChecklist other = (MasterProjMonitorChecklist) object;
        if (!(id == null ? other.id == null : id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 37;
        int result = 1;
        result = PRIME * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
}
