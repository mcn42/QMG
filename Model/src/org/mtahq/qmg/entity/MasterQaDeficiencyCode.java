package org.mtahq.qmg.entity;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({
              @NamedQuery(name = "MasterQaDeficiencyCode.findAll", query = "select o from MasterQaDeficiencyCode o order by o.sortingPosition1,o.sortingPosition2,o.sortingPosition3"),
              @NamedQuery(name = "MasterQaDeficiencyCode.findForTableAndCode",
                          query =
                          "select o from MasterQaDeficiencyCode o where o.masterDeficTableVersion = :table and o.masterDeficCodeId = :code order by o.sortingPosition1,o.sortingPosition2,o.sortingPosition3")
    })
@Table(name = "MASTER_QA_DEFICIENCY_CODE")
public class MasterQaDeficiencyCode implements Serializable {
    private static final long serialVersionUID = 6086444331736222855L;
    @Temporal(TemporalType.DATE)
    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;
    @Column(name = "CREATE_USER_ID", nullable = false, length = 8)
    private String createUserId;
    @Column(length = 100)
    private String description;
    @Id
    @GeneratedValue(generator = "QMG_GENERAL_SQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Long id;
    @Temporal(TemporalType.DATE)
    @Column(name = "LAST_MOD_DATE", nullable = false)
    private Date lastModDate;
    @Column(name = "LAST_MOD_USER_ID", nullable = false, length = 8)
    private String lastModUserId;
    @Column(name = "MASTER_DEFIC_CODE_ID", nullable = false, unique = true, length = 10)
    private String masterDeficCodeId;
    @Column(name = "MASTER_DEFIC_CODE_TYP", length = 1)
    private String masterDeficCodeTyp;
    @Column(name = "MASTER_DEFIC_TABLE_VERSION", nullable = false, unique = true, length = 20)
    private String masterDeficTableVersion;
    @Column(name = "SORTING_POSITION_1", length = 3)
    private String sortingPosition1;
    @Column(name = "SORTING_POSITION_2", length = 3)
    private String sortingPosition2;
    @Column(name = "SORTING_POSITION_3", length = 3)
    private String sortingPosition3;

    public MasterQaDeficiencyCode() {
    }

    public MasterQaDeficiencyCode(Date createDate, String createUserId, String description, Long id, Date lastModDate,
                                  String lastModUserId, String masterDeficCodeId, String masterDeficCodeTyp,
                                  String masterDeficTableVersion, String sortingPosition1, String sortingPosition2,
                                  String sortingPosition3) {
        this.createDate = createDate;
        this.createUserId = createUserId;
        this.description = description;
        this.id = id;
        this.lastModDate = lastModDate;
        this.lastModUserId = lastModUserId;
        this.masterDeficCodeId = masterDeficCodeId;
        this.masterDeficCodeTyp = masterDeficCodeTyp;
        this.masterDeficTableVersion = masterDeficTableVersion;
        this.sortingPosition1 = sortingPosition1;
        this.sortingPosition2 = sortingPosition2;
        this.sortingPosition3 = sortingPosition3;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLastModDate() {
        return lastModDate;
    }

    public void setLastModDate(Date lastModDate) {
        this.lastModDate = lastModDate;
    }

    public String getLastModUserId() {
        return lastModUserId;
    }

    public void setLastModUserId(String lastModUserId) {
        this.lastModUserId = lastModUserId;
    }

    public String getMasterDeficCodeId() {
        return masterDeficCodeId;
    }

    public void setMasterDeficCodeId(String masterDeficCodeId) {
        this.masterDeficCodeId = masterDeficCodeId;
    }

    public String getMasterDeficCodeTyp() {
        return masterDeficCodeTyp;
    }

    public void setMasterDeficCodeTyp(String masterDeficCodeTyp) {
        this.masterDeficCodeTyp = masterDeficCodeTyp;
    }

    public String getMasterDeficTableVersion() {
        return masterDeficTableVersion;
    }

    public void setMasterDeficTableVersion(String masterDeficTableVersion) {
        this.masterDeficTableVersion = masterDeficTableVersion;
    }

    public String getSortingPosition1() {
        return sortingPosition1;
    }

    public void setSortingPosition1(String sortingPosition1) {
        this.sortingPosition1 = sortingPosition1;
    }

    public String getSortingPosition2() {
        return sortingPosition2;
    }

    public void setSortingPosition2(String sortingPosition2) {
        this.sortingPosition2 = sortingPosition2;
    }

    public String getSortingPosition3() {
        return sortingPosition3;
    }

    public void setSortingPosition3(String sortingPosition3) {
        this.sortingPosition3 = sortingPosition3;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(getClass().getName() + "@" + Integer.toHexString(hashCode()));
        buffer.append('[');
        buffer.append("createDate=");
        buffer.append(getCreateDate());
        buffer.append(',');
        buffer.append("createUserId=");
        buffer.append(getCreateUserId());
        buffer.append(',');
        buffer.append("description=");
        buffer.append(getDescription());
        buffer.append(',');
        buffer.append("id=");
        buffer.append(getId());
        buffer.append(',');
        buffer.append("lastModDate=");
        buffer.append(getLastModDate());
        buffer.append(',');
        buffer.append("lastModUserId=");
        buffer.append(getLastModUserId());
        buffer.append(',');
        buffer.append("masterDeficCodeId=");
        buffer.append(getMasterDeficCodeId());
        buffer.append(',');
        buffer.append("masterDeficCodeTyp=");
        buffer.append(getMasterDeficCodeTyp());
        buffer.append(',');
        buffer.append("masterDeficTableVersion=");
        buffer.append(getMasterDeficTableVersion());
        buffer.append(',');
        buffer.append("sortingPosition1=");
        buffer.append(getSortingPosition1());
        buffer.append(',');
        buffer.append("sortingPosition2=");
        buffer.append(getSortingPosition2());
        buffer.append(',');
        buffer.append("sortingPosition3=");
        buffer.append(getSortingPosition3());
        buffer.append(']');
        return buffer.toString();
    }
    
    public String getDescriptiveString() {
        return String.format("%s - %s", this.masterDeficCodeId,this.getDescription());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof MasterQaDeficiencyCode)) {
            return false;
        }
        final MasterQaDeficiencyCode other = (MasterQaDeficiencyCode) object;
        if (!(id == null ? other.id == null : id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 37;
        int result = 1;
        result = PRIME * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
}
