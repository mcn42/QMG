package org.mtahq.qmg.entity;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumns;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({ @NamedQuery(name = "ProjectStaff.findAll", query = "select o from ProjectStaff o"),
                @NamedQuery(name = "ProjectStaff.findProjectAndStaffExists", query = "select case when (count(o) > 0) then true else false end from ProjectStaff o where o.CMIS_STAFF = :staff and o.qmgProject = :proj"),
                @NamedQuery(name = "ProjectStaff.findForProjectAndStaff", query = "select o from ProjectStaff o where o.CMIS_STAFF = :staff and o.qmgProject = :proj"),
                @NamedQuery(name = "ProjectStaff.findForStaff", query = "select o from ProjectStaff o where o.CMIS_STAFF = :staff")})
@Table(name = "PROJECT_STAFF")
public class ProjectStaff implements Serializable {
    private static final long serialVersionUID = 1553871956400857706L;
    @Column(name = "CREATED_BY", nullable = false, length = 32)
    private String createdBy;
    @Temporal(TemporalType.DATE)
    @Column(name = "CREATED_ON", nullable = false)
    private Date createdOn;
    @Id
    @GeneratedValue(generator = "QMG_GENERAL_SQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Long id;
    @Column(name = "PROJECT_ROLE", nullable = false, length = 2)
    private String projectRole;
    @Column(nullable = false, length = 2)
    private String status;
    @ManyToOne
    @JoinColumn(name = "STAFF_ID",referencedColumnName="CMIS_STAFF_ID")    
    private CmisStaff CMIS_STAFF;
    @ManyToOne
    @JoinColumn(name = "PROJECT_ID")
    private QmgProject qmgProject;

    public ProjectStaff() {
    }

    public ProjectStaff(String createdBy, Date createdOn, Long id, QmgProject qmgProject, String projectRole,
                        CmisStaff CMIS_STAFF,
                        String status) {
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.id = id;
        this.qmgProject = qmgProject;
        this.projectRole = projectRole;
        this.CMIS_STAFF = CMIS_STAFF;
        this.status = status;
    }


    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getProjectRole() {
        return projectRole;
    }

    public void setProjectRole(String projectRole) {
        this.projectRole = projectRole;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CmisStaff getCMIS_STAFF() {
        return CMIS_STAFF;
    }

    public void setCMIS_STAFF(CmisStaff CMIS_STAFF) {
        this.CMIS_STAFF = CMIS_STAFF;
    }

    public QmgProject getQmgProject() {
        return qmgProject;
    }

    public void setQmgProject(QmgProject qmgProject) {
        this.qmgProject = qmgProject;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(getClass().getName() + "@" + Integer.toHexString(hashCode()));
        buffer.append('[');
        buffer.append("createdBy=");
        buffer.append(getCreatedBy());
        buffer.append(',');
        buffer.append("createdOn=");
        buffer.append(getCreatedOn());
        buffer.append(',');
        buffer.append("id=");
        buffer.append(getId());
        buffer.append(',');
        buffer.append("projectRole=");
        buffer.append(getProjectRole());
        buffer.append(',');
        buffer.append("status=");
        buffer.append(getStatus());
        buffer.append(']');
        return buffer.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof ProjectStaff)) {
            return false;
        }
        final ProjectStaff other = (ProjectStaff) object;
        if (!(id == null ? other.id == null : id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 37;
        int result = 1;
        result = PRIME * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
}
