package org.mtahq.qmg.entity;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({
              @NamedQuery(name = "QaSubRptDeficiencyCode.findAll", query = "select o from QaSubRptDeficiencyCode o"),
              @NamedQuery(name = "QaSubRptDeficiencyCode.deleteForSubReport", query = "delete from QaSubRptDeficiencyCode o where o.mainReportNum = :main and o.subReportNum = :sub")})
@Table(name = "QA_SUB_RPT_DEFICIENCY_CODE")
public class QaSubRptDeficiencyCode implements Serializable {
    private static final long serialVersionUID = -2618330511761433244L;
    @Temporal(TemporalType.DATE)
    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;
    @Column(name = "CREATE_USER_ID", nullable = false, length = 8)
    private String createUserId;
    @Id
    @GeneratedValue(generator = "QMG_GENERAL_SQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Long id;
    @Temporal(TemporalType.DATE)
    @Column(name = "LAST_MOD_DATE", nullable = false)
    private Date lastModDate;
    @Column(name = "LAST_MOD_USER_ID", nullable = false, length = 8)
    private String lastModUserId;

    @Column(name = "MAIN_REPORT_NUM", nullable = false, unique = true, length = 10)
    private String mainReportNum;

    @Column(name = "MASTER_DEFIC_CODE_ID", nullable = false, unique = true, length = 10)
    private String masterDeficCodeId;
    @Column(name = "MASTER_DEFIC_TABLE_VERSION", nullable = false, unique = true, length = 20)
    private String masterDeficTableVersion;

    @Column(name = "SUB_REPORT_NUM", nullable = false, unique = true, length = 10)
    private String subReportNum;

    public QaSubRptDeficiencyCode() {
    }

    public QaSubRptDeficiencyCode(Date createDate, String createUserId, Long id, Date lastModDate, String lastModUserId,
                                  String mainReportNum, String masterDeficCodeId, String masterDeficTableVersion,
                                  String subReportNum) {
        this.createDate = createDate;
        this.createUserId = createUserId;
        this.id = id;
        this.lastModDate = lastModDate;
        this.lastModUserId = lastModUserId;
        this.mainReportNum = mainReportNum;
        this.masterDeficCodeId = masterDeficCodeId;
        this.masterDeficTableVersion = masterDeficTableVersion;
        this.subReportNum = subReportNum;
    }

    public QaSubRptDeficiencyCode(MasterQaDeficiencyCode mCode) {
        this.masterDeficCodeId = mCode.getMasterDeficCodeId();
        this.masterDeficTableVersion = mCode.getMasterDeficTableVersion();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLastModDate() {
        return lastModDate;
    }

    public void setLastModDate(Date lastModDate) {
        this.lastModDate = lastModDate;
    }

    public String getLastModUserId() {
        return lastModUserId;
    }

    public void setLastModUserId(String lastModUserId) {
        this.lastModUserId = lastModUserId;
    }

    public String getMainReportNum() {
        return mainReportNum;
    }

    public void setMainReportNum(String mainReportNum) {
        this.mainReportNum = mainReportNum;
    }

    public String getMasterDeficCodeId() {
        return masterDeficCodeId;
    }

    public void setMasterDeficCodeId(String masterDeficCodeId) {
        this.masterDeficCodeId = masterDeficCodeId;
    }

    public String getMasterDeficTableVersion() {
        return masterDeficTableVersion;
    }

    public void setMasterDeficTableVersion(String masterDeficTableVersion) {
        this.masterDeficTableVersion = masterDeficTableVersion;
    }

    public String getSubReportNum() {
        return subReportNum;
    }

    public void setSubReportNum(String subReportNum) {
        this.subReportNum = subReportNum;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(getClass().getName() + "@" + Integer.toHexString(hashCode()));
        buffer.append('[');
        buffer.append("createDate=");
        buffer.append(getCreateDate());
        buffer.append(',');
        buffer.append("createUserId=");
        buffer.append(getCreateUserId());
        buffer.append(',');
        buffer.append("id=");
        buffer.append(getId());
        buffer.append(',');
        buffer.append("lastModDate=");
        buffer.append(getLastModDate());
        buffer.append(',');
        buffer.append("lastModUserId=");
        buffer.append(getLastModUserId());
        buffer.append(',');
        buffer.append("mainReportNum=");
        buffer.append(getMainReportNum());
        buffer.append(',');
        buffer.append("masterDeficCodeId=");
        buffer.append(getMasterDeficCodeId());
        buffer.append(',');
        buffer.append("masterDeficTableVersion=");
        buffer.append(getMasterDeficTableVersion());
        buffer.append(',');
        buffer.append("subReportNum=");
        buffer.append(getSubReportNum());
        buffer.append(']');
        return buffer.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof QaSubRptDeficiencyCode)) {
            return false;
        }
        final QaSubRptDeficiencyCode other = (QaSubRptDeficiencyCode) object;
        if (!(id == null ? other.id == null : id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 37;
        int result = 1;
        result = PRIME * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }


}
