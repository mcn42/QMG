package org.mtahq.qmg.entity;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({ @NamedQuery(name = "CqaDatePerformed.findAll", query = "select o from CqaDatePerformed o") })
@Table(name = "CQA_DATE_PERFORMED")
public class CqaDatePerformed implements Serializable {
    private static final long serialVersionUID = 4963006238918935327L;
    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_PERFORMED", unique = true)
    private Date datePerformed;
    @Id
    @GeneratedValue(generator = "QMG_GENERAL_SQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Long id;
    @Column(name = "MAIN_REPORT_NUM", unique = true, length = 10)
    private String mainReportNum;

    public CqaDatePerformed() {
    }

    public CqaDatePerformed(Date datePerformed, Long id, String mainReportNum) {
        this.datePerformed = datePerformed;
        this.id = id;
        this.mainReportNum = mainReportNum;
    }

    public Date getDatePerformed() {
        return datePerformed;
    }

    public void setDatePerformed(Date datePerformed) {
        this.datePerformed = datePerformed;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMainReportNum() {
        return mainReportNum;
    }

    public void setMainReportNum(String mainReportNum) {
        this.mainReportNum = mainReportNum;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(getClass().getName() + "@" + Integer.toHexString(hashCode()));
        buffer.append('[');
        buffer.append("datePerformed=");
        buffer.append(getDatePerformed());
        buffer.append(',');
        buffer.append("id=");
        buffer.append(getId());
        buffer.append(',');
        buffer.append("mainReportNum=");
        buffer.append(getMainReportNum());
        buffer.append(']');
        return buffer.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof CqaDatePerformed)) {
            return false;
        }
        final CqaDatePerformed other = (CqaDatePerformed) object;
        if (!(id == null ? other.id == null : id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 37;
        int result = 1;
        result = PRIME * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
}
