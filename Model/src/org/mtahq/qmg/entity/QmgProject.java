package org.mtahq.qmg.entity;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({ @NamedQuery(name = "QmgProject.findAll", query = "select o from QmgProject o order by o.agency,o.category,o.element,o.project"),
                @NamedQuery(name = "QmgProject.findByA", query = "select o from QmgProject o where o.agency = :agency order by o.agency,o.category,o.element,o.project"),
                @NamedQuery(name = "QmgProject.findByAC", query = "select o from QmgProject o where o.agency = :agency and o.category = :category order by o.agency,o.category,o.element,o.project"),
                @NamedQuery(name = "QmgProject.findByACE", query = "select o from QmgProject o where o.agency = :agency and o.category = :category and o.element = :element order by o.agency,o.category,o.element,o.project"),
                @NamedQuery(name = "QmgProject.findByACEP", query = "select o from QmgProject o where o.agency = :agency and o.category = :category and o.element = :element and o.project = :project")})
@Table(name = "QMG_PROJECTS")
public class QmgProject implements Serializable {
    private static final long serialVersionUID = -65689122096540860L;
    @Column(length = 2)
    private String agency;
    @Column(length = 6)
    private String category;
    @Column(name = "CREATED_BY", nullable = false, length = 32)
    private String createdBy;
    @Temporal(TemporalType.DATE)
    @Column(name = "CREATED_ON", nullable = false)
    private Date createdOn;
    @Column(length = 4)
    private String element;
    @Id
    @GeneratedValue(generator = "QMG_GENERAL_SQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Long id;
    @Column(length = 4)
    private String project;
    @Column(length = 20)
    private String status;
    @Column(name = "UPDATED_BY", length = 32)
    private String updatedBy;
    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATED_ON")
    private Date updatedOn;
    
    @OneToMany(mappedBy = "qmgProject", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE,CascadeType.DETACH })
    private List<ProjectStaff> projectStaffList1;

    public QmgProject() {
    }

    public QmgProject(String agency, String category, String createdBy, Date createdOn, String element, Long id,
                      String project, String status, String updatedBy, Date updatedOn) {
        this.agency = agency;
        this.category = category;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.element = element;
        this.id = id;
        this.project = project;
        this.status = status;
        this.updatedBy = updatedBy;
        this.updatedOn = updatedOn;
    }


    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public List<ProjectStaff> getProjectStaffList1() {
        return projectStaffList1;
    }

    public void setProjectStaffList1(List<ProjectStaff> projectStaffList1) {
        this.projectStaffList1 = projectStaffList1;
    }

    public ProjectStaff addProjectStaff(ProjectStaff projectStaff) {
        getProjectStaffList1().add(projectStaff);
        projectStaff.setQmgProject(this);
        return projectStaff;
    }

    public ProjectStaff removeProjectStaff(ProjectStaff projectStaff) {
        getProjectStaffList1().remove(projectStaff);
        projectStaff.setQmgProject(null);
        return projectStaff;
    }
    
    @Override
    public String toString() {
        String res = String.format("%s%s%s/%s", this.agency,this.category,this.element,this.project);
        return res;
    }
    
    public Integer getStaffCount() {
        return this.projectStaffList1 == null?0:this.projectStaffList1.size();
    }


    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof QmgProject)) {
            return false;
        }
        final QmgProject other = (QmgProject) object;
        if (!(id == null ? other.id == null : id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 37;
        int result = 1;
        result = PRIME * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    //    @Override
//    public String toString() {
//        StringBuffer buffer = new StringBuffer();
//        buffer.append(getClass().getName() + "@" + Integer.toHexString(hashCode()));
//        buffer.append('[');
//        buffer.append("agency=");
//        buffer.append(getAgency());
//        buffer.append(',');
//        buffer.append("category=");
//        buffer.append(getCategory());
//        buffer.append(',');
//        buffer.append("createdBy=");
//        buffer.append(getCreatedBy());
//        buffer.append(',');
//        buffer.append("createdOn=");
//        buffer.append(getCreatedOn());
//        buffer.append(',');
//        buffer.append("element=");
//        buffer.append(getElement());
//        buffer.append(',');
//        buffer.append("id=");
//        buffer.append(getId());
//        buffer.append(',');
//        buffer.append("project=");
//        buffer.append(getProject());
//        buffer.append(',');
//        buffer.append("status=");
//        buffer.append(getStatus());
//        buffer.append(',');
//        buffer.append("updatedBy=");
//        buffer.append(getUpdatedBy());
//        buffer.append(',');
//        buffer.append("updatedOn=");
//        buffer.append(getUpdatedOn());
//        buffer.append(']');
//        return buffer.toString();
//    }
}
