package org.mtahq.qmg.entity;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import javax.persistence.Transient;

import javax.xml.bind.annotation.XmlTransient;

import org.mtahq.qmg.enumerations.SubReportTypes;

@Entity
@NamedQueries({
              @NamedQuery(name = "CorpQaSubReport.findAll", query = "select o from CorpQaSubReport o"),
              @NamedQuery(name = "CorpQaSubReport.findForMainReport", query = "select o from CorpQaSubReport o where o.mainReport = :mainRpt"),
              @NamedQuery(name = "CorpQaSubReport.findMaxNumberByType",
                          query =
                          "select max(o.subReportNum) from CorpQaSubReport o where o.subReportType = :type and o.subReportNum like :numPrefix")
    })
@Table(name = "CORP_QA_SUB_REPORT")
public class CorpQaSubReport implements Serializable {
    private static final long serialVersionUID = -3471117690196181758L;
    @Column(name = "ALL_DEFIC_CODES", length = 500)
    private String allDeficCodes;
    @Column(name = "AUTHORIZED_BY", length = 30)
    private String authorizedBy;
    @Temporal(TemporalType.DATE)
    @Column(name = "AUTHORIZED_DATE")
    private Date authorizedDate;
    @Column(name = "CAUSE_DETERMINED_BY", length = 30)
    private String causeDeterminedBy;
    @Temporal(TemporalType.DATE)
    @Column(name = "CAUSE_DETERMINED_DATE")
    private Date causeDeterminedDate;
    @Column(length = 10)
    private String classification;
    @Column(name = "CONDITION_DESCRIPTION", length = 4000)
    private String conditionDescription;
    @Column(name = "CQA_REPRESENTATIVE", length = 30)
    private String cqaRepresentative;
    @Temporal(TemporalType.DATE)
    @Column(name = "CQA_REVIEW_DATE")
    private Date cqaReviewDate;
    @Temporal(TemporalType.DATE)
    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;
    @Column(name = "CREATE_USER_ID", nullable = false, length = 8)
    private String createUserId;
    @Column(name = "DISPOSITION_BY", length = 30)
    private String dispositionBy;
    @Temporal(TemporalType.DATE)
    @Column(name = "DISPOSITION_DATE")
    private Date dispositionDate;
    @Column(name = "DISPOSITION_MATERIAL", length = 10)
    private String dispositionMaterial;
    @Column(name = "DISPOSITION_PROCEDURE", length = 10)
    private String dispositionProcedure;
    @Temporal(TemporalType.DATE)
    @Column(name = "DUE_DATE")
    private Date dueDate;
    @Column(name = "GOV_DOC", length = 60)
    private String govDoc;
    @Id
    @GeneratedValue(generator = "QMG_GENERAL_SQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Long id;
    @Column(name = "INVESTIGATED_BY", length = 30)
    private String investigatedBy;
    @Temporal(TemporalType.DATE)
    @Column(name = "INVESTIGATED_DATE")
    private Date investigatedDate;
    @Column(name = "ISSUED_BY", length = 30)
    private String issuedBy;
    @Temporal(TemporalType.DATE)
    @Column(name = "ISSUED_DATE")
    private Date issuedDate;
    @Column(name = "ISSUED_TO", length = 30)
    private String issuedTo;
    @Temporal(TemporalType.DATE)
    @Column(name = "LAST_MOD_DATE", nullable = false)
    private Date lastModDate;
    @Column(name = "LAST_MOD_USER_ID", nullable = false, length = 8)
    private String lastModUserId;
    @Column(name = "LETTER_NUM", length = 15)
    private String letterNum;
    @Column(name = "PAR_CAR_TYPE", length = 10)
    private String parCarType;
    @Column(name = "SUB_REPORT_NUM", nullable = false, unique = true, length = 10)
    private String subReportNum;
    @Column(name = "SUB_REPORT_TYPE", length = 10)
    private String subReportType;
    @Column(name = "VERIFIED_BY", length = 30)
    private String verifiedBy;
    @Temporal(TemporalType.DATE)
    @Column(name = "VERIFIED_DATE")
    private Date verifiedDate;

    @ManyToOne
    @JoinColumn(name = "MAIN_REPORT_NUM", referencedColumnName = "MAIN_REPORT_NUM")
    private CorpQaMainReport mainReport;

    @OneToMany(cascade = { CascadeType.ALL})
    @JoinColumn(name = "SUB_REPORT_NUM", referencedColumnName = "SUB_REPORT_NUM")
    private List<QaSubRptDeficiencyCode> deficiencyCodes;
    
    @Transient
    private QaSubRptDeficiencyCode singleDefCode;

    public CorpQaSubReport() {
    }

    public CorpQaSubReport(CorpQaMainReport main, SubReportTypes type) {
        this.mainReport = main;
        this.createDate = new Date();
    }

    public CorpQaSubReport(String allDeficCodes, String authorizedBy, Date authorizedDate, String causeDeterminedBy,
                           Date causeDeterminedDate, String classification, String conditionDescription,
                           String cqaRepresentative, Date cqaReviewDate, Date createDate, String createUserId,
                           String dispositionBy, Date dispositionDate, String dispositionMaterial,
                           String dispositionProcedure, Date dueDate, String govDoc, Long id, String investigatedBy,
                           Date investigatedDate, String issuedBy, Date issuedDate, String issuedTo, Date lastModDate,
                           String lastModUserId, String letterNum, CorpQaMainReport mainReport, String parCarType,
                           String subReportNum, String subReportType, String verifiedBy, Date verifiedDate) {
        this.allDeficCodes = allDeficCodes;
        this.authorizedBy = authorizedBy;
        this.authorizedDate = authorizedDate;
        this.causeDeterminedBy = causeDeterminedBy;
        this.causeDeterminedDate = causeDeterminedDate;
        this.classification = classification;
        this.conditionDescription = conditionDescription;
        this.cqaRepresentative = cqaRepresentative;
        this.cqaReviewDate = cqaReviewDate;
        this.createDate = createDate;
        this.createUserId = createUserId;
        this.dispositionBy = dispositionBy;
        this.dispositionDate = dispositionDate;
        this.dispositionMaterial = dispositionMaterial;
        this.dispositionProcedure = dispositionProcedure;
        this.dueDate = dueDate;
        this.govDoc = govDoc;
        this.id = id;
        this.investigatedBy = investigatedBy;
        this.investigatedDate = investigatedDate;
        this.issuedBy = issuedBy;
        this.issuedDate = issuedDate;
        this.issuedTo = issuedTo;
        this.lastModDate = lastModDate;
        this.lastModUserId = lastModUserId;
        this.letterNum = letterNum;
        this.mainReport = mainReport;
        this.parCarType = parCarType;
        this.subReportNum = subReportNum;
        this.subReportType = subReportType;
        this.verifiedBy = verifiedBy;
        this.verifiedDate = verifiedDate;
    }

    public String getAllDeficCodes() {
        return allDeficCodes;
    }

    public void setAllDeficCodes(String allDeficCodes) {
        this.allDeficCodes = allDeficCodes;
    }

    public String getAuthorizedBy() {
        return authorizedBy;
    }

    public void setAuthorizedBy(String authorizedBy) {
        this.authorizedBy = authorizedBy;
    }

    public Date getAuthorizedDate() {
        return authorizedDate;
    }

    public void setAuthorizedDate(Date authorizedDate) {
        this.authorizedDate = authorizedDate;
    }

    public String getCauseDeterminedBy() {
        return causeDeterminedBy;
    }

    public void setCauseDeterminedBy(String causeDeterminedBy) {
        this.causeDeterminedBy = causeDeterminedBy;
    }

    public Date getCauseDeterminedDate() {
        return causeDeterminedDate;
    }

    public void setCauseDeterminedDate(Date causeDeterminedDate) {
        this.causeDeterminedDate = causeDeterminedDate;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getConditionDescription() {
        return conditionDescription;
    }

    public void setConditionDescription(String conditionDescription) {
        this.conditionDescription = conditionDescription;
    }

    public String getCqaRepresentative() {
        return cqaRepresentative;
    }

    public void setCqaRepresentative(String cqaRepresentative) {
        this.cqaRepresentative = cqaRepresentative;
    }

    public Date getCqaReviewDate() {
        return cqaReviewDate;
    }

    public void setCqaReviewDate(Date cqaReviewDate) {
        this.cqaReviewDate = cqaReviewDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getDispositionBy() {
        return dispositionBy;
    }

    public void setDispositionBy(String dispositionBy) {
        this.dispositionBy = dispositionBy;
    }

    public Date getDispositionDate() {
        return dispositionDate;
    }

    public void setDispositionDate(Date dispositionDate) {
        this.dispositionDate = dispositionDate;
    }

    public String getDispositionMaterial() {
        return dispositionMaterial;
    }

    public void setDispositionMaterial(String dispositionMaterial) {
        this.dispositionMaterial = dispositionMaterial;
    }

    public String getDispositionProcedure() {
        return dispositionProcedure;
    }

    public void setDispositionProcedure(String dispositionProcedure) {
        this.dispositionProcedure = dispositionProcedure;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getGovDoc() {
        return govDoc;
    }

    public void setGovDoc(String govDoc) {
        this.govDoc = govDoc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInvestigatedBy() {
        return investigatedBy;
    }

    public void setInvestigatedBy(String investigatedBy) {
        this.investigatedBy = investigatedBy;
    }

    public Date getInvestigatedDate() {
        return investigatedDate;
    }

    public void setInvestigatedDate(Date investigatedDate) {
        this.investigatedDate = investigatedDate;
    }

    public String getIssuedBy() {
        return issuedBy;
    }

    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }

    public Date getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(Date issuedDate) {
        this.issuedDate = issuedDate;
    }

    public String getIssuedTo() {
        return issuedTo;
    }

    public void setIssuedTo(String issuedTo) {
        this.issuedTo = issuedTo;
    }

    public Date getLastModDate() {
        return lastModDate;
    }

    public void setLastModDate(Date lastModDate) {
        this.lastModDate = lastModDate;
    }

    public String getLastModUserId() {
        return lastModUserId;
    }

    public void setLastModUserId(String lastModUserId) {
        this.lastModUserId = lastModUserId;
    }

    public String getLetterNum() {
        return letterNum;
    }

    public void setLetterNum(String letterNum) {
        this.letterNum = letterNum;
    }

    public String getParCarType() {
        return parCarType;
    }

    public void setParCarType(String parCarType) {
        this.parCarType = parCarType;
    }

    public String getSubReportNum() {
        return subReportNum;
    }

    public void setSubReportNum(String subReportNum) {
        this.subReportNum = subReportNum;
    }

    public String getSubReportType() {
        return subReportType;
    }

    public void setSubReportType(String subReportType) {
        this.subReportType = subReportType;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public Date getVerifiedDate() {
        return verifiedDate;
    }

    public void setVerifiedDate(Date verifiedDate) {
        this.verifiedDate = verifiedDate;
    }

    @XmlTransient
    public CorpQaMainReport getMainReport() {
        return mainReport;
    }

    public void setMainReport(CorpQaMainReport mainReport) {
        this.mainReport = mainReport;
    }

    public void setDeficiencyCodes(List<QaSubRptDeficiencyCode> deficiencyCodes) {
        this.deficiencyCodes = deficiencyCodes;
        if(this.deficiencyCodes != null && this.deficiencyCodes.size() > 0)
          this.singleDefCode = this.getDeficiencyCodes().get(0);
    }

    public List<QaSubRptDeficiencyCode> getDeficiencyCodes() {
        if (this.deficiencyCodes == null)
            this.deficiencyCodes = new ArrayList<QaSubRptDeficiencyCode>();
        return deficiencyCodes;
    }
    
    public String getDeficiencyCodeString() {
        StringBuffer buff = new StringBuffer();
        for (QaSubRptDeficiencyCode sr : this.getDeficiencyCodes()) {
            buff.append(sr.getMasterDeficCodeId());
            buff.append(" ");
        }
        String res = buff.toString();
        return res;
    }

    public QaSubRptDeficiencyCode addQaSubRptDeficiencyCode(QaSubRptDeficiencyCode code) {
        this.getDeficiencyCodes().add(code);
        this.singleDefCode = this.getDeficiencyCodes().get(0);
        //        Date d = new Date();
        //        code.setCreateDate(d);
        //        code.setLastModDate(d);
        code.setSubReportNum(this.subReportNum);
        code.setMainReportNum(this.mainReport.getMainReportNum());
        return code;
    }


    public void setSingleDefCode(QaSubRptDeficiencyCode singleDefCode) {
        this.singleDefCode = singleDefCode;
        this.getDeficiencyCodes().clear();
        if(this.singleDefCode != null) {
            this.getDeficiencyCodes().add(singleDefCode);
        }
    }

    public QaSubRptDeficiencyCode getSingleDefCode() {
        return singleDefCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(getClass().getName() + "@" + Integer.toHexString(hashCode()));
        buffer.append('[');
        buffer.append("allDeficCodes=");
        buffer.append(getAllDeficCodes());
        buffer.append(',');
        buffer.append("authorizedBy=");
        buffer.append(getAuthorizedBy());
        buffer.append(',');
        buffer.append("authorizedDate=");
        buffer.append(getAuthorizedDate());
        buffer.append(',');
        buffer.append("causeDeterminedBy=");
        buffer.append(getCauseDeterminedBy());
        buffer.append(',');
        buffer.append("causeDeterminedDate=");
        buffer.append(getCauseDeterminedDate());
        buffer.append(',');
        buffer.append("classification=");
        buffer.append(getClassification());
        buffer.append(',');
        buffer.append("conditionDescription=");
        buffer.append(getConditionDescription());
        buffer.append(',');
        buffer.append("cqaRepresentative=");
        buffer.append(getCqaRepresentative());
        buffer.append(',');
        buffer.append("cqaReviewDate=");
        buffer.append(getCqaReviewDate());
        buffer.append(',');
        buffer.append("createDate=");
        buffer.append(getCreateDate());
        buffer.append(',');
        buffer.append("createUserId=");
        buffer.append(getCreateUserId());
        buffer.append(',');
        buffer.append("dispositionBy=");
        buffer.append(getDispositionBy());
        buffer.append(',');
        buffer.append("dispositionDate=");
        buffer.append(getDispositionDate());
        buffer.append(',');
        buffer.append("dispositionMaterial=");
        buffer.append(getDispositionMaterial());
        buffer.append(',');
        buffer.append("dispositionProcedure=");
        buffer.append(getDispositionProcedure());
        buffer.append(',');
        buffer.append("dueDate=");
        buffer.append(getDueDate());
        buffer.append(',');
        buffer.append("govDoc=");
        buffer.append(getGovDoc());
        buffer.append(',');
        buffer.append("id=");
        buffer.append(getId());
        buffer.append(',');
        buffer.append("investigatedBy=");
        buffer.append(getInvestigatedBy());
        buffer.append(',');
        buffer.append("investigatedDate=");
        buffer.append(getInvestigatedDate());
        buffer.append(',');
        buffer.append("issuedBy=");
        buffer.append(getIssuedBy());
        buffer.append(',');
        buffer.append("issuedDate=");
        buffer.append(getIssuedDate());
        buffer.append(',');
        buffer.append("issuedTo=");
        buffer.append(getIssuedTo());
        buffer.append(',');
        buffer.append("lastModDate=");
        buffer.append(getLastModDate());
        buffer.append(',');
        buffer.append("lastModUserId=");
        buffer.append(getLastModUserId());
        buffer.append(',');
        buffer.append("letterNum=");
        buffer.append(getLetterNum());
        buffer.append(',');
        buffer.append("parCarType=");
        buffer.append(getParCarType());
        buffer.append(',');
        buffer.append("subReportNum=");
        buffer.append(getSubReportNum());
        buffer.append(',');
        buffer.append("subReportType=");
        buffer.append(getSubReportType());
        buffer.append(',');
        buffer.append("verifiedBy=");
        buffer.append(getVerifiedBy());
        buffer.append(',');
        buffer.append("verifiedDate=");
        buffer.append(getVerifiedDate());
        buffer.append(']');
        return buffer.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof CorpQaSubReport)) {
            return false;
        }
        final CorpQaSubReport other = (CorpQaSubReport) object;
        if (!(id == null ? other.id == null : id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 37;
        int result = 1;
        result = PRIME * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
}
