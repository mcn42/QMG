<?xml version='1.0' encoding='UTF-8'?>
<ui:composition xmlns:ui="http://java.sun.com/jsf/facelets" xmlns:af="http://xmlns.oracle.com/adf/faces/rich"
                xmlns:f="http://java.sun.com/jsf/core">
  <af:panelGroupLayout id="pgl1" layout="vertical">
        <af:panelGroupLayout id="pgl2" layout="vertical">
            <af:outputText value="Vendor Quality Audit Report" id="ot1" styleClass="header"/>
            <af:spacer width="10" height="10" id="s3"/>
            <af:panelGroupLayout id="pgl3" layout="horizontal">
                <af:inputText value="#{ReportBean.report.mainReportNum == null?'&lt;Pending>':ReportBean.report.mainReportNum}"
                              label="Report Number" styleClass="bold"
                              id="it1" readOnly="true" partialTriggers="b1 b2"></af:inputText>
                <af:spacer width="50" height="10" id="s4"/>
                <af:inputText value="#{ReportBean.report.reportTitle}"
                              label="Report Title" id="it3"
                              styleClass="bold" columns="60" required="true"
                              readOnly="#{ReportBean.disabled}"></af:inputText>
            </af:panelGroupLayout>
        </af:panelGroupLayout>
        <af:spacer width="10" height="5" id="s2"/>
        <af:panelGroupLayout id="pgl5" layout="horizontal">
            <af:outputText value="Project and Task" id="ot4" styleClass="subheader"/>
            <af:spacer width="20" height="10" id="s8"/>
            <af:selectBooleanCheckbox
                                      label="This Report is not associated with a Project" id="sbc2"
                                      value="#{ReportBean.noProject}" autoSubmit="true"
                                      readOnly="#{ReportBean.disabled}"
                                      rendered="#{!ReportBean.disabled || ReportBean.report.project == null}"/>
        </af:panelGroupLayout>
        <af:panelGroupLayout id="sdh1" partialTriggers="b11">
            <af:panelFormLayout id="pfl2" maxColumns="3" rows="2" rendered="#{!ReportBean.noProject}">
                <af:inputText value="#{ReportBean.report.acepString}" label="ACEP" id="it4"
                              readOnly="true" partialTriggers="b11 b2 p2 d2"/>
                <af:inputText value="#{ReportBean.report.task}" label="Task Num." id="it5"
                              readOnly="true" partialTriggers="b11 b2 p2 d2"/>
                <af:inputText id="it2" columns="50" readOnly="true" label="Project Desc."
                              value="#{ReportBean.report.projectDescription}" partialTriggers="b11 b2 p2 d2"/>
                <af:inputText id="it19" columns="50" readOnly="true" label="Task Desc."
                              value="#{ReportBean.report.taskDescription}" partialTriggers="b11 b2 p2 d2"/>
                <f:facet name="footer">
                    <af:button text="Select Project And Task" id="b11" immediate="true"
                               disabled="#{ReportBean.disabled || ReportBean.noProject}"
                               partialTriggers="sbc2">
                        <af:showPopupBehavior popupId="p2"/>
                    </af:button>
                </f:facet>
            </af:panelFormLayout>
        </af:panelGroupLayout>
        <af:spacer width="10" height="5" id="s6"/>
        <af:panelFormLayout id="pgl4" maxColumns="3" rows="1">
            <af:inputText value="#{ReportBean.report.owner == null?'[None]':ReportBean.report.owner.fullName}"
                          label="QA Manager" id="it15" readOnly="#{ReportBean.disabled}">
            </af:inputText>
            <af:selectBooleanCheckbox label="PQP Appr." id="sbc1"
                                      value="#{ReportBean.report.projQualityPlanApproval}"
                                      rendered="false"/>
            <af:inputText label="Vendor" id="it7" value="#{ReportBean.report.corpQaVendor}"
                          readOnly="#{ReportBean.disabled}"/>
            <af:selectOneChoice label="Report Sub-Type" id="soc2"
                                value="#{ReportBean.report.reportCategoryId}"
                                readOnly="#{ReportBean.disabled}" required="true">
                <f:selectItems id="si2" value="#{AppInfo.subtypeMap['VQAR']}"/>
            </af:selectOneChoice>
            <af:selectOneChoice label="Deficiency Code Version" id="soc1"
                                value="#{ReportBean.report.masterDeficTableVersion}"
                                readOnly="#{ReportBean.disabled}" required="true">
                <f:selectItems value="#{DeficiencyCodeBean.tableVersions}" id="si1"/>
            </af:selectOneChoice>
            <af:inputDate label="Date Performed" id="id4" required="true"
                          value="#{ReportBean.report.datePerformed}"
                          readOnly="#{ReportBean.disabled}">
                <af:convertDateTime type="date" hintDate="#{AppInfo.constants['DATE_HINT_TEXT']}"/>
            </af:inputDate>
        </af:panelFormLayout>
        <af:spacer width="10" height="10" id="s5"/>
        <af:showDetailHeader text="Reference Docs, Purpose and Summary" disclosed="true" id="sdh2">
            <f:facet name="context"/>
            <f:facet name="menuBar"/>
            <f:facet name="toolbar"/>
            <f:facet name="legend"/>
            <f:facet name="info"/>
            <af:panelFormLayout id="pfl3">
                <af:inputText value="#{ReportBean.report.referenceDocuments}" label="Reference Documents"
                              columns="90" id="it9" rows="2" readOnly="#{ReportBean.disabled}"
                              required="true"/>
                <af:inputText value="#{ReportBean.report.purpose}" label="Purpose" columns="90" id="it10"
                              rows="3" readOnly="#{ReportBean.disabled}" required="true"/>
                <af:inputText value="#{ReportBean.report.executiveSummary}" label="Executive Summary"
                              columns="90" id="it8" rows="4" readOnly="#{ReportBean.disabled}"
                              required="true"/>
            </af:panelFormLayout>
        </af:showDetailHeader>
        <af:spacer width="10" height="5" id="s1"/>
        <af:outputText value="Subreports" id="ot12" styleClass="subheader"/>
        <af:panelCollection id="pc1" styleClass="AFStretchWidth">
            <f:facet name="menus"/>
            <f:facet name="toolbar">
                <af:toolbar id="t3">
                    <af:group id="g2">
                        <af:button text="Add PAR" id="b3" action="#{ReportBean.addPAR_action}"
                                   disabled="#{ReportBean.report.id == null || !ReportBean.addParCar}"
                                   partialTriggers="::b1 ::b2"/>
                        <af:button text="Add CAR" id="b4" action="#{ReportBean.addCAR_action}"
                                   disabled="#{ReportBean.report.id == null || !ReportBean.addParCar}"
                                   partialTriggers="::b1 ::b2"/>
                        <af:button text="Add OBS" id="b5" action="#{ReportBean.addOBS_action}"
                                   disabled="#{ReportBean.report.id == null || ReportBean.disabled}"
                                   partialTriggers="::b1 ::b2"/>
                    </af:group>
                    <af:button text="Deactivate Selected" id="b7"
                               disabled="#{ReportBean.report.id == null || ReportBean.disabled}" partialTriggers="::b1 ::b2"
                               rendered="false"/>
                </af:toolbar>
            </f:facet>
            <f:facet name="statusbar"/>
            <af:table value="#{ReportBean.report.subReportList}" var="row" rows="5"
                      emptyText="No Subreports" rowBandingInterval="1" rowSelection="single" fetchSize="5"
                      varStatus="vs" id="t2" columnStretching="column:c5" summary="Subreports"
                      selectionListener="#{ReportBean.subreport_selectionListener}" autoHeightRows="5">
                <af:column sortProperty="subReportType"
                           sortable="true"
                           headerText="Type"
                           id="c1" rowHeader="true">
                    <af:outputText value="#{row.subReportType}"
                                   id="ot2"/>
                </af:column>
                <af:column sortProperty="subReportNum"
                           sortable="true"
                           headerText="Subreport Num."
                           id="c2">
                    <af:link text="#{row.subReportNum}" id="l1" action="subreport">
                        <af:setActionListener to="#{ReportBean.subreport}" from="#{row}"/>
                    </af:link>
                </af:column>
                <af:column sortProperty="letterNum}"
                           filterable="true" sortable="true"
                           headerText="Letter Num." id="c4">
                    <af:outputText value="#{row.letterNum}"
                                   id="ot5"/>
                </af:column>
                <af:column sortProperty="conditionDescription}"
                           sortable="true"
                           headerText="Description"
                           id="c5">
                    <af:outputText value="#{row.conditionDescription}"
                                   id="ot6"/>
                </af:column>
                <af:column id="c3" headerText="Deficiency Codes">
                    <af:outputText value="#{row.deficiencyCodeString}" id="ot13"/>
                </af:column>
                <af:column sortProperty="createDate"
                           sortable="true"
                           headerText="Date Created" id="c6">
                    <af:outputText value="#{row.createDate}"
                                   id="ot7">
                        <af:convertDateTime pattern="MMM/dd/yyyy"/>
                    </af:outputText>
                </af:column>
                <af:column sortProperty="createUserId"
                           sortable="true"
                           headerText="Created By"
                           id="c7">
                    <af:outputText value="#{row.createUserId}"
                                   id="ot8"/>
                </af:column>
                <af:column sortProperty="dueDate"
                           sortable="true"
                           headerText="Due Date" id="c8">
                    <af:outputText value="#{row.dueDate}"
                                   id="ot9">
                        <af:convertDateTime pattern="MMM/dd/yyyy"/>
                    </af:outputText>
                </af:column>
                <af:column sortProperty="lastModDate}"
                           sortable="true"
                           headerText="Last Modified"
                           id="c9">
                    <af:outputText value="#{row.lastModDate}"
                                   id="ot10">
                        <af:convertDateTime pattern="MMM/dd/yyyy"/>
                    </af:outputText>
                </af:column>
                <af:column sortProperty="lastModUserId"
                           sortable="true"
                           headerText="Last Modified By"
                           id="c10">
                    <af:outputText value="#{row.lastModUserId}"
                                   id="ot11"/>
                </af:column>
            </af:table>
        </af:panelCollection>
        <af:spacer width="10" height="10" id="s7"/>
        <af:toolbar id="t1" inlineStyle="float: right;">
            <af:button text="Transmittal" id="b101"
                       disabled="#{ReportBean.report.id == null || ReportBean.disabled}"
                       destination="#{ReportBean.CRTransimittalUrl}" targetFrame="_blank"
                       partialTriggers="b1 b2"/>
            <af:button text="Additional Report Info" id="b9" rendered="false">
                <af:showPopupBehavior popupId="p1"/>
            </af:button>
            <af:group id="g3">
                <af:button text="Save" id="b1" action="#{ReportBean.saveReport_action}"
                           disabled="#{ReportBean.disabled}"/>
                <af:button text="Cancel" id="b2" action="#{ReportBean.refreshReport_action}"
                           disabled="#{ReportBean.disabled}" immediate="true"/>
            </af:group>
            <af:button text="Return To Search" id="b8" action="return" immediate="true"/>
        </af:toolbar>
        <af:popup childCreation="deferred" autoCancel="disabled" id="p1">
            <af:dialog id="d1" title="Additional Report Info" type="ok">
                <f:facet name="buttonBar"/>
                <af:panelFormLayout id="pfl16" rows="10">
                    <af:inputDate value="#{ReportBean.report.datePerformed}"
                                  label="Date Performed"
                                  id="id1" readOnly="true">
                        <af:convertDateTime pattern="MMM/dd/yyyy"/>
                    </af:inputDate>
                    <af:inputText value="#{ReportBean.report.corpQaVendor}"
                                  label="Corp QA Vendor"
                                  id="it6" readOnly="true">
                    </af:inputText>
                    <af:inputText value="#{ReportBean.report.areaOps}"
                                  label="Area Ops"
                                  id="it11" readOnly="true">
                    </af:inputText>
                    <af:inputText value="#{ReportBean.report.usageCode}"
                                  label="Usage Code"
                                  id="it16" readOnly="true">
                    </af:inputText>
                    <af:group id="g1">
                        <af:inputText value="#{ReportBean.report.createUserId}"
                                      label="Created By"
                                      id="it17" readOnly="true">
                        </af:inputText>
                        <af:inputDate value="#{ReportBean.report.createDate}"
                                      label="Date Created"
                                      id="id2"
                                      readOnly="true">
                            <af:convertDateTime pattern="MMM/dd/yyyy"/>
                        </af:inputDate>
                        <af:inputText value="#{ReportBean.report.lastModUserId}"
                                      label="Modified By"                                      
                                      id="it18" readOnly="true">
                        </af:inputText>
                        <af:inputDate value="#{ReportBean.report.lastModDate}"
                                        label="Last Modified"
                                      id="id3"
                                      readOnly="true">
                            <af:convertDateTime pattern="MMM/dd/yyyy"/>
                        </af:inputDate>
                    </af:group>
                    <f:facet name="footer"/>
                </af:panelFormLayout>
            </af:dialog>
        </af:popup>
        <af:popup childCreation="deferred" autoCancel="disabled" id="p2">
            <af:dialog id="d2" title="QA Manager Project and Task Selection" type="yesNoCancel"
                       affirmativeTextAndAccessKey="Select Project Only" noTextAndAccessKey="Select Project and Task"
                       dialogListener="#{ReportBean.acep_dialogListener}">
                <f:facet name="buttonBar"/>
                <af:panelGroupLayout id="pgl6" layout="vertical">
                    <af:spacer width="10" height="10" id="s9"/>
                    <af:panelFormLayout id="pfl1">
                        <af:selectOneChoice label="ACEP"
                                            valueChangeListener="#{AcepBean.userProject_valueChangeListener}" id="soc3"
                                            unselectedLabel="Select a Project" autoSubmit="true" required="true"
                                            value="#{AcepBean.selectedProject}">
                            <f:selectItems value="#{QmgSession.userProjectItems}" id="si3"/>
                        </af:selectOneChoice>
                        <af:selectOneChoice label="Task" id="soc4" unselectedLabel="Select a Task (optional)"
                                            partialTriggers="soc3" autoSubmit="true" value="#{AcepBean.userTask}">
                            <f:selectItems value="#{AcepBean.userTaskItems}" id="si4"/>
                        </af:selectOneChoice>
                        <af:spacer width="10" height="10" id="s11"/>
                    </af:panelFormLayout>
                </af:panelGroupLayout>
            </af:dialog>
        </af:popup>
    </af:panelGroupLayout>
</ui:composition>
