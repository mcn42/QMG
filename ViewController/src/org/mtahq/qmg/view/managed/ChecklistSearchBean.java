package org.mtahq.qmg.view.managed;

import java.io.Serializable;

import java.util.List;

import org.mtahq.qmg.acep.AcepInfo;
import org.mtahq.qmg.session.QmgSessionEJBLocal;
import org.mtahq.qmg.view.util.ServiceLocator;
import org.mtahq.qmg.view.util.Utils;

public class ChecklistSearchBean   implements Serializable {
    private String projectNum = null;
    private AcepInfo acepInfo;
    boolean addOnly = false;
    boolean addMode = false;
    private Integer versionCount;
    private Integer masterVersion = 1;
    
    private QmgSessionEJBLocal ejbRef = ServiceLocator.getQmgSessionBean();
    
    public ChecklistSearchBean() {
        super();
    }

    public void setProjectNum(String projectNum) {
        this.projectNum = projectNum;
    }

    public String getProjectNum() {
        return projectNum;
    }

    public void setAcepInfo(AcepInfo acepInfo) {
        this.acepInfo = acepInfo;
        if(this.acepInfo != null) {
            this.versionCount = this.retrieveChecklistVersionCountForAcep(this.acepInfo);
        }
        else {
            this.versionCount = 0;
        }
    }

    public AcepInfo getAcepInfo() {
        return acepInfo;
    }

    public void setAddOnly(boolean addOnly) {
        this.addOnly = addOnly;
    }

    public boolean isAddOnly() {
        return addOnly;
    }

    public void setAddMode(boolean addMode) {
        this.addMode = addMode;
    }

    public boolean isAddMode() {
        return addMode;
    }

    public void setVersionCount(Integer versionCount) {
        this.versionCount = versionCount;
    }

    public void setMasterVersion(Integer masterVersion) {
        this.masterVersion = masterVersion;
    }

    public Integer getMasterVersion() {
        return masterVersion;
    }

    public Integer getVersionCount() {
        return versionCount;
    }

    public String checkChecklist_action() {
        int count = this.retrieveChecklistVersionCount(this.projectNum);
        if(count == 0) {
            this.addOnly = true;
            Utils.addWarningMessage("No Checklist exists for Project '" + this.projectNum + "'",
                                    "Use the 'New Checklist' button to create a new one.");
            return null;
        }
        return "checklist";
    }
    
    public String createChecklist_action() {
        int count = this.retrieveChecklistVersionCount(this.projectNum);
        if(count == 0) {
            Utils.addWarningMessage("No Checklist exists for Project '" + this.projectNum + "'",
                                    "A new Checklist will be created.");
        }
        return "checklist";
    }
    
    private int retrieveChecklistVersionCount(String projectNum) {
        List<Integer> list = this.ejbRef.getProjMonitorChecklistDistinctProjectVersions(projectNum);
        return list.size();
    }
    
    private int retrieveChecklistVersionCountForAcep(AcepInfo acepInfo) {
        List<Integer> list = this.ejbRef.getProjMonitorChecklistDistinctProjectVersionsForACEP(
            acepInfo.getAgency(), 
            acepInfo.getCategory(), 
            acepInfo.getElement(), 
            acepInfo.getProject());
        return list.size();
    }
    
    public String editAcepChecklist_action() {
        return "checklist";
    }
    
    public String createAcepChecklist_action() {
        
        return "checklist";
    }
}
