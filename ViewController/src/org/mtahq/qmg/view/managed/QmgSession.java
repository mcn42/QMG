package org.mtahq.qmg.view.managed;

import java.io.Serializable;

import java.security.Principal;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;


import javax.annotation.PostConstruct;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import javax.servlet.http.HttpSession;

import org.mtahq.qmg.entity.CmisStaff;
import org.mtahq.qmg.entity.ProjectStaff;
import org.mtahq.qmg.entity.QmgProject;
import org.mtahq.qmg.enumerations.RoleCodes;
import org.mtahq.qmg.session.QmgSessionEJBLocal;
import org.mtahq.qmg.util.QmgLogger;
import org.mtahq.qmg.view.util.ServiceLocator;


public class QmgSession implements Serializable {
    @SuppressWarnings("compatibility:8347336290687508322")
    private static final long serialVersionUID = 1L;
    private transient QmgSessionEJBLocal ejbRef = ServiceLocator.getQmgSessionBean();

    private String userId;
    private String role;
    private boolean admin;

    private CmisStaff staffEntity;
    private List<QmgProject> userProjects = new ArrayList<QmgProject>();
    private List<SelectItem> userProjectItems = new ArrayList<SelectItem>();
    boolean emailLogin = false;

    private boolean reportAdditionOK;


    public QmgSession() {
        super();
    }

    public void postActivate() {

    }

    //@PostConstruct
    public void postConstruct() {
        if (this.ejbRef == null)
            this.ejbRef = ServiceLocator.getQmgSessionBean();
        FacesContext ctx = FacesContext.getCurrentInstance();

        //  User ID could be a QMG ID or an ADLDS email address
        this.userId = ctx.getExternalContext().getRemoteUser();
        if (this.userId == null) {
            //  2nd try
            Principal principal = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
            if (principal != null)
                this.userId = principal.getName();
            if (this.userId == null) {
                //  unauthorized
                QmgLogger.getLogger().warning("Remote User was NULL");
                //this.userId = "1001273";  //  for testing only
                LogoutBean.redirectToUnauth();
                return;
            }
        }

        QmgLogger.getLogger().info("Staff Authentication ID: " + this.userId);
        HttpSession session = (HttpSession) ctx.getExternalContext().getSession(false);
        StringBuffer buff = new StringBuffer();
        buff.append("Session Login:\n");
        buff.append("Session ID:");
        buff.append(session.getId());
        buff.append("\n");
        buff.append("Auth ID:");
        buff.append(this.userId);
        buff.append("\n");
        QmgLogger.getLogger().info(buff.toString());
        //  Check existence in User table and retrieve role

        this.retrieveStaffRecord();
    }


    private void retrieveStaffRecord() {
        try {
            //this.staffEntity = this.ejbRef.getCmisStaffFindByBSCID(this.userId);
            this.emailLogin = this.userId.contains("@");
            if (this.emailLogin) {
                this.userId = this.userId.toLowerCase();
                this.staffEntity = this.ejbRef.getCmisStaffFindByEmail(this.userId);
            } else {
                this.userId = this.userId.toUpperCase();
                this.staffEntity = this.ejbRef.getCmisStaffFindByUserID(this.userId);
            }
            if (this.staffEntity == null) {
                QmgLogger.getLogger().severe(String.format("Staff entity was null for ID '%s'", this.userId));
                LogoutBean.redirectToUnauth();
                return;
            }
            this.userId = this.staffEntity.getUserId().toUpperCase();

            this.role = this.staffEntity.getRoleCode();
            //  'None' Role cannot log in, nor can a Login Status other than 'L'
            if (this.role.equals(RoleCodes.NONE.getCode()) || !this.staffEntity.getLoginStatus().equals("L")) {
                QmgLogger.getLogger().severe(String.format("Staff entity with ID '%s' had an invalid Role of '%s'",
                                                           this.userId, this.role));
                LogoutBean.redirectToUnauth();
                return;
            }
            this.admin = this.role.equals(RoleCodes.ADMINISTRATOR.getCode());
            this.reportAdditionOK =
                this.role.equals(RoleCodes.ADMINISTRATOR.getCode()) || this.role.equals(RoleCodes.QA_MANAGER.getCode());
            QmgLogger.getLogger().info(String.format("Staff Entity: '%s'", this.staffEntity.toString()));
            
            this.buildUserProjects();

            //  Switch BSC ID to user ID after BSC authentication
            this.userId = this.staffEntity.getUserId().toUpperCase();
        } catch (Exception e) {
            QmgLogger.getLogger().log(Level.SEVERE, "Cmis Staff record retrieval error", e);
            LogoutBean.redirectToUnauth();
        }
    }

    private void buildUserProjects() {
        //  Admins see all Projects
        if (this.admin) {
            this.userProjects = this.ejbRef.getQmgProjectFindAll();
            for(QmgProject proj:this.userProjects) {
                SelectItem si = new SelectItem(proj);
                this.userProjectItems.add(si);
            }
        } else {
            List<ProjectStaff> list = this.staffEntity.getProjectStaffList();
            QmgLogger.getLogger().info(String.format("Found %s Projects for Staff member %s", list.size(),this.staffEntity.getCmisStaffId()));
            for (ProjectStaff ps : list) {
                this.userProjects.add(ps.getQmgProject());
                SelectItem si = new SelectItem(ps.getQmgProject());
                this.userProjectItems.add(si);
            }
        }
    }
    
    //	public void refreshUserProjects() {
    //	    //this.staffEntity = (CmisStaff) this.ejbRef.refreshEntity(this.staffEntity.get , CmisStaff.class);
    //	    this.staffEntity = this.ejbRef.getCmisStaffFindByBSCID(this.userId);
    //		this.userProjectItems.clear();
    //	    for (ProjectStaff ps : this.staffEntity.getProjectStaffList()) {
    //	        this.userProjects.add(ps.getQmgProject());
    //	        SelectItem si = new SelectItem(ps.getQmgProject());
    //	        this.userProjectItems.add(si);
    //	    }
    //	}

    public void refreshUserProjects() {
        this.staffEntity = (CmisStaff) this.ejbRef.refreshEntity(this.staffEntity.getCmisStaffId(), CmisStaff.class);
        this.userProjectItems.clear();
        for (ProjectStaff ps : this.staffEntity.getProjectStaffList()) {
            this.userProjects.add(ps.getQmgProject());
            SelectItem si = new SelectItem(ps.getQmgProject());
            this.userProjectItems.add(si);
        }
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isAdmin() {
        return admin;
    }

    public List<QmgProject> getUserProjects() {
        return userProjects;
    }

    public List<SelectItem> getUserProjectItems() {
        return userProjectItems;
    }

    public CmisStaff getStaffEntity() {
        //if(this.staffEntity == null) this.retrieveStaffRecord();
        return staffEntity;
    }

    public boolean isReportAdditionOK() {
        return reportAdditionOK;
    }
}
