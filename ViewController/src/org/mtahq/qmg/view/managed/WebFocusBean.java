package org.mtahq.qmg.view.managed;

import java.io.Serializable;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;

import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.myfaces.trinidad.event.LaunchEvent;

import org.mtahq.qmg.acep.AcepInfo;
import org.mtahq.qmg.entity.CmisStaff;
import org.mtahq.qmg.enumerations.MainReportTypes;
import org.mtahq.qmg.enumerations.SubReportTypes;
import org.mtahq.qmg.util.QmgLogger;
import org.mtahq.qmg.view.util.Utils;

public class WebFocusBean implements Serializable {
    public final static String PARM_SEPARATOR = "&";
    public final static String START_DATE_PARM = "STARTDATE";
    public final static String END_DATE_PARM = "ENDDATE";
    public final static String REPORT_TYPE_PARM = "RPTTYPE";
    public final static String QAM_PARM = "QAM";
    public final static String USAGE_CODE_PARM = "UC";
    public final static String PROJECT_NUM_PARM = "PN";
    public final static String FORMAT_PARM = "OF";

    public final static String AGENCY_PARM = "A";
    public final static String CATEGORY_PARM = "C";
    public final static String ELEMENT_PARM = "E";
    public final static String PROJECT_PARM = "P";
    public final static String TASK_PARM = "TASK";

    public final static String WF_DATE_FORMAT = "yyyyMMdd";
    private final static String MAIN_REPORT_NAME = "QMG_MAIN_REPORTS";
    private final static String SUBREPORT_NAME = "QMG_SUB_REPORTS";
    private final static String CHECKLIST_REPORT_NAME = "qmg_project_monitoring_report_checklist";

    private SimpleDateFormat df = new SimpleDateFormat(WF_DATE_FORMAT);

    private String baseUrl;

    private String reportName = MAIN_REPORT_NAME;
    private String reportType = "QASR";
    private String projectNum = null;
    private Date startDate;
    private Date endDate;

    private CmisStaff staff = null;
    private List<SelectItem> typeItems;
    private List<SelectItem> reportTypeItems;
    private List<SelectItem> subreportTypeItems;
    private String fullReportUrl;
    private String typeInd = "M";

    private String checklistReportUrl;
    private String checklistProjectNum = "";
    private AcepInfo acepInfo;
    private String usageCode = "1";
    private String outputFormat = "EXCEL";

    public WebFocusBean() {
        super();
        initializeDates();
        this.buildReportTypes();
        this.typeItems = this.reportTypeItems;
    }

    private void initializeDates() {
        GregorianCalendar cal = new GregorianCalendar();
        cal.set(Calendar.HOUR, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        this.endDate = cal.getTime();
        cal.set(Calendar.MONTH, Calendar.JANUARY);
        cal.set(Calendar.DATE, 1);
        cal.set(Calendar.YEAR, 2004);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 1);
        this.startDate = cal.getTime();
    }

    @PostConstruct
    public void postConstruct() {
        this.baseUrl = Utils.getContextParameter("WEBFOCUS_URL");
        QmgLogger.getLogger().info(String.format("WebFocus bean created with base URL '%s'", this.baseUrl));
    }

    private void buildReportTypes() {
        this.reportTypeItems = new ArrayList<SelectItem>();
        for (MainReportTypes t : MainReportTypes.values()) {
            SelectItem si = new SelectItem(t.toString());
            this.reportTypeItems.add(si);
        }
        this.subreportTypeItems = new ArrayList<SelectItem>();
        for (SubReportTypes t : SubReportTypes.values()) {
            SelectItem si = new SelectItem(t.toString());
            this.subreportTypeItems.add(si);
        }
    }

    public void buildReportUrl() {
        String start = df.format(this.startDate);
        String end = df.format(this.endDate);
        StringBuilder sb =
            new StringBuilder().append(baseUrl).append(reportName).append(PARM_SEPARATOR).append(REPORT_TYPE_PARM).append("=").append(this.reportType).append(PARM_SEPARATOR).append(START_DATE_PARM).append("=").append(start).append(PARM_SEPARATOR).append(END_DATE_PARM).append("=").append(end);

        //        if(this.projectNum != null  && this.projectNum.length() > 0) {
        //            sb.append(PARM_SEPARATOR)
        //            .append(PROJECT_NUM_PARM)
        //            .append("=")
        //            .append(this.projectNum);
        //        }

        if (this.acepInfo != null) {
            sb.append(PARM_SEPARATOR).append(AGENCY_PARM).append("=").append(this.acepInfo.getAgency());
            sb.append(PARM_SEPARATOR).append(CATEGORY_PARM).append("=").append(this.acepInfo.getCategory());
            sb.append(PARM_SEPARATOR).append(ELEMENT_PARM).append("=").append(this.acepInfo.getElement());
            sb.append(PARM_SEPARATOR).append(PROJECT_PARM).append("=").append(this.acepInfo.getProject());

            if (this.acepInfo.getTask() != null) {
                sb.append(PARM_SEPARATOR).append(TASK_PARM).append("=").append(this.acepInfo.getTask());
            }
        }

        if (this.staff != null) {
            sb.append(PARM_SEPARATOR).append(QAM_PARM).append("=").append(this.staff.getCmisStaffId());
        }
        
        sb.append(PARM_SEPARATOR).append(FORMAT_PARM).append("=").append(this.outputFormat);

        this.fullReportUrl = sb.toString();
        QmgLogger.getLogger().info(String.format("WebFocus Report URL built: '%s'", this.fullReportUrl));
    }

    private void buildChecklistUrl() {
        StringBuilder sb = new StringBuilder().append(baseUrl).append(CHECKLIST_REPORT_NAME);

        if (this.usageCode != null && !this.usageCode.equals("A")) {
            sb.append(PARM_SEPARATOR).append(USAGE_CODE_PARM).append("=").append(this.usageCode);
        }

        if (this.checklistProjectNum != null && this.checklistProjectNum.length() > 0) {
            sb.append(PARM_SEPARATOR).append(PROJECT_PARM).append("=").append(this.checklistProjectNum);
        }

        if (this.acepInfo != null) {
            //  TODO: Set up ACEP parameters

        }

        this.checklistReportUrl = sb.toString();
        QmgLogger.getLogger().info(String.format("Checklist Report URL built: '%s'", this.checklistReportUrl));
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getReportType() {
        return reportType;
    }

    public void setProjectNum(String projectNum) {
        this.projectNum = projectNum;
    }

    public String getProjectNum() {
        return projectNum;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String launchReport_action() {
        return null;
    }

    public String clearParms_action() {
        this.projectNum = null;
        this.acepInfo = null;
        this.staff = null;
        this.initializeDates();
        this.typeItems = this.reportTypeItems;
        this.reportType = "QASR";
        this.typeInd = "M";
        this.reportName = MAIN_REPORT_NAME;
        return null;
    }

    public String getFullReportUrl() {
        this.buildReportUrl();
        return fullReportUrl;
    }

    public void setStaff(CmisStaff staff) {
        this.staff = staff;
    }

    public CmisStaff getStaff() {
        return staff;
    }

    public List<SelectItem> getTypeItems() {
        return typeItems;
    }

    public String getChecklistReportUrl() {
        this.buildChecklistUrl();
        return checklistReportUrl;
    }

    public void setChecklistProjectNum(String checklistProjectNum) {
        this.checklistProjectNum = checklistProjectNum;
    }

    public String getChecklistProjectNum() {
        return checklistProjectNum;
    }

    public void setAcepInfo(AcepInfo acepInfo) {
        this.acepInfo = acepInfo;
    }

    public AcepInfo getAcepInfo() {
        return acepInfo;
    }

    public void setUsageCode(String configuration) {
        this.usageCode = configuration;
    }

    public String getUsageCode() {
        return usageCode;
    }

    public void setOutputFormat(String outputFormat) {
        this.outputFormat = outputFormat;
    }

    public String getOutputFormat() {
        return outputFormat;
    }

    public void startDate_valueChangeListener(ValueChangeEvent vce) {
        Date d = (Date) vce.getNewValue();
        this.startDate = d;
        this.buildReportUrl();
    }

    public void endDate_valueChangeListener(ValueChangeEvent vce) {
        Date d = (Date) vce.getNewValue();
        this.endDate = d;
        this.buildReportUrl();
    }

    public void reportType_valueChangeListener(ValueChangeEvent vce) {
        String s = (String) vce.getNewValue();
        this.reportType = s;
        this.buildReportUrl();
    }

    public void qam_valueChangeListener(ValueChangeEvent vce) {
        CmisStaff s = (CmisStaff) vce.getNewValue();
        this.staff = s;
        this.buildReportUrl();
    }

    public void projectNum_valueChangeListener(ValueChangeEvent vce) {
        String s = (String) vce.getNewValue();
        this.projectNum = s;
        this.buildReportUrl();
    }

    public void checklistProjectNum_valueChangeListener(ValueChangeEvent vce) {
        String s = (String) vce.getNewValue();
        this.checklistProjectNum = s;
        this.buildChecklistUrl();
    }

    public void usageCode_valueChangeListener(ValueChangeEvent vce) {
        String s = (String) vce.getNewValue();
        this.usageCode = s;
        this.buildChecklistUrl();
    }

    public void report_launchListener(LaunchEvent launchEvent) {
        QmgLogger.getLogger().info("WebFocus Report launhed.");
    }

    public void types_valueChangeListener(ValueChangeEvent vce) {
        String val = (String) vce.getNewValue();
        this.typeInd = val;
        if (val.equals("M")) {
            this.typeItems = this.reportTypeItems;
            this.reportType = "QASR";
            this.reportName = MAIN_REPORT_NAME;
        } else {
            this.typeItems = this.subreportTypeItems;
            this.reportType = "PAR";
            this.reportName = SUBREPORT_NAME;
        }
    }

    public void setTypeInd(String typeInd) {
        this.typeInd = typeInd;
    }

    public String getTypeInd() {
        return typeInd;
    }

    public String resetChecklistFields_action() {
        this.usageCode = "A";
        this.checklistProjectNum = null;
        this.acepInfo = null;
        return null;
    }

    public void outputFormat_valueChangeListener(ValueChangeEvent vce) {
        String s = (String) vce.getNewValue();
        this.outputFormat = s;
        this.buildReportUrl();
    }
}
