package org.mtahq.qmg.view.managed;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.util.logging.Level;

import javax.annotation.PostConstruct;

import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.mtahq.qmg.acep.AcepInfo;
import org.mtahq.qmg.acep.session.AcepSessionEJBLocal;
import org.mtahq.qmg.entity.CmisStaff;
import org.mtahq.qmg.entity.ProjectStaff;
import org.mtahq.qmg.entity.QmgProject;
import org.mtahq.qmg.session.QmgSessionEJBLocal;
import org.mtahq.qmg.util.QmgLogger;
import org.mtahq.qmg.view.util.ServiceLocator;
import org.mtahq.qmg.view.util.Utils;

public class ProjectManagementBean {
    private QmgSessionEJBLocal ejbRef = ServiceLocator.getQmgSessionBean();
    private AcepSessionEJBLocal acepEjb = ServiceLocator.getAcepSessionBean();

    private QmgProject selectedProject;
    private List<QmgProject> allProjects;
    private List<QmgProject> filteredProjects;
    private AcepInfo acepInfo;

    private List<SelectItem> allQAManagerItems = new ArrayList<SelectItem>();
    private List<SelectItem> remainingQAManagerItems;
    private List<CmisStaff> assistantManagers;
    private List<CmisStaff> allQAManagers;
    private CmisStaff primaryManager;
    private String originalStatus;

    private String userId;
    private QmgSession session = null;
    private List<CmisStaff> activeStaff;

    public ProjectManagementBean() {
        super();
    }

    @PostConstruct
    public void postConstruct() {
        this.allProjects = this.ejbRef.getQmgProjectFindAll();
        this.activeStaff = this.ejbRef.getCmisStaffFindActive();
        for (CmisStaff staff : this.activeStaff) {
            SelectItem si = new SelectItem(staff, staff.getFullName());
            this.allQAManagerItems.add(si);
        }

    }

    public void prepareStaff() {

    }

    public void setSelectedProject(QmgProject selectedProject) {
        this.selectedProject = selectedProject;
        if (this.selectedProject != null) {
            this.originalStatus = this.selectedProject.getStatus();
            this.loadProjectStaff();
        }
    }

    public QmgProject getSelectedProject() {
        return selectedProject;
    }

    public void setAllProjects(List<QmgProject> allProjects) {
        this.allProjects = allProjects;
    }

    public List<QmgProject> getAllProjects() {
        return allProjects;
    }

    public void setFilteredProjects(List<QmgProject> filteredProjects) {
        this.filteredProjects = filteredProjects;
    }

    public List<QmgProject> getFilteredProjects() {
        return filteredProjects;
    }

    public void setAcepInfo(AcepInfo acepInfo) {
        this.acepInfo = acepInfo;
        this.acepSelected();
    }

    public AcepInfo getAcepInfo() {
        return acepInfo;
    }

    public void setAssistantManagers(List<CmisStaff> assistantManagers) {
        this.assistantManagers = assistantManagers;
    }

    public List<CmisStaff> getAssistantManagers() {
        return assistantManagers;
    }

    public void setPrimaryManager(CmisStaff primaryManager) {
        this.primaryManager = primaryManager;
    }

    public CmisStaff getPrimaryManager() {
        return primaryManager;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public List<SelectItem> getAllQAManagers() {
        return allQAManagerItems;
    }

    public List<SelectItem> getRemainingQAManagers() {
        return remainingQAManagerItems;
    }

    public void setSession(QmgSession session) {
        this.session = session;
    }

    public QmgSession getSession() {
        return session;
    }

    public void acepSelected() {
        if (this.acepInfo == null)
            return;
        QmgProject proj =
            this.ejbRef.getQmgProjectFindByACEP(this.acepInfo.getAgency(), this.acepInfo.getCategory(),
                                                this.acepInfo.getElement(), this.acepInfo.getProject());
        if (proj != null) {
            Utils.addWarningMessage(String.format("The Project for ACEP '%s' is already included as a QMG Project.",
                                                  this.acepInfo), "No action was taken.");
            this.setSelectedProject(proj);
            return;
        }


        proj = new QmgProject();
        proj.setAgency(this.acepInfo.getAgency());
        proj.setCategory(this.acepInfo.getCategory());
        proj.setElement(this.acepInfo.getElement());
        proj.setProject(this.acepInfo.getProject());
        proj.setCreatedBy(this.userId);
        proj.setCreatedOn(new Date());
        proj.setStatus("A");
        try {
            proj = this.ejbRef.persistEntity(proj);
        } catch (Exception e) {
            Utils.addErrorMessage("An error occurred while adding the Project", e);
        }
        Utils.addInfoMessage("The Prjoject was added successfully.", "");
        this.setSelectedProject(proj);
    }

    public void primaryManager_valueChangeListener(ValueChangeEvent vce) {
        this.remainingQAManagerItems = new ArrayList<SelectItem>();
        CmisStaff s = (CmisStaff) vce.getNewValue();
        this.primaryManager = s;
        if (this.primaryManager == null)
            return;
        //  Remove Primary Mgr. from Available Assistants list
        for (SelectItem si : this.allQAManagerItems) {
            CmisStaff staff = (CmisStaff) si.getValue();
            if (staff.equals(this.primaryManager))
                continue;
            this.remainingQAManagerItems.add(si);
        }
    }

    private void loadProjectStaff() {
        //this.removeDuplicates();
        this.remainingQAManagerItems = new ArrayList<SelectItem>();
        //this.remainingQAManagerItems.addAll(this.allQAManagerItems);
        this.assistantManagers = new ArrayList<CmisStaff>();
        this.primaryManager = null;

        for (ProjectStaff proj : this.selectedProject.getProjectStaffList1()) {
            CmisStaff s = proj.getCMIS_STAFF();
            if (proj.getProjectRole().equals("P")) {
                this.primaryManager = s;
            } else {
                this.assistantManagers.add(s);
            }
        }

        //  Remove Primary Mgr. from Available Assistants list
        for (SelectItem si : this.allQAManagerItems) {
            CmisStaff staff = (CmisStaff) si.getValue();
            if (staff.equals(this.primaryManager))
                continue;
            //            if(this.assistantManagers.contains(staff))
            //                continue;
            this.remainingQAManagerItems.add(si);
        }
    }

    public String saveStaff_action() {
        try {
            int indexOnList = allProjects.indexOf(this.selectedProject);
            Long projId = this.selectedProject.getId();
            //  Disconnect ProjectStaff from Project and Staff
            for (ProjectStaff ps : this.selectedProject.getProjectStaffList1()) {
                this.ejbRef.removeProjectStaff(ps);
            }
            this.selectedProject.getProjectStaffList1().clear();
            this.selectedProject.setUpdatedOn(new Date());
            this.selectedProject.setUpdatedBy(this.userId);
            //  Update to delete all ProjectStaff
            this.selectedProject = this.ejbRef.mergeEntity(this.selectedProject);

            ArrayList<ProjectStaff> newStaffList = new ArrayList<ProjectStaff>();
            //---- manager ------
            String role = "P";
            // this.addProjectStaff(this.primaryManager, role);
            newStaffList.add(buildProjectStaff(this.primaryManager, role));

            role = "A";
            for (CmisStaff staff : this.assistantManagers) {
                newStaffList.add(buildProjectStaff(staff, role));
            }
            this.selectedProject.setProjectStaffList1(newStaffList);

            this.selectedProject = this.ejbRef.mergeEntity(this.selectedProject);
            // QmgLogger.getLogger().log(Level.INFO, "---new staff count after merge= " + this.selectedProject.getStaffCount());
            //allProjects.set(indexOnList, this.selectedProject);   // refresh the row on projects list -- MJ
            this.allProjects = this.ejbRef.getQmgProjectFindAll();
            Utils.addInfoMessage("The Prjoject Staff was saved successfully.", "");

            //  Refresh current user
            boolean currentUserUpdated = false;
            
            for (CmisStaff staff : this.assistantManagers) {
                currentUserUpdated = staff.getUserId().equals(this.userId);
                if (currentUserUpdated)
                    break;
            }
            if (!currentUserUpdated)
                currentUserUpdated = this.primaryManager.getUserId().equals(this.userId);
            if (currentUserUpdated) {
                this.session.refreshUserProjects();
            }
            this.activeStaff = this.ejbRef.getCmisStaffFindActive();
            this.logProject(this.selectedProject);

        } catch (Exception e) {
            QmgLogger.getLogger().log(Level.SEVERE, "Save Project Staff error", e.getMessage());
            Utils.addErrorMessage("An error occurred while saving the Project Staff", e);
        }
        return null;
    }
    
    private void logProject(QmgProject pr) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Project Staff for %s/%s/%s/%s:\n",pr.getAgency(),pr.getCategory(),pr.getElement(),pr.getProject()));
        QmgProject proj = ejbRef.refreshProject(pr);
        for(ProjectStaff ps:proj.getProjectStaffList1()) {
            sb.append(String.format("\t%s: %s\n", ps.getCMIS_STAFF().getUserId(),ps.getProjectRole()));
        }
        QmgLogger.getLogger().warning(sb.toString());
    }

    private ProjectStaff buildProjectStaff(CmisStaff staff, String role) {
        Date d = new Date();

        ProjectStaff ps = new ProjectStaff();
        ps.setQmgProject(this.getSelectedProject());
        ps.setCMIS_STAFF(staff);
        ps.setProjectRole(role);
        ps.setCreatedBy(this.userId);
        ps.setCreatedOn(d);
        ps.setStatus("A");

        return ps;
    }

    public List<CmisStaff> getActiveStaff() {
        return activeStaff;
    }

}
