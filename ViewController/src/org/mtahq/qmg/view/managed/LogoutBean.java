package org.mtahq.qmg.view.managed;

import java.io.IOException;

import java.util.logging.Level;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oracle.adf.controller.ControllerContext;

import org.mtahq.qmg.util.QmgLogger;


public class LogoutBean {
    public LogoutBean() {
        super();
    }

    public String logout_action() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = ctx.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) ectx.getResponse();

        HttpSession session = (HttpSession) ectx.getSession(false);

        StringBuffer buff = new StringBuffer();
        buff.append("Session Logout:\n");
        buff.append("Session ID:");
        buff.append(session.getId());
        buff.append("\n");
        buff.append("User ID:");
        buff.append(ectx.getRemoteUser());
        buff.append("\n");
        QmgLogger.getLogger().info(buff.toString());

        session.invalidate();

        HttpServletRequest request = (HttpServletRequest) ectx.getRequest();
        try {
            String logoutUrl = request.getContextPath() + "/faces/logout.jsf";
            response.sendRedirect(logoutUrl);
            ctx.responseComplete();
            //ServiceLocator.getLogger().fine("Logout URL: " + logoutUrl);
        } catch (IOException ioe) {
            QmgLogger.getLogger().log(Level.SEVERE, "Logout error", ioe);
        }
        return null;
    }

    public static void redirectToUnauth() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = ctx.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) ectx.getResponse();

        HttpSession session = (HttpSession) ectx.getSession(false);

        StringBuffer buff = new StringBuffer();
        buff.append("Unauthorized Login -");
        buff.append("User ID: ");
        buff.append(ectx.getRemoteUser());
        buff.append("\n");
        QmgLogger.getLogger().warning(buff.toString());

        String viewId = "Unauthorized.jsf";
        ControllerContext controllerCtx = null;
        controllerCtx = ControllerContext.getInstance();
        String activityURL = controllerCtx.getGlobalViewActivityURL(viewId);
        try {
            ectx.redirect(activityURL);
            if(ectx.getRemoteUser() != null)
              session.invalidate();
        } catch (Exception e) {
            QmgLogger.getLogger().log(Level.WARNING, "Unauthorized Redirect error", e);
        }
    }
}
