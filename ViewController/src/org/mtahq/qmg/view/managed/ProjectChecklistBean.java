package org.mtahq.qmg.view.managed;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.mtahq.qmg.acep.AcepInfo;
import org.mtahq.qmg.entity.CmisStaff;
import org.mtahq.qmg.entity.MasterProjMonitorChecklist;
import org.mtahq.qmg.entity.ProjMonitorChecklist;
import org.mtahq.qmg.session.QmgSessionEJBLocal;
import org.mtahq.qmg.view.util.ServiceLocator;
import org.mtahq.qmg.view.util.Utils;

public class ProjectChecklistBean implements Serializable  {
    private QmgSessionEJBLocal ejbRef = ServiceLocator.getQmgSessionBean();

    private Integer masterVersion = new Integer(1);

    private Integer projectVersion;
    private Integer lastProjectVersion;
    private List<SelectItem> projectVersionItems;

    private String projectNumber;

    private List<ProjMonitorChecklist> checklist;
    private boolean editable = true;
    private String userId = null;
    private boolean addOnly = false;
    private boolean addMode = false;
    private AcepInfo acepInfo;
    
    private Integer usageCode =  new Integer(1);
    
    

    public ProjectChecklistBean() {
        super();
    }

    private void buildNewListForSeries() {
        this.projectVersion = ++this.lastProjectVersion;
        SelectItem si = new SelectItem(this.projectVersion);
        this.projectVersionItems.add(si);
        List<MasterProjMonitorChecklist> masterItems =
            this.ejbRef.getMasterProjMonitorChecklistFindForVersionNumber(this.masterVersion);
        this.checklist = new ArrayList<ProjMonitorChecklist>();
        for (MasterProjMonitorChecklist mi : masterItems) {
            ProjMonitorChecklist pmc = new ProjMonitorChecklist(mi);
            pmc.setAgency(this.acepInfo.getAgency());
            pmc.setCategory(this.acepInfo.getCategory());
            pmc.setElement(this.acepInfo.getElement());
            pmc.setProject(this.acepInfo.getProject());
            pmc.setProjectNum(this.acepInfo.getProject());
            pmc.setProjectVersionNum(this.projectVersion);
            pmc.setUsageCode(this.usageCode);
            this.checklist.add(pmc);
        }
    }

    

    private void retrieveChecklist() {
        this.checklist =
            this.ejbRef.getProjMonitorChecklistFindForProjectVersion(this.projectNumber, this.projectVersion);
        for (ProjMonitorChecklist pmc : this.checklist) {
            if (pmc.getStatus() == null)
                pmc.setStatus("NY");
            pmc.setStatus(pmc.getStatus().trim());
            if (pmc.getMasterItem() == null) {
                MasterProjMonitorChecklist mi =
                    this.ejbRef.getMasterProjMonitorChecklistFindForVersionAndItem(pmc.getMasterVersionNum(),
                                                                                   pmc.getItemNum());
                pmc.setMasterItem(mi);
            }
        }
        this.masterVersion = this.checklist.get(0).getMasterVersionNum();
    }

    public String retrieveVersionsForProject() {
        this.addOnly = false;
        this.projectVersion = 0;
        this.projectVersionItems = new ArrayList<SelectItem>();
        List<Integer> versions = this.ejbRef.getProjMonitorChecklistDistinctProjectVersions(this.projectNumber);
        if (versions.size() == 0) {
            Utils.addInfoMessage("No Checklists exist for ACEP '" + this.acepInfo + "'",
                                    "A new Checklist version has been created, but not saved.");
            this.projectVersionItems = new ArrayList<SelectItem>();
            this.lastProjectVersion = new Integer(0);
            this.buildNewListForSeries();
            return "continue";
        }
        for (Integer v : versions) {
            SelectItem si = new SelectItem(v);
            this.projectVersionItems.add(si);
            this.lastProjectVersion = v;
        }
        this.projectVersion = this.lastProjectVersion;
        if (this.projectVersion > 0) {
            this.retrieveChecklist();
        }
        
        return "continue";
    }

    public void setMasterVersion(Integer seriesId) {
        this.masterVersion = seriesId;
    }

    public Integer getMasterVersion() {
        return masterVersion;
    }

    public void setChecklist(List<ProjMonitorChecklist> checklist) {
        this.checklist = checklist;
    }

    public List<ProjMonitorChecklist> getChecklist() {
        return checklist;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setProjectVersion(Integer projectVersion) {
        this.projectVersion = projectVersion;
    }

    public Integer getProjectVersion() {
        return projectVersion;
    }

    public Integer getLastProjectVersion() {
        return lastProjectVersion;
    }

    public List<SelectItem> getProjectVersiontems() {
        return projectVersionItems;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setAddMode(boolean addMode) {
        this.addMode = addMode;
    }

    public boolean isAddMode() {
        return addMode;
    }

    public void setAcepInfo(AcepInfo acepInfo) {
        this.acepInfo = acepInfo;
    }

    public AcepInfo getAcepInfo() {
        return acepInfo;
    }

    public void status_valueChangeListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
    }

    public void version_valueChangeListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
    }

    public String addNewVersion_action() {
        this.buildNewListForSeries();
        FacesContext.getCurrentInstance().addMessage(null,
                                                     new FacesMessage("A new Checklist version has been created.", ""));
        return "newVersion";
    }
    
    public String addInitialVersion_action() {
        this.projectVersion = 0;
        this.projectVersionItems = new ArrayList<SelectItem>();
        this.buildNewListForSeries();
        FacesContext.getCurrentInstance().addMessage(null,
                                                     new FacesMessage("A new Checklist has been created.", ""));
        return "newVersion";
    }

    public String revert_action() {
        if(this.checklist.get(0).getId() != null)
          this.retrieveChecklist();
        return null;
    }

    public String saveChecklist_action() {
        Date d = new Date();
        for (ProjMonitorChecklist pmc : this.checklist) {
            pmc.setLastModUserId(this.userId);
            pmc.setLastModDate(d);
            if (pmc.getId() == null) {
                if (pmc.getStatus() == null) pmc.setStatus("NY");
                pmc.setCreateDate(d);
                pmc.setCreateUserId(this.userId);
                pmc.setProjectVersionDate(d);
                pmc.setPmrStatusVersionNum(1);
                pmc = this.ejbRef.persistEntity(pmc);
            } else {
                pmc = this.ejbRef.mergeEntity(pmc);
            }          
        }
        Utils.addInfoMessage("The Checklist was saved successfully.", "");
        return null;
    }
}
