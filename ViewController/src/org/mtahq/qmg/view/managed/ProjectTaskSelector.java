package org.mtahq.qmg.view.managed;

import java.io.Serializable;

import java.util.List;

import org.apache.myfaces.trinidad.model.ChildPropertyTreeModel;

import org.mtahq.qmg.acep.model.ImpPlanProject;
import org.mtahq.qmg.acep.model.ImpPlanTask;
import org.mtahq.qmg.acep.session.AcepSessionEJBLocal;
import org.mtahq.qmg.view.util.ServiceLocator;


public class ProjectTaskSelector  implements Serializable {
    private ImpPlanTask task;
    private ImpPlanProject project;
    
    private String searchKey;
    private String searchMode = "P";
    
    private AcepSessionEJBLocal acepEjb = ServiceLocator.getAcepSessionBean();   
    private transient ChildPropertyTreeModel resultRoot;
    
    public ProjectTaskSelector() {
        super();
    }

    public void setTask(ImpPlanTask task) {
        this.task = task;
    }

    public ImpPlanTask getTask() {
        return task;
    }

    public void setProject(ImpPlanProject project) {
        this.project = project;
    }

    public ImpPlanProject getProject() {
        return project;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchMode(String searchMode) {
        this.searchMode = searchMode;
    }

    public String getSearchMode() {
        return searchMode;
    }

    public ChildPropertyTreeModel getResultRoot() {
        return resultRoot;
    }

    public String search_action() {
        // Add event code here...
        return null;
    }
    
    private void buildProjectResultTree() {
        List<ImpPlanProject> matchingProjects = this.acepEjb.getImpPlanProjectFindByProject(this.searchKey + "%");
        for(ImpPlanProject proj:matchingProjects) {
            
        }
    }
    
    private void buildProjectTaskResultTree() {
        
    }
    
    private class TreeNode {
        
    }
}
