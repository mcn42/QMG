package org.mtahq.qmg.view.managed;

import java.io.Serializable;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.myfaces.trinidad.model.ChildPropertyTreeModel;

import org.mtahq.qmg.entity.CmisStaff;
import org.mtahq.qmg.session.QmgSessionEJBLocal;
import org.mtahq.qmg.view.util.ServiceLocator;

public class UserTreeBean implements Serializable {
    private ChildPropertyTreeModel model;
    private QmgSessionEJBLocal ejbRef = ServiceLocator.getQmgSessionBean();
    
    public UserTreeBean() {
        super();
    }
    
    @PostConstruct
    public void postConstruct() {
        List<CmisStaff> staff = this.ejbRef.getCmisStaffFindActive();
        this.model = new ChildPropertyTreeModel(staff,"userProjects");
    }


    public ChildPropertyTreeModel getModel() {
        return model;
    }
}
