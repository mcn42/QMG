package org.mtahq.qmg.view.managed;

import java.io.Serializable;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.PostConstruct;

import javax.el.ValueExpression;

import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.event.DialogEvent;

import org.apache.myfaces.trinidad.event.SelectionEvent;

import org.mtahq.qmg.acep.AcepInfo;
import org.mtahq.qmg.acep.model.ImpPlanProject;
import org.mtahq.qmg.acep.model.ImpPlanTask;
import org.mtahq.qmg.acep.session.AcepSessionEJBLocal;
import org.mtahq.qmg.beans.SequenceBeanInterface;
import org.mtahq.qmg.entity.CmisStaff;
import org.mtahq.qmg.entity.CorpQaMainReport;
import org.mtahq.qmg.entity.CorpQaSubReport;
import org.mtahq.qmg.entity.CqaDatePerformed;
import org.mtahq.qmg.enumerations.MainReportTypes;
import org.mtahq.qmg.enumerations.RoleCodes;
import org.mtahq.qmg.enumerations.SubReportTypes;
import org.mtahq.qmg.exception.QmgException;
import org.mtahq.qmg.session.QmgSessionEJBLocal;
import org.mtahq.qmg.util.QmgLogger;
import org.mtahq.qmg.view.util.ServiceLocator;
import org.mtahq.qmg.view.util.Utils;

public class ReportBean implements Serializable {
    @SuppressWarnings("compatibility:-7649875889260388453")
    private static final long serialVersionUID = 1L;
    private SequenceBeanInterface sequenceBean = ServiceLocator.getSequenceBean();
    private QmgSessionEJBLocal ejbRef = ServiceLocator.getQmgSessionBean();
    private AcepSessionEJBLocal acepEjb = ServiceLocator.getAcepSessionBean();

    private CorpQaMainReport report;
    private CorpQaSubReport subreport;
    private MainReportTypes reportType;
    private SubReportTypes subreportType;

    private ImpPlanProject project;
    private ImpPlanTask task;
    private Date originalDatePerformed;

    private String projectTaskString = "";

    private AcepInfo acepInfo;
    private CmisStaff staffEntity;
    private String userId;

    private boolean disabled = false;
    private boolean noProject = false;
    private boolean admin;
    boolean addParCar = false;

    private String reportBaseUrl = null;
    private String reportCRBaseUrl = null;

    public ReportBean() {
        super();
    }

    @PostConstruct
    public void posstConstruct() {
        this.reportBaseUrl = Utils.getContextParameter("WEBFOCUS_URL");
        this.reportCRBaseUrl = Utils.getContextParameter("CRYSTAL_URL");
        //QmgLogger.getLogger().info(String.format("Report bean created with ID '%s'", this.toString()));
    }

    //    public void after_selectProjectTask() {
    //        if (this.project != null)
    //            this.report.setProjectNum(project.getProject());
    //        if (this.task != null)
    //            this.report.setTaskNum(task.getTaskNumber());
    //    }

    public String addSubreport_action() {
        this.subreport = new CorpQaSubReport(this.report, this.subreportType);
        return "sub";
    }

    public void setReport(CorpQaMainReport report) {
        this.report = report;
        if (report == null) {
            QmgLogger.getLogger().info("Report set as NULL");
            return;
        }

        //  Authorization setup
        if (this.staffEntity == null) {
            this.staffEntity = Utils.getCurrentStaffEntity();
        }
        this.userId = this.staffEntity.getUserId();
        this.admin = this.staffEntity.getRoleCode().equals(RoleCodes.ADMINISTRATOR.getCode());
        QmgLogger.getLogger().info(String.format("ReportBean Admin Mode: %%s", this.admin));
        //QmgLogger.getLogger().info(String.format("Report set: '%s'", report.toString()));
        QmgLogger.getLogger().info(String.format("ReportBean Current User: %s/%s/%s", this.staffEntity.getUserId(),
                                                 this.staffEntity.getRoleCode(), this.staffEntity.getEmail()));
        if (this.report.getOwner() != null) {
            QmgLogger.getLogger().info(String.format("ReportBean Report Owner: %s/%s/%s",
                                                     this.report.getOwner().getUserId(),
                                                     this.report.getOwner().getRoleCode(),
                                                     this.report.getOwner().getEmail()));
        }

        //  Populate Project & Task descriptions for Report if not new
        if (this.report.getId() != null) {
            this.loadReport_action();
            this.originalDatePerformed = this.report.getDatePerformed();
            if (this.report.getProject() != null) {
                ImpPlanProject proj =
                    this.acepEjb.getImpPlanProjectFindForAgencyCategoryElementAndProject(this.report.getAgency(),
                                                                                         this.report.getCategory(),
                                                                                         this.report.getElement(),
                                                                                         this.report.getProject());
                if (proj != null)
                    this.report.setProjectDescription(proj.getDescription());
                this.noProject = false;
            } else {
                this.noProject = true;
            }
            if (this.report.getTask() != null) {
                ImpPlanTask task = this.acepEjb.getImpPlanTaskFindByTaskNum(this.report.getTask());
                if (task != null)
                    this.report.setTaskDescription(task.getDescription());
            }
        } else {
            //  reset
            this.subreport = null;
            this.noProject = false;
            this.project = null;
            this.task = null;
            this.originalDatePerformed = null;
        }
        this.calcualtePermissions();
    }

    private void calcualtePermissions() {
        if (this.report == null)
            return;
        //  Inactive Reports and SSSR/VQAR Type Reports are always read-only.
        //  A Role Code of 'V' for View is can never edit.
        boolean owner = report.getOwner() == null?true:Utils.isOwner(report, this.staffEntity);
        
        
        if(this.isNonEditableType()) {
            disabled = true;
        }
        else if(this.admin) {  //  Admins can always edit
            this.disabled = false;
        }       
        else if (report.getStatus().equals("I")) {
            disabled = true;
        } else {
            disabled = !(this.admin || owner);
        }
        this.addParCar = this.admin || owner;
        QmgLogger.getLogger().info(String.format("ReportBean Is Owner: %s, disabled: %s", owner, disabled));
    }
    
    //  Only IQAR, QASR and 
    private boolean isNonEditableType() {
        String type = this.report.getMainReportType();
        return type.equals("VQAR") || type.equals("SSSR");
    }

    public boolean getCanChangeDefCodeTable() {
        if (this.report == null || this.report.getSubReportList().isEmpty())
            return true;
        return false;
    }

    public CorpQaMainReport getReport() {
        return report;
    }

    public void setSubreport(CorpQaSubReport subreport) {
        this.subreport = subreport;
    }

    public CorpQaSubReport getSubreport() {
        return subreport;
    }

    public void setProject(ImpPlanProject project) {
        this.project = project;
    }

    public ImpPlanProject getProject() {
        return project;
    }

    public void setTask(ImpPlanTask task) {
        this.task = task;
    }

    public ImpPlanTask getTask() {
        return task;
    }

    public void setReportType(MainReportTypes reportType) {
        this.reportType = reportType;
    }

    public MainReportTypes getReportType() {
        return reportType;
    }

    public void setSubreportType(SubReportTypes subreportType) {
        this.subreportType = subreportType;
    }

    public SubReportTypes getSubreportType() {
        return subreportType;
    }

    public void setAcepInfo(AcepInfo acepInfo) {
        this.acepInfo = acepInfo;
        this.populateProjectAndTask();
    }

    public AcepInfo getAcepInfo() {
        return acepInfo;
    }

    public String getProjectTaskString() {
        return projectTaskString;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setNoProject(boolean noProject) {
        this.noProject = noProject;
    }

    public boolean isNoProject() {
        return noProject;
    }

    public boolean isAddParCar() {
        return addParCar;
    }

    public void setStaffEntity(CmisStaff staffEntity) {
        this.staffEntity = staffEntity;
        if (this.staffEntity != null)
            this.userId = this.staffEntity.getUserId();
    }

    public CmisStaff getStaffEntity() {
        return staffEntity;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTransimittalUrl() {
        StringBuilder sb =
            new StringBuilder().append(this.reportBaseUrl).append("qmg_transmittal&MRN=").append(this.report.getMainReportNum()).append("&RPTTYPE=").append(this.report.getMainReportType());

        return sb.toString();
    }

    //  for Crystal Reports URL
    public String getCRTransimittalUrl() {
        StringBuilder sb = new StringBuilder().append(this.reportCRBaseUrl).append(this.report.getMainReportNum());

        return sb.toString();
    }

    public String getUserId() {
        return userId;
    }

    public void subreport_selectionListener(SelectionEvent selectionEvent) {
        RichTable tbl = (RichTable) selectionEvent.getSource();
        CorpQaSubReport rpt = (CorpQaSubReport) tbl.getSelectedRowData();
        if (rpt != null) {
            this.subreport = rpt;
            this.subreportType = SubReportTypes.valueOf(rpt.getSubReportType());
        }
    }

    public String addPAR_action() {
        this.subreport = new CorpQaSubReport();
        this.subreport.setSubReportType("PAR");

        return "subreport";
    }

    public String addCAR_action() {
        this.subreport = new CorpQaSubReport();
        this.subreport.setSubReportType("CAR");

        return "subreport";
    }

    public String addOBS_action() {
        this.subreport = new CorpQaSubReport();
        this.subreport.setSubReportType("OBS");

        return "subreport";
    }

    public String getReportBaseUrl() {
        return reportBaseUrl;
    }

    private void addPerformedDate() {
        CqaDatePerformed cdp = new CqaDatePerformed();
        cdp.setDatePerformed(this.report.getDatePerformed());
        cdp.setMainReportNum(this.report.getMainReportNum());
        this.report.getPerformedDates().add(cdp);
    }

    public String saveReport_action() {
        if (!this.isNoProject() && (this.report.getProject() == null || this.report.getProject().length() == 0)) {
            Utils.addWarningMessage("No Project is selected.",
                                    "Select a Project or check the 'Not Associated with a Project' checkbox.");
            return null;
        }

        Date d = new Date();
        CmisStaff owner = Utils.getCurrentStaffEntity();
        this.userId = owner.getUserId();
        this.report.setLastModDate(d);
        this.report.setLastModUserId(this.userId);
        if (this.noProject) {
            this.report.setProject(null);
            this.report.setAgency(null);
            this.report.setCategory(null);
            this.report.setElement(null);
            this.report.setTask(null);
        }
        try {
            if (this.report.getId() == null) {

                this.report.setCreateDate(d);
                this.report.setOwner(owner);
                this.report.setCreateUserId(this.userId);
                String reportId = this.sequenceBean.getNextReportNumber(this.report.getMainReportType());
                this.report.setMainReportNum(reportId);
                this.addPerformedDate();
                this.report = this.ejbRef.persistEntity(this.report);

            } else {
                if (this.originalDatePerformed != null) {
                    if (!this.report.getDatePerformed().equals(originalDatePerformed)) {
                        if (this.originalDatePerformed.after(this.report.getDatePerformed())) {
                            Utils.addWarningMessage("The new Performed Date is before a previous Performed Date.",
                                                    "Set a date after the last Performed Date.");
                            return null;
                        }
                        this.addPerformedDate();
                    }
                    this.report = this.ejbRef.mergeEntity(this.report);
                }
            }

        } catch (QmgException e) {
            QmgLogger.getLogger().log(Level.SEVERE, "An error occurred while saving the Report", e);
            Utils.addErrorMessage("An error occurred while saving the Report", e);
        }
        Utils.addInfoMessage("The Report was saved successfully.", "");
        return null;
    }

    public String refreshReport_action() {
        if (report != null)
            QmgLogger.getLogger().info(String.format("Report refreshed: '%s'", report.toString()));
        if (this.report.getId() == null) {
            //            this.setReport(new CorpQaMainReport());
            //            this.report.setOwner(Utils.getCurrentStaffEntity());
            //            Utils.addInfoMessage("The Report has been reset", "");
            return "return";
        } else {
            this.setReport((CorpQaMainReport) this.ejbRef.refreshEntity(this.report.getId(), CorpQaMainReport.class));
            //Utils.addInfoMessage("The Report has been restored to its most recently saved state", "");
        }
        return null;
    }

    public String loadReport_action() {
        if (this.report == null || this.report.getId() == null) {
            return null;
        }
        this.report = (CorpQaMainReport) this.ejbRef.refreshEntity(this.report.getId(), CorpQaMainReport.class);
        QmgLogger.getLogger().info(String.format("Report refreshed: '%s'", report.toString()));
        List<CorpQaSubReport> subreports = this.ejbRef.getCorpQaSubReportFindForMainReport(this.report);
        this.report.setSubReportList(subreports);

        return null;
    }

    public void populateProjectAndTask() {
        if (this.acepInfo != null) {
            this.report.setAgency(this.acepInfo.getAgency());
            this.report.setCategory(this.acepInfo.getCategory());
            this.report.setElement(this.acepInfo.getElement());
            this.report.setProject(this.acepInfo.getProject());
            this.projectTaskString = this.acepInfo.toString();
            this.report.setProjectDescription(this.acepInfo.getProjectDescription());
            if (this.acepInfo.getTask() != null) {
                this.report.setTask(this.acepInfo.getTask());
                this.report.setTaskDescription(this.acepInfo.getTaskDescription());
            } else {
                this.report.setTask(null);
                this.report.setTaskDescription(null);
                this.projectTaskString = "";
            }
        }
    }

    public void acep_dialogListener(DialogEvent dialogEvent) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        ValueExpression ve =
            ctx.getApplication().getExpressionFactory().createValueExpression(ctx.getELContext(), "#{AcepBean}",
                                                                              AcepBean.class);
        AcepBean ab = (AcepBean) ve.getValue(ctx.getELContext());
        this.acepInfo = ab.getUserAcepInfo();
        if (this.acepInfo == null)
            return;
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes || dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            this.acepInfo.setTask(null);
            this.acepInfo.setTaskDescription(null);
        }
        this.populateProjectAndTask();
    }
}
