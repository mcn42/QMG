package org.mtahq.qmg.view.util;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter("BooleanYNConverter")
public class BooleanYNStringConverter implements Converter
{
    public static final String TRUE_VALUE = "1";
    public static final String FALSE_VALUE = "0";
    
    public static final String ALT_TRUE_VALUE = "1";
    public static final String ALT_FALSE_VALUE = "0";
    
    public BooleanYNStringConverter()
    {
    }    

    public Object getAsObject(FacesContext facesContext, 
        UIComponent uIComponent, String string) throws ConverterException
    {
        String val;
        if(string.equals("false"))
        {
            val = FALSE_VALUE;
        }
        else if(string.equals("true"))
        {
            val = TRUE_VALUE;
        }
        else
        {
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Converter error","Invalid value (not 'false' or 'true')."));
        }
        
        return val;
    }

    public String getAsString(FacesContext facesContext, 
        UIComponent uIComponent, Object object) throws ConverterException
    {
        String val = object.toString();
        String res = null;
        if(val.equals(TRUE_VALUE) || val.equals(ALT_TRUE_VALUE))
        {
            res = "true";
        }
        else if(val.equals(FALSE_VALUE) || val.equals(ALT_FALSE_VALUE))
        {
            res = "false";
        }
        else
        {
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Converter error","Invalid value (not Y or N)."));
        }
        
        return res;
    }
}
