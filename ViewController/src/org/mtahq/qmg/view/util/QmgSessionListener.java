package org.mtahq.qmg.view.util;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.mtahq.qmg.util.QmgLogger;

public class QmgSessionListener implements HttpSessionListener {
    public QmgSessionListener() {
        super();
    }

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        QmgLogger.getLogger().info(String.format("QMG HTTP session created: %s",httpSessionEvent.getSession().getId()));
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        QmgLogger.getLogger().info(String.format("QMG HTTP session destroyed: %s",httpSessionEvent.getSession().getId()));
    }
}
