package org.mtahq.qmg.view.util;

import java.util.logging.Level;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.mtahq.qmg.acep.session.AcepSessionEJBLocal;
import org.mtahq.qmg.beans.SequenceBeanInterface;
import org.mtahq.qmg.session.QmgSessionEJBLocal;
import org.mtahq.qmg.util.QmgLogger;

public class ServiceLocator {
    private static QmgSessionEJBLocal qmgSessionBean;
    private static AcepSessionEJBLocal acepSessionBean;
    private static SequenceBeanInterface sequenceBean;
    
    private ServiceLocator() {
        super();
    }
    
    static {
        setUpEjbRefs();
    }
    
    private static void setUpEjbRefs() {
        try {
            InitialContext ctx = new InitialContext();
            qmgSessionBean = (QmgSessionEJBLocal) ctx.lookup("java:comp/env/ejb/local/QmgSessionEJB");
            acepSessionBean = (AcepSessionEJBLocal) ctx.lookup("java:comp/env/ejb/local/AcepSessionEJB");
            sequenceBean = (SequenceBeanInterface) ctx.lookup("java:comp/env/ejb/local/QmgSequenceBean");
        } catch (NamingException e) {
            QmgLogger.getLogger().log(Level.SEVERE,"EJB lookup failed",e);
        }
    }

    public static QmgSessionEJBLocal getQmgSessionBean() {
        return qmgSessionBean;
    }

    public static AcepSessionEJBLocal getAcepSessionBean() {
        return acepSessionBean;
    }

    public static SequenceBeanInterface getSequenceBean() {
        return sequenceBean;
    }
}
