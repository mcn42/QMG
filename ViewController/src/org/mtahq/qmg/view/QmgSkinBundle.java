package org.mtahq.qmg.view;

import java.util.ListResourceBundle;

public class QmgSkinBundle extends ListResourceBundle {
    static private final Object[][] _CONTENTS = {
              {"af_panelCollection.LABEL_DETACH_TABLE_DLG", "Maximize"},
              {"af_inputDate.TIP_TITLE_SELECT_DATE", "Example: 05/15/2016"},
              {"af_inputDate.TIP_TITLE_SELECT_DATE_AND_TIME", "Example: 05/15/2016 12:05:30AM"},
              {"af_document.LABEL_SPLASH_SCREEN","Quality! Excellence!  Dedication!  You won't find them here!"}
            };
    
    public QmgSkinBundle() {
        super();
    }

    @Override
    protected Object[][] getContents() {
        return _CONTENTS;
    }
}
